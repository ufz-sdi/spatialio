<!-- 
SPDX-FileCopyrightText: 2025 Hannes Bohring <hannes.bohring@ufz.de>
SPDX-FileCopyrightText: 2025 Helmholtz Centre for Environmental Research GmbH - UFZ

SPDX-License-Identifier: EUPL-1.2
-->

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.7.0] - 2025-02-12

- Release 0.7.0

## [0.7.0-alpha1] - 2025-01-23

- adds possibility to override deployment strategy for geoserver

## [0.6.0-alpha1] - 2025-01-23

- adds startupProbe for geoserver-subchart

## [0.5.0-alpha1] - 2025-01-22

- adds possibility of lifecycle hook to geoserver


## [0.4.0-alpha1] - 2024-12-18

- adds thredds server

## [0.3.0-alpha2] - 2024-12-17

- some env vars for pygeoapi

## [0.3.0-alpha1] - 2024-12-17

- adds pygeoapi

## [0.2.0-alpha2] - 2024-12-12

- optional livenessProbes/readinessProbes for wis-d

## [0.2.0-alpha1] - 2024-12-12

- fix wrong image-names
- make resources optional
- optional livenessProbes/readinessProbes

## [0.1.0-alpha1] - 2024-11-15

- Initial release

[0.1.0]:
