<!-- 
SPDX-FileCopyrightText: 2024 Hannes Bohring <hannes.bohring@ufz.de>
SPDX-FileCopyrightText: 2024 Helmholtz Centre for Environmental Research GmbH - UFZ

SPDX-License-Identifier: EUPL-1.2
-->

# Spatial.IO Helm charts

This project provides a collection of Spatial.IO related Helm charts.
Helm packages are published in the project's [package registry](https://codebase.helmholtz.cloud/ufz-sdi/spatialio/-/packages).

## Usage
### Authenticate to the Helm repository
To authenticate to the Helm repository, you need either:
* A [personal access token](https://codebase.helmholtz.cloud/-/profile/personal_access_tokens) with the scope set to `api`.
* A [deploy token](https://docs.gitlab.com/ee/user/project/deploy_tokens/index.html) with the scope set to `read_package_registry`, `write_package_registry` , or both.
* A [CI/CD job token](https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html).

### Add the Helm repository
To add the Helm chart repository, use the following command:
```shell
helm repo add --username <username> --password <access_token> spatialio https://codebase.helmholtz.cloud/api/v4/projects/<PROJECTNR>/packages/helm/stable
helm repo add --username <username> --password <access_token> spatialio-devel https://codebase.helmholtz.cloud/api/v4/projects/<PROJECTNR>/packages/helm/devel
```

Update the latest information about charts from your added chart repositories:
```shell
helm repo update
```

List available charts:
```shell
helm search repo spatialio
```

### Install a package
To install the latest version of the chart, use the following command:
```shell
helm install my-release-name spatialio/spatialio
```

### Use CI/CD to publish a Helm package
Create and push a git tag according to the following naming scheme, e.g. `0.4.0` or `0.5.0-alpha3`.
This Tagname should match the `version` parameter inside the main `Chart.yaml`

When such a tag is pushed to the default branch `main`, the Helm package is published to the stable channel. 
In all other cases, it is published as a development version on the unstable channel.

### Publish a package manually
Get the `helm cm-push` plugin:
```shell
helm plugin install https://github.com/chartmuseum/helm-push
```

Package the chart directory into a chart archive:
```shell
helm package .
```

Push the chart manually to the Helm package registry of this GitLab project:
```shell
helm cm-push spatialio-0.1.0.tgz spatialio
```


## Develop
### Test the charts
```shell
helm install custom-rdm-spatialio ./ -n custom-rdm-spatialio --dry-run
```
