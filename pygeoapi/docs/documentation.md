# Documentation

## Processes

### Aggregation

Calculates spatial aggregation on raster data.

```
http://localhost:5000/processes/aggregation/execution
```

| parameter                  | mandatory | desctiption                               |
|----------------------------|-----------|-------------------------------------------|
| _raster_                   | yes       | path to netcdf file                       | 
| _crs_                      | yes       | crs of input raster                       |
| _time_                     | no        | time period or stamp (day, month or year) |
| _geometry / bl_id / lk_id_ | no        | subset as geojson or id                   |
| _spatial_aggregation_type_ | yes       | list of aggregation types                 |
| _variables_                | no        | variable name(s)                          |

### Anomaly

Calculates absolute and relative difference of raster data compared to a 30-year reference-period.

```
http://localhost:5000/processes/anomaly/execution
```

| parameter                   | mandatory | desctiption                                    |
|-----------------------------|-----------|------------------------------------------------|
| _raster_                    | yes       | path to netcdf file                            | 
| _crs_                       | yes       | crs of input raster                            |
| _time_                      | yes       | time period or stamp (month or year)           |
| _geometry / bl_id / lk_id_  | no        | subset as geojson or id                        |
| _reference_                 | yes       | start and end year of 30-year reference period |
| _variables_                 | no        | variable name(s)                               |
| _spatial_aggregation_type_  | yes       | aggregation type for spatial aggregation       |
| _temporal_aggregation_type_ | yes       | aggregation type for temporal aggregation      |

### Magnitude

Calculates magnitude and intensity for percentile-based raster data.

```
http://localhost:5000/processes/magnitude/execution
```

| parameter                  | mandatory | desctiption                          |
|----------------------------|-----------|--------------------------------------|
| _raster_                   | yes       | path to netcdf file                  | 
| _crs_                      | yes       | crs of input raster                  |
| _time_                     | no        | time period or stamp (month or year) |
| _geometry / bl_id / lk_id_ | no        | subset as geojson or id              |
| _threshold_                | yes       | threshold                            |
| _variables_                | no        | variable name(s)                     |

## Subsets

All processes can pre-select data to a spatio-temporal extent. Subsets can be given as input parameters for each process.

Spatial extent can be given as list of geojson objects (if no _crs_ is given, _epsg:4326_ will be used as default):
```
"geometry": [
                  {
                    "coordinates": [10, 52],
                    "crs": "EPSG:4326",
                    "type": "Point"
                  }
            ]
```
Or id for a _Bundesland_ (for full list see: /data/bundesland/rs.txt) :
```
"bl_id": "01"
```
Or as id for a _Landkreis_ (extended _Bundesland_-id):
```
"lk_id": "01001"
```

Temporal subsets can be given as list of date(s) (day, month or year):
```
"time": ["2022-01-01"] or "time": ["2022-02"] or "time": ["2022"] or "time": ["2022-01", "2022-02"]
```