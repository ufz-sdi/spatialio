# Aggregation API

API for spatial aggregation on netCDF data

[Documentation](./docs/documentation.md).

## External data

This application uses district and country data as geojson from [OpenDataLab](http://opendatalab.de/projects/geojson-utilities/).

## Installation

### Docker

Build Image and start up:
```commandline
docker compose up -d
```

Run test:

```commandline
docker exec -it pygeoapi-aggregation-api bash

python3 /usr/processes/tests/run_tests.py
```

### Without Docker

Change into directory "aggregation"

```
pip install pygeoapi

export PYGEOAPI_CONFIG=/path/to/pygeoapi-config.yaml

export PYGEOAPI_OPENAPI=/path/to/pygeoapi-openapi.yaml

pip install -e .

pygeoapi serve
```
## Start application

Call http://localhost:5000/openapi?f=html
