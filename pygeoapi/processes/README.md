## Create Python Package

```
python3 -m pip install --upgrade pip

python3 -m pip install --upgrade build

python3 -m build

python3 -m pip install -e .
```
