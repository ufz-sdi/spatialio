# =================================================================
#
# Authors: Tom Kralidis <tomkralidis@gmail.com>
#
# Copyright (c) 2019 Tom Kralidis
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# =================================================================

import logging
from pygeoapi.process.base import BaseProcessor
from src.computation.AggregationComputation import AggregationComputation
from src.manager.AggregationInputManager import AggregationInputManager

LOGGER = logging.getLogger(__name__)

#: Process metadata and description
PROCESS_METADATA = {
    "version": "0.0.1",
    "id": "aggregation",
    "title": {
        "en": "aggregation"
    },
    "description": {
        "en": "Aggregate data to a spatio-temporal extend"
    },
    "keywords": ["aggregation"],
    "links": [{
        "type": "text/html",
        "rel": "canonical",
        "title": "information",
        "href": "https://example.org/process",
        "hreflang": "en-US"
    }],
    "inputs": {
        "raster": {
            "title": "raster layer",
            "description": "Path to a raster layer",
            "schema": {
                "type": "string"
            },
            "minOccurs": 1,
            "maxOccurs": 1,
            "metadata": None,
            "keywords": ["raster layer"]
        },
        "raster_format": {
            "title": "format raster layer",
            "description": "Format of raster layer (currently supported: netcdf, tiff)",
            "schema": {
                "type": "string"
            },
            "minOccurs": 1,
            "maxOccurs": 1,
            "metadata": None,
            "keywords": ["raster layer format"]
        },
        "time": {
            "title": "time range or steps (optional)",
            "description": "Time subset",
            "schema": {
                "type": "string"
            },
            "minOccurs": 0,
            "metadata": None,
            "keywords": ["time range"]
        },
        "bl_id": {
            "title": "Bundesland Regionalschlüssel (optional)",
            "description": "identifier of Bundesland",
            "schema": {
                "type": "string"
            },
            "minOccurs": 0,
            "maxOccurs": 1,
            "metadata": None,
            "keywords": ["predefined region"]
        },
        "lk_id": {
            "title": "Landkreis Regionalschlüssel (optional)",
            "description": "identifier of Landkreis",
            "schema": {
                "type": "string"
            },
            "minOccurs": 0,
            "maxOccurs": 1,
            "metadata": None,
            "keywords": ["predefined region"]
        },
        "geometry": {
            "title": "spatial subset (optional)",
            "description": "Spatial subset as geojson",
            "schema": {
                "type": "geojson"
            },
            "minOccurs": 0,
            "metadata": None,
            "keywords": ["spatial subset"]
        },
        "spatial_aggregation_type": {
            "title": "spatial aggregation type (optional)",
            "description": "Aggregation type",
            "schema": {
                "type": "string"
            },
            "minOccurs": 0,
            "metadata": None,
            "keywords": ["computation type"]
        },
        "crs": {
            "title": "crs",
            "description": "Crs of input raster",
            "schema": {
                "type": "string"
            },
            "minOccurs": 1,
            "maxOccurs": 1,
            "metadata": None,
            "keywords": ["crs"]
        },
        "variable": {
            "title": "variable (optional)",
            "description": "name of the variable to use for computation",
            "schema": {
                "type": "string"
            },
            "minOccurs": 0,
            "metadata": None,
            "keywords": ["variable"]
        },
        "no_data_value": {
            "title": "no data value",
            "description": "no data value",
            "schema": {
                "type": "integer"
            },
            "minOccurs": 0,
            "macOccurs": 1,
            "metadata": None,
            "keywords": ["nan", "no data value"]
        }
    },
    "outputs": {
        "computation": {
            "title": "Aggregation",
            "description": "List of computation values",
            "schema": {
                "type": "object",
                "contentMediaType": "application/json"
            }
        }
    },
    "example": {
        "inputs": {
            "spatial_aggregation_type": "mean",
            "raster_format": "netcdf",
            "crs": "EPSG:4326",
            "variable": "pre",
            "raster": "wis-d/historic/pre/pre_1947_2019_short_sf0p1_d9_monsum/pre_1947_2019_short_sf0p1_d9_monsum.nc",
            "geometry": [{"type": "Point", "crs": "EPSG:4326", "coordinates": [10, 52]}],
            "time": ["2018-03"]
        }
    }
}


class AggregationProcessor(BaseProcessor):

    def __init__(self, processor_def):
        """
        Initialize object

        :param processor_def: provider definition

        :returns: pygeoapi.process.hello_world.HelloWorldProcessor
        """

        super().__init__(processor_def, PROCESS_METADATA)

    def execute(self, data: dict) -> tuple:
        """
        exeute process Einfache Aggregation
        :param data: user input
        :return: time steps and aggregated values
        """

        LOGGER.debug("execute process computation")
        execution_error = False
        error_message = ""
        mimetype = "application/json"

        while execution_error is False:
            """
            check and get input
            """
            LOGGER.debug("Preparing inputs")
            try:
                valid_input = AggregationInputManager().get_valid_input(data)
            except ValueError as e:
                LOGGER.error("error occurred at input checks, cancel process: %s", e)
                error_message = str(e)
                execution_error = True
                break
            """
            computation
            """
            LOGGER.debug("Starting computation")
            try:
                time_steps, aggregated = AggregationComputation().start_computation(valid_input)
                output = self.create_output(data, time_steps, aggregated)
                LOGGER.debug("Calculated computation: %s", output)
                return mimetype, output
            except Exception as e:
                LOGGER.error("Error occurred at computation: %s", e)
                error_message = str(e)
                execution_error = True
                break
        return mimetype, {"ERROR": error_message}

    def __repr__(self):
        return "<AggregationProcessor> {}".format(self.name)

    def create_output(self, inputs: dict, time: list, aggregation: dict) -> dict:
        """
        create output dict
        :param inputs:
        :param time: time steps
        :param aggregation: aggregated values
        :return: output dict
        """
        return {
            "id": "aggregation",
            "inputs": inputs,
            "results": {
                "time_steps": time,
                "values": aggregation
            }
        }
