# =================================================================
#
# Authors: Tom Kralidis <tomkralidis@gmail.com>
#
# Copyright (c) 2019 Tom Kralidis
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# =================================================================

import logging

from pygeoapi.process.base import BaseProcessor
from src.manager.AnomalyInputManager import AnomalyInputManager
from src.computation.AnomalyComputation import AnomalyComputation

LOGGER = logging.getLogger(__name__)

#: Process metadata and description
PROCESS_METADATA = {
    "version": "0.0.1",
    "id": "anomaly",
    "title": {
        "en": "anomaly"
    },
    "description": {
        "en": "Calculate anomaly of time series data"
    },
    "keywords": ["computation", "anomaly"],
    "links": [{
        "type": "text/html",
        "rel": "canonical",
        "title": "information",
        "href": "https://example.org/process",
        "hreflang": "en-US"
    }],
    "inputs": {
        "raster": {
            "title": "raster layer",
            "description": "Path to a raster layer",
            "schema": {
                "type": "string"
            },
            "minOccurs": 1,
            "maxOccurs": 1,
            "metadata": None,
            "keywords": ["raster layer"]
        },
        "reference": {
            "title": "reference raster layer",
            "description": "Path to a raster layer with 30-year reference",
            "schema": {
                "type": "string"
            },
            "minOccurs": 1,
            "maxOccurs": 1,
            "metadata": None,
            "keywords": ["raster layer", "reference", "30-year"]
        },
        "time": {
            "title": "time range",
            "description": "Time subset as yyyy-mm or yyyy",
            "schema": {
                "type": "string"
            },
            "minOccurs": 1,
            "metadata": None,
            "keywords": ["time range"]
        },
        "bl_id": {
            "title": "Bundesland Regionalschlüssel (optional)",
            "description": "identifier of Bundesland",
            "schema": {
                "type": "string"
            },
            "minOccurs": 0,
            "maxOccurs": 1,
            "metadata": None,
            "keywords": ["predefined region"]
        },
        "lk_id": {
            "title": "Landkreis Regionalschlüssel (optional)",
            "description": "identifier of Landkreis",
            "schema": {
                "type": "string"
            },
            "minOccurs": 0,
            "maxOccurs": 1,
            "metadata": None,
            "keywords": ["predefined region"]
        },
        "geometry": {
            "title": "spatial subset (optional)",
            "description": "Spatial subset as geojson",
            "schema": {
                "type": "geojson"
            },
            "minOccurs": 0,
            "metadata": None,
            "keywords": ["spatial subset"]
        },
        "spatial_aggregation_type": {
            "title": "computation type",
            "description": "Aggregation type",
            "schema": {
                "type": "string"
            },
            "minOccurs": 1,
            "maxOccurs": 1,
            "metadata": None,
            "keywords": ["computation type"]
        },
        "temporal_aggregation_type": {
            "title": "computation type",
            "description": "Aggregation type",
            "schema": {
                "type": "string"
            },
            "minOccurs": 1,
            "maxOccurs": 1,
            "metadata": None,
            "keywords": ["computation type"]
        },
        "variable": {
            "title": "variable (optional)",
            "description": "name of the variable to use for computation",
            "schema": {
                "type": "string"
            },
            "minOccurs": 0,
            "metadata": None,
            "keywords": ["variable"]
        },
        "crs": {
            "title": "crs",
            "description": "Variable name for crs",
            "schema": {
                "type": "string"
            },
            "minOccurs": 1,
            "maxOccurs": 1,
            "metadata": None,
            "keywords": ["crs"]
        }
    },
    "outputs": {
        "computation": {
            "title": "Anomaly",
            "description": "Absolute and relative difference",
            "schema": {
                "type": "object",
                "contentMediaType": "application/json"
            }
        }
    },
    "example": {
        "inputs": {
            "spatial_aggregation_type": "mean",
            "temporal_aggregation_type": "sum",
            "crs": "EPSG:4326",
            "raster": "/usr/test-data/pre/pre_1947_2019_short_sf0p1_d9_monsum.nc",
            "reference": ["1961", "1990"],
            "bl_id": "14",
            "time": ["2018-07"],
            "variable": "pre"
        }
    }
}


class AnomalyProcessor(BaseProcessor):

    def __init__(self, processor_def):
        """
        Initialize object

        :param processor_def: provider definition

        :returns: pygeoapi.process.hello_world.HelloWorldProcessor
        """

        super().__init__(processor_def, PROCESS_METADATA)

    def execute(self, data: dict) -> tuple:
        """
        exeute process Anomalie-Berechnung
        :param data: user input
        :return: absolute and relative difference between data and reference
        """

        LOGGER.debug("start process anomaly detection")
        execution_error = False
        error_message = ""
        mimetype = "application/json"

        while execution_error is False:
            """
            check and get input
            """
            try:
                valid_input = AnomalyInputManager().get_valid_input(data)
            except ValueError as e:
                LOGGER.debug("error occurred at input checks, cancel process: %s", str(e))
                execution_error = True
                error_message = str(e)
                break

            """
            start computation
            """
            try:
                anomalies = AnomalyComputation().start_computation(valid_input)
                LOGGER.debug("calculated anomaly: %s", anomalies)
                return mimetype, self.create_output(data, anomalies)
            except Exception as e:
                LOGGER.debug("error occurred at anomaly calculation: %s", str(e))
                execution_error = True
                error_message = str(e)
                break

        """
        error output
        """
        return mimetype, {"ERROR": error_message}

    def create_output(self, inputs: dict, results: dict):
        """
        create output dict
        :param inputs:
        :param results: results
        :return: output dict
        """
        return {
            "id": "anomaly",
            "inputs": inputs,
            "results": results
        }

    def __repr__(self):
        return "<AnomalyProcessor> {}".format(self.name)
