# =================================================================
#
# Authors: Tom Kralidis <tomkralidis@gmail.com>
#
# Copyright (c) 2019 Tom Kralidis
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# =================================================================

import logging
from pygeoapi.process.base import BaseProcessor
from src.manager.MagnitudeInputManager import MagnitudeInputManager
from src.computation.MagnitudeComputation import MagnitudeComputation

LOGGER = logging.getLogger(__name__)

#: Process metadata and description
PROCESS_METADATA = {
    "version": "0.0.1",
    "id": "magnitude",
    "title": {
        "en": "magnitude"
    },
    "description": {
        "en": "Calculate magnitude and intensity of percentile based data"
    },
    "keywords": ["computation"],
    "links": [{
        "type": "text/html",
        "rel": "canonical",
        "title": "information",
        "href": "https://example.org/process",
        "hreflang": "en-US"
    }],
    "inputs": {
        "raster": {
            "title": "raster layer",
            "description": "Path to a raster layer",
            "schema": {
                "type": "string"
            },
            "minOccurs": 1,
            "maxOccurs": 1,
            "metadata": None,
            "keywords": ["raster layer"]
        },
        "time": {
            "title": "time range (optional)",
            "description": "Time subset",
            "schema": {
                "type": "string"
            },
            "minOccurs": 0,
            "metadata": None,
            "keywords": ["time range"]
        },
        "bl_id": {
            "title": "Bundesland Regionalschlüssel (optional)",
            "description": "identifier of Bundesland",
            "schema": {
                "type": "string"
            },
            "minOccurs": 0,
            "maxOccurs": 1,
            "metadata": None,
            "keywords": ["predefined region"]
        },
        "lk_id": {
            "title": "Landkreis Regionalschlüssel (optional)",
            "description": "identifier of Landkreis",
            "schema": {
                "type": "string"
            },
            "minOccurs": 0,
            "maxOccurs": 1,
            "metadata": None,
            "keywords": ["predefined region"]
        },
        "geometry": {
            "title": "spatial subset (optional)",
            "description": "Spatial subset as geojson",
            "schema": {
                "type": "geojson"
            },
            "minOccurs": 0,
            "metadata": None,
            "keywords": ["spatial subset"]
        },
        "crs": {
            "title": "crs",
            "description": "Variable name for crs",
            "schema": {
                "type": "string"
            },
            "minOccurs": 1,
            "maxOccurs": 1,
            "metadata": None,
            "keywords": ["crs"]
        },
        "threshold": {
            "title": "threshold",
            "description": "threshold",
            "schema": {
                "type": "float"
            },
            "minOccurs": 1,
            "maxOccurs": 1,
            "metadata": None,
            "keywords": ["threshold"]
        },
        "variable": {
            "title": "variable (optional)",
            "description": "name of the variable to use for computation",
            "schema": {
                "type": "string"
            },
            "minOccurs": 0,
            "metadata": None,
            "keywords": ["variable"]
        }
    },
    "outputs": {
        "computation": {
            "title": "Aggregation",
            "description": "List of computation values",
            "schema": {
                "type": "object",
                "contentMediaType": "application/json"
            }
        }
    },
    "example": {
        "inputs": {
            "crs": "EPSG:4326",
            "raster": "/usr/test-data/SMI/SMI_SM_5_25cm_1951_2020_day15_short_sf0p0001_d9.nc",
            "geometry": [{"type": "Point", "coordinates": [10, 52]}],
            "time": ["2018"],
            "variables": "SMI",
            "threshold": 0.2
        }
    }
}


class MagnitudeProcessor(BaseProcessor):

    def __init__(self, processor_def):
        """
        Initialize object

        :param processor_def: provider definition

        :returns: pygeoapi.process.hello_world.HelloWorldProcessor
        """

        super().__init__(processor_def, PROCESS_METADATA)

    def execute(self, data: dict) -> tuple:
        """
        exeute process Magnitude und Intensität
        :param data: user input
        :return: magnitude and intensity
        """

        LOGGER.debug("start process magnitude and intensity calculation")
        execution_error = False
        error_message = ""
        mimetype = "application/json"

        while execution_error is False:
            """
            check and get input
            """
            try:
                valid_input = MagnitudeInputManager().get_valid_input(data)
            except ValueError as e:
                LOGGER.debug("error occurred at input checks, cancel process: %s", str(e))
                execution_error = True
                error_message = str(e)
                break

            """
            start computation
            """
            try:
                magnitude_and_intensity = MagnitudeComputation().start_computation(valid_input)
                LOGGER.debug("calculated magnitude and intensity: %s", magnitude_and_intensity)
                return mimetype, self.create_output(data, magnitude_and_intensity)
            except Exception as e:
                LOGGER.debug("error occurred at computation: %s", str(e))
                execution_error = True
                error_message = str(e)
                break

        return mimetype, {"ERROR": error_message}

    def create_output(self, inputs: dict, results: dict):
        """
        create output dict
        :param inputs:
        :param results: content
        :return: output dict
        """
        return {
            "id": "magnitude",
            "inputs": inputs,
            "results": results
        }

    def __repr__(self):
        return "<MagnitudeProcessor> {}".format(self.name)
