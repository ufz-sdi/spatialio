# =================================================================
#
# Authors: Tom Kralidis <tomkralidis@gmail.com>
#
# Copyright (c) 2019 Tom Kralidis
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# =================================================================

import logging
from pygeoapi.process.base import BaseProcessor
from src.manager.TimeSeriesInputManager import TimeSeriesInputManager
from src.computation.TimeSeriesComputation import TimeSeriesComputation

LOGGER = logging.getLogger(__name__)

#: Process metadata and description
PROCESS_METADATA = {
    "version": "0.0.1",
    "id": "time series",
    "title": {
        "en": "time series"
    },
    "description": {
        "en": "Get time series for given point"
    },
    "keywords": ["computation"],
    "links": [{
        "type": "text/html",
        "rel": "canonical",
        "title": "information",
        "href": "https://example.org/process",
        "hreflang": "en-US"
    }],
    "inputs": {
        "raster": {
            "title": "raster layer",
            "description": "Path to a raster layer",
            "schema": {
                "type": "string"
            },
            "minOccurs": 1,
            "maxOccurs": 1,
            "metadata": None,
            "keywords": ["raster layer"]
        },
        "raster_format": {
            "title": "format raster layer",
            "description": "Format of raster layer (currently supported: netcdf, tiff)",
            "schema": {
                "type": "string"
            },
            "minOccurs": 1,
            "maxOccurs": 1,
            "metadata": None,
            "keywords": ["raster layer format"]
        },
        "subset": {
            "title": "spatial subset",
            "description": "Spatial subset as geojson",
            "schema": {
                "type": "json"
            },
            "minOccurs": 0,
            "metadata": None,
            "keywords": ["spatial subset"]
        },
        "time": {
            "title": "time range or steps (optional)",
            "description": "Time subset",
            "schema": {
                "type": "string"
            },
            "minOccurs": 0,
            "metadata": None,
            "keywords": ["time range"]
        },
        "crs": {
            "title": "crs",
            "description": "Variable name for crs",
            "schema": {
                "type": "string"
            },
            "minOccurs": 1,
            "maxOccurs": 1,
            "metadata": None,
            "keywords": ["crs"]
        },
        "variable": {
            "title": "variable (optional)",
            "description": "name of the variable to use for computation",
            "schema": {
                "type": "string"
            },
            "minOccurs": 0,
            "metadata": None,
            "keywords": ["variable"]
        }

    },
    "outputs": {
        "computation": {
            "title": "Aggregation",
            "description": "List of computation values",
            "schema": {
                "type": "object",
                "contentMediaType": "application/json"
            }
        }
    },
    "example": {
        "inputs": {
            "raster_format": "netcdf",
            "crs": "EPSG:4326",
            "variable": "pre",
            "raster": "/usr/test-data/pre/pre_1947_2019_short_sf0p1_d9_monsum.nc",
            "subset": {"latitude": 50, "longitude": 10},
            "time": ["2018"]

        }
    }
}


class TimeSeriesProcessor(BaseProcessor):

    def __init__(self, processor_def):
        """
        Initialize object

        :param processor_def: provider definition

        :returns: pygeoapi.process.hello_world.HelloWorldProcessor
        """

        super().__init__(processor_def, PROCESS_METADATA)

    def execute(self, data: dict) -> tuple:
        """
        exeute process Magnitude und Intensität
        :param data: user input
        :return: magnitude and intensity
        """

        LOGGER.debug("start process magnitude and intensity calculation")
        execution_error = False
        error_message = ""
        mimetype = "application/json"

        while execution_error is False:
            """
            check and get input
            """
            try:
                valid_input = TimeSeriesInputManager().get_valid_input(data)
            except ValueError as e:
                LOGGER.debug("error occurred at input checks, cancel process: %s", str(e))
                execution_error = True
                error_message = str(e)
                break

            """
            start computation
            """
            try:
                time_steps, time_series = TimeSeriesComputation().start_computation(valid_input)
                LOGGER.debug("Extracted time series for point: %s", time_series)
                return mimetype, self.create_output(time_steps, time_series)
            except Exception as e:
                LOGGER.debug("error occurred at computation: %s", str(e))
                execution_error = True
                error_message = str(e)
                break

        return mimetype, {"ERROR": error_message}

    def create_output(self, time_steps: list, time_series: list):
        """
        create output dict
        :param time_series:
        :param time_steps:
        :return: output dict
        """
        return {
            "id": "time series",
            "time_steps": time_steps,
            "values": time_series
        }

    def __repr__(self):
        return "<MagnitudeProcessor> {}".format(self.name)
