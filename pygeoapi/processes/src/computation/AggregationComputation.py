from typing import Tuple
import numpy
from src.computation.Computation import Computation
import logging

LOGGER = logging.getLogger(__name__)


class AggregationComputation(Computation):

    def __init__(self):
        super().__init__()

    def start_computation(self, data: dict) -> Tuple[list, list]:
        """
        call necessary functions for computation calculations
        :param data: data dict
        :return: dict with aggregated values for chosen variables
        """
        if data["raster_format"] == "tiff":
            mf_list = self.get_mf_list(data["raster_path"], data["time"])
            aggregated_list = []
            for handle in mf_list:
                ds = self.get_rasterio(handle, data["data_variable"])
                aggregated_list.append(self._computation(data, ds)[0])
            return data["time_steps"], aggregated_list
        else:
            ds = self.get_dataset(data["raster_path"])
            ds = self.slice_by_time(ds, data["time"])
            if type(ds.time.values) is numpy.datetime64:
                data["time_steps"].append(str(ds.time.values.astype("datetime64[D]")))
            else:
                for date in ds.time.values:
                    data["time_steps"].append(str(date.astype("datetime64[D]")))
            return data["time_steps"], self._computation(data, ds)

    def _computation(self, data, ds):
        ds = self.clip_by_geometry(ds, data["subset"], data["raster_crs"])
        data_variable = self.get_data_var(ds, data["data_variable"])
        coordinate_names = self.get_coordinate_names(ds)
        if data["spatial_aggregation_type"] is not None:
            aggregated = self.aggregation(ds, data_variable, data["spatial_aggregation_type"],
                                          [coordinate_names["lat"], coordinate_names["lon"]],
                                          data["no_data_value"])
            ds.close()
            self.close_s3fs()
            return self.aggregated_as_str(aggregated)
        else:
            ds.close()
            self.close_s3fs()
            return []
