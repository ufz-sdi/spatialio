import os
import xarray
from src.computation.Computation import Computation
import logging

LOGGER = logging.getLogger(__name__)


class AnomalyComputation(Computation):

    def __init__(self):
        super().__init__()

    def start_computation(self, data: dict) -> dict:
        """
        call necessary functions for computation calculations
        :param data: data dict
        :return: dict with aggregated values for chosen variables
        """
        ds = self.prepare_dataset(data)
        ds_reference = self.prepare_precipitation_reference_dataset(data)
        reference = self.get_reference(ds_reference["pre"], data)
        data_variable = self.get_data_var(ds, data["data_variable"])
        observation = self.get_observation(ds[data_variable], data)
        LOGGER.debug("observation mean: %s", observation.values)
        LOGGER.debug("reference mean: %s", reference.values)
        absolute_difference = observation - reference
        LOGGER.debug("Absolute difference: %s", absolute_difference.values)
        relative_difference = absolute_difference / reference * 100
        LOGGER.debug("Relative difference: %s", relative_difference.values)
        return {
            "absolute": round(float(absolute_difference), 2),
            "relative": round(float(relative_difference), 2)
        }

    def slice_years_by_months(self, ds: xarray.Dataset, month_ids: list) -> xarray.Dataset:
        """
        reshapes dataset and creates coordinates for year and month
        select only months which are inside given time range for each year
        :param ds: dataset
        :param month_ids: numbers of chosen months
        :return: dataset with only selected months
        """
        ds = self.reshape_time(ds)
        if len(month_ids) > 1:
            return ds.sel(month=slice(month_ids[0], month_ids[-1]))
        elif len(month_ids) == 1:
            return ds.sel(month=month_ids[0])
        else:
            return ds

    def reshape_time(self, ds: xarray.Dataset) -> xarray.Dataset:
        """
        set new coordinates for year and month from time coordinates
        :param ds: dataset
        :return: dataset with new coordinates
        """
        # ds.time.dt.year.data -> dataset->time coordinate->date->year->data
        ds = ds.assign_coords(year=("time", ds.time.dt.year.data), month=("time", ds.time.dt.month.data))
        return ds.set_index(time=("year", "month")).unstack("time")

    def prepare_dataset(self, data: dict) -> xarray.Dataset:
        """
        create dataset and create subset from time and spatial ranges given
        :param data: dictionary containing raster path, time range and spatial subset
        :return: selected dataset
        """
        ds = self.get_dataset(data["raster_path"])
        ds = self.slice_by_time(ds, data["time"])
        return self.clip_by_geometry(ds, data["subset"], data["raster_crs"])

    def prepare_precipitation_reference_dataset(self, data: dict) -> xarray.Dataset:
        """
        create dataset from precipitation reference data
        :param data: data containing time range and spatial subset
        :return: selected dataset
        """
        ds_reference = self.get_dataset(os.environ["PRE_REFERENCE"])
        ds_reference = self.slice_by_time(ds_reference, data["reference_time"])
        ds_reference = self.slice_years_by_months(ds_reference, data["months_ids"])
        return self.clip_by_geometry(ds_reference, data["subset"], data["raster_crs"])

    def get_observation(self, ds: xarray.Dataset, data: dict) -> xarray.Dataset:
        """
        calculate spatial and temporal aggregation for observation data
        :param ds: dataset
        :param data: dictionary with aggregation types and dimension names
        :return: aggregated value
        """
        coordinate_names = self.get_coordinate_names(ds)
        observation = self.call_aggregation_method(ds, data["spatial_aggregation_type"],
                                                   [coordinate_names["lat"], coordinate_names["lon"]])
        return self.call_aggregation_method(observation, data["temporal_aggregation_type"],
                                            [coordinate_names["time"]])

    def get_reference(self, ds: xarray.Dataset, data: dict) -> xarray.Dataset:
        """
        calculate spatial and temporal aggregation for reference data and return mean over all years
        :param ds: dataset
        :param data: dictionary with aggregation types and dimension names
        :return: aggregated dataset
        """
        coordinate_names = self.get_coordinate_names(ds)
        reference = self.call_aggregation_method(ds, data["spatial_aggregation_type"],
                                                 [coordinate_names["lat"], coordinate_names["lon"]])
        if ds.ndim > 3:
            reference = self.call_aggregation_method(reference, data["temporal_aggregation_type"],
                                                     ["year", "month"]) / reference.sizes["year"]
        else:
            reference = self.call_aggregation_method(reference, data["temporal_aggregation_type"], ["year"]) / \
                        reference.sizes["year"]
        return reference
