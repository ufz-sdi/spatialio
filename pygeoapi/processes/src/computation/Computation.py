import xarray
import rioxarray
import rasterio
import os
import logging

from numpy import ndarray
from xarray import Dataset

import s3fs

LOGGER = logging.getLogger(__name__)

"""
variables without spatial dimensions will be left as is when the data is edited
"""
rioxarray.set_options(skip_missing_spatial_dims=True)


def get_s3fs():
    params = {
        "endpoint_url": os.environ.get("MINIO_ENDPOINT")
    }

    region_name = os.environ.get("MINIO_REGION_NAME", None)
    if region_name:
        params["region_name"] = region_name

    return s3fs.S3FileSystem(
        client_kwargs=params,
        key=os.environ.get("MINIO_USERNAME"),
        secret=os.environ.get("MINIO_PASSWORD")
    )

class Computation:

    def __init__(self):
        self.s3fs_file_handle = None

    def get_s3fs_handle(self, path: str):
        s3fs = get_s3fs()
        self.s3fs_file_handle = s3fs.open(path, 'rb')

    def close_s3fs(self):
        self.s3fs_file_handle.close()

    def get_dataset(self, path: str) -> xarray.Dataset:
        """
        create xarray.Dataset from netcdf
        :param engine:
        :param path: path to netcdf file
        :return: dataset
        """
        try:
            LOGGER.debug("create dataset from netcdf")
            self.get_s3fs_handle(path)
            return xarray.open_dataset(self.s3fs_file_handle, chunks="auto", engine="h5netcdf")
        except Exception as e:
            LOGGER.error("Could not create dataset: %s", e)
            raise

    def get_rasterio(self, path: str, data_var: str = 'band_data') -> xarray.Dataset:
        """
        create xarray.Dataset from COG, Tiff
        :param data_var:
        :param path:
        :return:
        """
        try:
            LOGGER.debug("create dataset from tiff")
            self.get_s3fs_handle(path)
            ds = xarray.open_rasterio(self.s3fs_file_handle)
            ds = ds.to_dataset(name=data_var)
            ds = ds.chunk(chunks="auto")
            return ds
        except Exception as e:
            LOGGER.error("Could not create dataset: %s", e)
            raise

    def get_mf_list(self, path: str, dates: list = None) -> xarray.Dataset:
        """
        create xarray.Dataset from multiple files
        :param path: path
        :param dates: dates in years to select files for dataset creation
        :return: dataset
        """
        try:
            LOGGER.debug("create dataset from tif")
            s3fs = get_s3fs()
            s3fs_glob = s3fs.glob(path)
            mf_list = []
            if dates is not None:
                for date in dates:
                    for file_handle in s3fs_glob:
                        if file_handle.find(date) != -1:
                            mf_list.append(file_handle)
                            break
            else:
                mf_list = s3fs_glob
            return mf_list
        except Exception as e:
            LOGGER.error("Could not create dataset: %s", e)
            raise

    def slice_by_time(self, ds: xarray.Dataset, time: list) -> xarray.Dataset:
        """
        select data within given time range
        :param ds: dataset
        :param time: time range
        :return: filtered dataset
        """
        if time is None:
            LOGGER.debug("No time range available for subset creation. This step will be skipped.")
            return ds
        try:
            if len(time) > 1:
                return ds.sel(time=slice(time[0], time[-1]))
            else:
                return ds.sel(time=slice(time[0], time[0]))
        except Exception as e:
            LOGGER.error("Could not select time subset: %s", e)
            raise

    def clip_by_geometry(self, ds: xarray.Dataset, geometries: list, crs: str) -> xarray.Dataset:
        """
        clips the dataset to given geojson geometry
        :param geometry_crs: crs of the geometries
        :param ds: dataset
        :param geometries: geometry
        :param crs: crs of dataset
        :return: clipped dataset
        """
        if geometries is None:
            LOGGER.debug("No geometries available for subset creation. This step will be skipped.")
            return ds
        try:
            return ds.rio.write_crs(crs).rio.clip(geometries=geometries, crs=self.get_geometry_crs(geometries))
        except Exception as e:
            LOGGER.error("Could not select spatial subset: %s", e)
            raise

    def get_geometry_crs(self, geometry: list) -> str:
        try:
            return geometry[0]["crs"]
        except Exception:
            LOGGER.debug("No crs for subset geometries found. Apply default: EPSG:4326.")
            return "EPSG:4326"

    def aggregation(self, ds: xarray.Dataset, variable: str, aggregation_type: str, dimensions: list,
                    no_data_value: int = None) -> list:
        """
        calculate aggregation on dataset
        :param no_data_value:
        :param ds: dataset
        :param variable: variable on which to calculate aggregation
        :param aggregation_type: aggregation type
        :param dimensions: name(s) of the dimension(s) to perform aggregation on
        :return: aggregated values for each variable as dictionary
        """
        try:
            if no_data_value is not None:
                ds = ds.where(ds[variable] != no_data_value)
            return self.call_aggregation_method(ds[variable], aggregation_type, dimensions).values
        except Exception as e:
            LOGGER.error("Cannot aggregate data for variable %s: %s", variable, e)
            raise

    def call_aggregation_method(self, ds: xarray.Dataset, aggregation_type: str, dimensions: list) -> xarray.Dataset:
        """
        redirect aggregation calculation to given aggregation type
        :param ds: dataset
        :param aggregation_type: aggregation type
        :param dimensions: name(s) of the dimension(s) to perform aggregation on
        :return: dataset with aggregated values
        """
        aggregation_function_name = "aggregate_" + aggregation_type
        aggregation_function = getattr(self, aggregation_function_name)
        return aggregation_function(ds, dimensions)

    def aggregate_mean(self, ds: xarray.Dataset, dimensions: list) -> ndarray:
        """
        calculate mean of given data array
        :param dimensions:
        :param ds:
        :return: mean
        """
        return ds.mean(dim=dimensions, skipna=True)

    def aggregate_sum(self, ds: xarray.Dataset, dimensions: list) -> ndarray:
        """
        calculate mean of given data array
        :param dimensions:
        :param ds:
        :return: mean
        """
        return ds.sum(dim=dimensions, skipna=True)

    def aggregate_median(self, ds: xarray.Dataset, dimensions: list) -> ndarray:
        """
        calculate mean of given data array
        :param dimensions:
        :param ds:
        :return: mean
        """
        return ds.median(dim=dimensions, skipna=True)

    def aggregate_quantile75(self, ds: xarray.Dataset, dimensions: list) -> Dataset:
        """
        calculate mean of given data array
        :param dimensions:
        :param ds:
        :return: mean
        """
        # chunks of dimensions need to be size 1 for quantile calculation
        for dim in dimensions:
            ds = ds.chunk(chunks={str(dim): -1})
        return ds.quantile(q=0.75, dim=dimensions, skipna=True)

    def aggregate_quantile25(self, ds: xarray.Dataset, dimensions: list) -> Dataset:
        """
        calculate mean of given data array
        :param dimensions:
        :param ds:
        :return: mean
        """
        # chunks of dimensions need to be size 1 for quantile calculation
        for dim in dimensions:
            ds = ds.chunk(chunks={str(dim): -1})
        return ds.quantile(q=0.25, dim=dimensions, skipna=True)

    def aggregate_min(self, ds: xarray.Dataset, dimensions: list) -> ndarray:
        """
        calculate mean of given data array
        :param dimensions:
        :param ds:
        :return: mean
        """
        return ds.min(dim=dimensions, skipna=True)

    def aggregate_max(self, ds: xarray.Dataset, dimensions: list) -> ndarray:
        """
        calculate mean of given data array
        :param dimensions:
        :param ds:
        :return: mean
        """
        return ds.max(dim=dimensions, skipna=True)

    def aggregated_as_str(self, aggregated: list) -> list:
        """
        converts aggregated values to str and rounds to two decimals
        :param aggregated: aggregated
        :return: aggregated as string
        """
        values_as_str = list()
        for value in aggregated:
            values_as_str.append(str(round(float(value), 2)))
        return values_as_str

    def get_coordinate_names(self, ds: xarray.Dataset) -> dict:
        coordinate_names = dict()
        ds_coordinates = list(ds.coords.keys())
        # lat | y | latitude
        if "lat" in ds_coordinates:
            coordinate_names["lat"] = "lat"
        elif "y" in ds_coordinates:
            coordinate_names["lat"] = "y"
        elif "latitude" in ds_coordinates:
            coordinate_names["lat"] = "latitude"
        # long | x | longitude
        if "lon" in ds_coordinates:
            coordinate_names["lon"] = "lon"
        elif "x" in ds_coordinates:
            coordinate_names["lon"] = "x"
        elif "longitude" in ds_coordinates:
            coordinate_names["lon"] = "longitude"
        # time | band
        if "time" in ds_coordinates:
            coordinate_names["time"] = "time"
        elif "band" in ds_coordinates:
            coordinate_names["time"] = "band"
        return coordinate_names

    def get_data_var(self, ds: xarray.Dataset, data_var: str = None) -> str:
        if data_var is None:
            return [i for i in ds.data_vars][0]
        else:
            return data_var
