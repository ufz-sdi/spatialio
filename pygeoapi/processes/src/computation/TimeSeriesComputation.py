from typing import Tuple
import numpy
from src.computation.Computation import Computation
import logging

LOGGER = logging.getLogger(__name__)


class TimeSeriesComputation(Computation):

    def __init__(self):
        super().__init__()

    def start_computation(self, data: dict) -> Tuple[list, list]:
        """
        call necessary functions for computation calculations
        :param data: data dict
        :return: dict with aggregated values for chosen variables
        """

        if data["raster_format"] == "tiff":
            ds = self.get_mfdataset(data["raster_path"], data["time"])
        else:
            ds = self.get_dataset(data["raster_path"])
            ds = self.slice_by_time(ds, data["time"])
            if type(ds.time.values) is numpy.datetime64:
                data["time_steps"].append(str(ds.time.values.astype("datetime64[D]")))
            else:
                for date in ds.time.values:
                    data["time_steps"].append(str(date.astype("datetime64[D]")))
        data_variable = self.get_data_var(ds, data["data_variable"])
        dim1 = str(list(data["subset"].keys())[0])
        dim2 = str(list(data["subset"].keys())[1])
        subset = {
            dim1: [float(data["subset"][dim1])],
            dim2: [float(data["subset"][dim2])]
        }
        return data["time_steps"], self.aggregated_as_str(self.reshape_list(ds.sel(subset, method="nearest")[data_variable].values))

    def reshape_list(self, values: list) -> list:
        reshaped = list()
        for value in values:
            reshaped.append(value[0][0])
        return reshaped
