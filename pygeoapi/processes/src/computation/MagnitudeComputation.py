from src.computation.Computation import Computation

import logging

LOGGER = logging.getLogger(__name__)


class MagnitudeComputation(Computation):

    def __init__(self):
        super().__init__()

    def start_computation(self, data: dict) -> dict:
        """
        call necessary functions for computation calculations
        :param data: data dict
        :return: dict with aggregated values for chosen variables
        """
        ds = self.get_dataset(data["raster_path"])
        ds = self.slice_by_time(ds, data["time"])
        ds = self.clip_by_geometry(ds, data["subset"], data["raster_crs"])
        data_variable = self.get_data_var(ds, data["data_variable"])
        mask = ds[data_variable].where(ds[data_variable] > data["threshold"], data["threshold"]) - ds[data_variable]
        magnitude = mask.sum(skipna=True).values
        LOGGER.debug("magnitude: %s", magnitude)
        intensity = mask.mean(skipna=True).values
        LOGGER.debug("intensity: %s", intensity)
        return {
            "magnitude": round(float(magnitude), 2),
            "intensity": round(float(intensity), 2)
        }
