from datetime import datetime

from src.manager.InputManager import InputManager


class AnomalyInputManager(InputManager):

    def __init__(self):
        super().__init__()

    def get_valid_input(self, data: dict) -> dict:
        """
        get all inputs and check for validity
        :param data: input data
        :return: checked input
        """
        time, time_format = self.check_time(data.get("time"), ["%Y-%m", "%Y"])
        if time_format == "%Y":
            months_ids = [1, 12]
        else:
            months_ids = self.get_months_ids(time, "%Y-%m")
        reference_time, reference_time_format = self.check_time(data.get("reference"), ["%Y"])
        return {
            "raster_path": data.get("raster"),
            "reference_time": reference_time,
            "data_variable": data.get("variable"),
            "raster_crs": data.get("crs"),
            "time": time,
            "months_ids": months_ids,
            "time_format": time_format,
            "subset": self.get_subset_geometries(data),
            "spatial_aggregation_type": self.get_valid_type(data.get("spatial_aggregation_type"), ["mean"]),
            "temporal_aggregation_type": self.get_valid_type(data.get("temporal_aggregation_type"), ["sum"])
        }

    def get_months_ids(self, dates: list, time_format: str) -> list:
        """
        get months as list with each month as number
        :param dates: dates to take chosen months from
        :param time_format: time format
        :return: months as int as list
        """
        months_ids = list()
        for date in dates:
            months_ids.append(int(datetime.strptime(date, time_format).month))
        return months_ids
