from src.manager.InputManager import InputManager


class TimeSeriesInputManager(InputManager):
    def __init__(self):
        pass

    def get_valid_input(self, data: dict) -> dict:
        """
        get all inputs and check for validity
        :param data: input data
        :return: checked input
        """
        if data.get('time') is None:
            time = time_steps = None
            time_format = ''
        elif data.get('raster_format') == 'tiff':
            time, time_steps, time_format = self.tiff_input(data.get('time'))
        else:
            time, time_format = self.check_time(data.get('time'), ['%Y-%m-%d', '%Y-%m', '%Y'])
            time_steps = list()
        return {
            "raster_path": data.get('raster'),
            "raster_format": data.get("raster_format"),
            "data_variable": data.get("variable"),
            "raster_crs": data.get("crs"),
            "subset": data.get("subset"),
            "time_steps": time_steps,
            "time_format": time_format,
            "time": time
        }
