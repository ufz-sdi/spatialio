import os
from datetime import datetime
import json
import logging
import geojson
from typing import Tuple, Optional

LOGGER = logging.getLogger(__name__)


class InputManager:

    def __init__(self):
        pass

    def get_subset_geometries(self, data: dict) -> list:
        """
        get spatial subset from Landkreis, Bundesland or geojson geometries
        :param data: data dict
        :return: spatial subset as geojson
        """
        if data.get("bl_id") is not None:
            return self.get_bundesland(data.get("bl_id"))
        elif data.get("lk_id") is not None:
            return self.get_landkreis(data.get("lk_id"))
        elif data.get("geometry") is not None:
            return self.check_geometries(data.get("geometry"))

    def get_bundesland(self, bl_rs: str) -> list:
        """
        read Bundesland geometry as geojson
        :param bl_rs: Regionalsschlüssel of the Bundesland
        :return: geojson with Bundesland geometry
        """
        try:
            LOGGER.debug("Bundesland id given")
            if int(bl_rs) not in range(1, 17):
                raise ValueError("invalid Bundesland id: " + bl_rs)
            else:
                jsonfile = open("/usr/data/bundesland/" + bl_rs + ".geojson")
                file = json.load(jsonfile)
                geometry = list()
                for feature in file["features"]:
                    geometry.append(self.check_geometry(feature["geometry"]))
                jsonfile.close()
                if not geometry:
                    raise ValueError("no (valid) geometries found for Bundesland id: " + bl_rs)
                return geometry
        except Exception as e:
            raise ValueError("could not extract Bundesland geometry: " + str(e))

    def get_landkreis(self, lk_rs: str) -> list:
        """
        read Landkreis geometry as geojson
        :param lk_rs: Regionalschlüssel of the Landkreis
        :return: geojson with Landkreis geoemtry
        """
        try:
            LOGGER.debug("Landkreis id given")
            bl_rs = lk_rs[0:2]
            jsonfile = open("/usr/data/landkreis/" + bl_rs + "_admin.geojson")
            file = json.load(jsonfile)
            geometry = list()
            for feature in file["features"]:
                if feature["properties"]["RS"] == lk_rs:
                    geometry.append(self.check_geometry(feature["geometry"]))
                    break
            jsonfile.close()
            if not geometry:
                raise ValueError("no (valid) geometries found for Landkreis id: " + lk_rs)
            return geometry
        except Exception as e:
            raise ValueError("could not extract Landkreis geometry: " + str(e))

    def check_geometry(self, geometry: geojson.GeoJSON) -> geojson.GeoJSON:
        """
        check if geojson geometry is valid
        :param geometry: geojson geometry
        :return: input geometry or None
        """
        if geojson.GeoJSON(geometry).is_valid:
            return geometry
        else:
            raise ValueError("geometry is not a valid geojson object" + str(geometry))

    def check_geometries(self, geometries: list) -> list:
        """
        check if geojson geometry is valid
        :param geometries: geometries
        :return: input geometry or None
        """
        for geometry in geometries:
            self.check_geometry(geometry)
        return geometries

    def check_raster(self, path: str, valid_formats: tuple) -> str:
        """
        check if path to raster file is valid
        check if format is .nc or .tif
        :param valid_formats: valid raster file formats
        :param path: path to raster file
        :return: path if valid
        """
        if path is None:
            raise ValueError("no path to raster file given")
        if not path.endswith(valid_formats):
            raise ValueError("wrong file format. Must be one of: " + str(valid_formats))
        if "*" in path:
            if not os.path.exists(os.path.dirname(path)):
                raise ValueError("invalid path to raster directory")
        else:
            if not os.path.exists(path):
                raise ValueError("invalid path to raster")
        return path

    def check_time(self, time: list, valid_formats: list) -> tuple:
        """
        check time format
        :param valid_formats: list of valid formats
        :param time: list of datetime as strings
        :return: time and format or None
        """
        found_format = None
        for date in time:
            for valid_format in valid_formats:
                try:
                    datetime.strptime(date, valid_format)
                    found_format = valid_format
                except ValueError:
                    pass
            if found_format is None:
                raise ValueError("wrong datetime format given")
        return time, found_format

    def get_date_as_int(self, date: datetime, date_format: str) -> int:
        """
        get date as int
        :param date: date
        :param date_format: format
        :return: date
        """
        if date_format == "%Y-%m-%d":
            return date.day
        elif date_format == "%Y-%m":
            return date.month
        elif date_format == "%Y":
            return date.year

    def tiff_input(self, time: list) -> Tuple[list, list, str]:
        """
        checks and gets time subset, all time steps and datetime format
        :param time: time range
        :return: time subset, all time steps and datetime format
        """
        valid_time, time_format = self.check_time(time, ["%Y-%m-%d", "%Y-%m", "%Y"])
        file_dates, new_time_format = self.convert_to_tiff_dates(valid_time, time_format)
        return file_dates, valid_time, new_time_format

    def convert_to_tiff_dates(self, dates: list, date_format: str) -> Tuple[list, str]:
        tiff_dates = list()
        new_date_format = ""
        for date in dates:
            if date_format == "%Y-%m-%d":
                tiff_dates.append("_" + self.convert_date(datetime.strptime(date, date_format), "%Y%m%d") + "_")
                tiff_dates.append("_" + self.convert_date(datetime.strptime(date, date_format), "%Y%m") + "_")
                tiff_dates.append("_" + self.convert_date(datetime.strptime(date, date_format), "%Y") + "_")
                new_date_format = "%Y%m%d"
            elif date_format == "%Y-%m":
                tiff_dates.append("_" + self.convert_date(datetime.strptime(date, date_format), "%Y%m") + "_")
                tiff_dates.append("_" + self.convert_date(datetime.strptime(date, date_format), "%Y") + "_")
                new_date_format = "%Y%m"
            elif date_format == "%Y":
                tiff_dates.append("_" + self.convert_date(datetime.strptime(date, date_format), "%Y") + "_")
                new_date_format = "%Y"
        return tiff_dates, new_date_format

    def convert_date(self, date: datetime, new_format: str) -> str:
        return date.strftime(new_format)

    def get_valid_type(self, value: str, valid_types: list) -> Optional[str]:
        """
        check if a value is of valid type
        :param value:
        :param valid_types:
        :return: value if valid
        """
        if value is None:
            return None
        if value not in valid_types:
            raise ValueError("no valid computation type given: " + value)
        return value
