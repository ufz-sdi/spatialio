from typing import Tuple, List, Any
from src.manager.InputManager import InputManager
from datetime import datetime
from typing import Optional


class AggregationInputManager(InputManager):

    def __init__(self):
        super().__init__()

    def get_valid_input(self, data: dict) -> dict:
        """
        get all inputs and check for validity
        :param data: input data
        :return: checked input
        """
        if data.get("time") is None:
            time = time_steps = None
            time_format = ""
        elif data.get("raster_format") == "tiff":
            time, time_steps, time_format = self.tiff_input(data.get("time"))
        else:
            time, time_format = self.check_time(data.get("time"), ["%Y-%m-%d", "%Y-%m", "%Y"])
            time_steps = list()
        return {
            "raster_path": data.get("raster"),
            "raster_format": data.get("raster_format"),
            "data_variable": data.get("variable"),
            "no_data_value": data.get("no_data_value"),
            "raster_crs": data.get("crs"),
            "time": time,
            "time_steps": time_steps,
            "time_format": time_format,
            "subset": self.get_subset_geometries(data),
            "spatial_aggregation_type": self.get_valid_type(data.get("spatial_aggregation_type"),
                                                            ["mean", "median", "sum", "min", "max",
                                                             "quantile25", "quantile75"])
        }
