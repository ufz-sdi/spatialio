from src.manager.InputManager import InputManager


class MagnitudeInputManager(InputManager):
    def __init__(self):
        pass

    def get_valid_input(self, data: dict) -> dict:
        """
        get all inputs and check for validity
        :param data: input data
        :return: checked input
        """
        time, time_format = self.check_time(data.get("time"), ["%Y-%m", "%Y"])
        return {
            "raster_path": data.get("raster"),
            "data_variable": data.get("variable"),
            "raster_crs": data.get("crs"),
            "time": time,
            "time_format": time_format,
            "subset": self.get_subset_geometries(data),
            "threshold": data.get("threshold")
        }