from src.processor.AggregationProcessor import AggregationProcessor
from src.processor.AnomalyProcessor import AnomalyProcessor
from src.processor.MagnitudeProcessor import MagnitudeProcessor
import logging

LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

class test_requests():
    def __init__(self):
        self.data = {
            'raster_pre': "/usr/test-data/pre/pre_2021_r.nc",
            'raster_pre_historic': "/usr/test-data/pre/pre_1947_2019_monsum.nc",
            'raster_smi': "/usr/test-data/SMI/SMI_SM_5_25cm_1951_2020_day15.nc",
            'raster_tif': "/usr/test-data/landnutzung/data/*.tif",
            'crs_netcdf': "EPSG:4326",
            'crs_tif': "EPSG:32632",
            'bl_id_sachsen': "14",
            'lk_id_leipzig': "14713",
            'bl_id_rheinland_pfalz': "14",
            'lk_id_mainz': "14713",
            'geometry_point': [{
                "coordinates": [10, 52],
                "crs": "EPSG:4326",
                "type": "Point"}],
            'geometry_polygon': [{
                "coordinates": [[[10, 52], [10, 53], [11, 53], [11, 52], [10, 52]]],
                "crs": "EPSG:4326",
                "type": "Polygon"}],
            'geometry_multi_polygon': [{
                "coordinates": [[
                    [[10, 52], [10, 53], [11, 53], [11, 52], [10, 52]],
                    [[13, 50], [13, 51], [14, 51], [14, 50], [13, 50]]
                ]],
                "crs": "EPSG:4326",
                "type": "MultiPolygon"}],
            'spatial_aggregation_type_value': "mean",
            'spatial_aggregation_type_list_single': ["mean"],
            'spatial_aggregation_type_list_multi': ["mean", "median", "sum", "min", "max", "quantile25", "quantile75"],
            'temporal_aggregation_type_value': "sum"
        }

    def test_aggregation_requests(self):
        """

        :return:
        """

        print("\nTesting process aggregation")
        AP = AggregationProcessor(processor_def={'name': "aggregation"})
        error_count = 0
        error_messages = {}
        error = False

        """
        test simple request
        spatial subset: geometry-point
        time: day
        """
        mimetype, result = AP.execute({
            "raster": self.data["raster_pre"],
            "time": ["2021-04-01"],
            "geometry": self.data["geometry_point"],
            "crs": self.data["crs_netcdf"],
            "spatial_aggregation_type": self.data["spatial_aggregation_type_list_multi"],
            "variables": ["pre"]
        })
        if 'ERROR' in result['values']:
            error = True
            error_count += 1
            error_messages['Test 1'] = str(result['values']['ERROR'])

        """
        test simple request
        spatial subset: geometry-polygon
        time: day
        """
        mimetype, result = AP.execute({
            "raster": self.data["raster_pre"],
            "time": ["2021-04-01"],
            "geometry": self.data["geometry_polygon"],
            "crs": self.data["crs_netcdf"],
            "spatial_aggregation_type": self.data["spatial_aggregation_type_list_multi"],
            "variables": ["pre"]
        })
        if 'ERROR' in result['values']:
            error = True
            error_count += 1
            error_messages['Test 2'] = str(result['values']['ERROR'])

        """
        test simple request
        spatial subset: geometry-multipolygon
        time: day
        """
        mimetype, result = AP.execute({
            "raster": self.data["raster_pre"],
            "time": ["2021-04-01"],
            "geometry": self.data["geometry_multi_polygon"],
            "crs": self.data["crs_netcdf"],
            "spatial_aggregation_type": self.data["spatial_aggregation_type_list_multi"],
            "variables": ["pre"]
        })
        if 'ERROR' in result['values']:
            error = True
            error_count += 1
            error_messages['Test 3'] = str(result['values']['ERROR'])

        """
        test simple request
        spatial subset: bl_id
        time: day
        """
        mimetype, result = AP.execute({
            "raster": self.data["raster_pre"],
            "time": ["2021-04-01"],
            "bl_id": self.data["bl_id_sachsen"],
            "crs": self.data["crs_netcdf"],
            "spatial_aggregation_type": self.data["spatial_aggregation_type_list_multi"],
            "variables": ["pre"]
        })
        if 'ERROR' in result['values']:
            error = True
            error_count += 1
            error_messages['Test 4'] = str(result['values']['ERROR'])

        """
        test simple request
        spatial subset: lk_id
        time: day
        """
        mimetype, result = AP.execute({
            "raster": self.data["raster_pre"],
            "time": ["2021-04-01"],
            "lk_id": self.data["lk_id_leipzig"],
            "crs": self.data["crs_netcdf"],
            "spatial_aggregation_type": self.data["spatial_aggregation_type_list_multi"],
            "variables": ["pre"]
        })
        if 'ERROR' in result['values']:
            error = True
            error_count += 1
            error_messages['Test 5'] = str(result['values']['ERROR'])

        """
        test simple request
        spatial subset: lk_id
        time: month
        """
        mimetype, result = AP.execute({
            "raster": self.data["raster_pre"],
            "time": ["2021-04"],
            "lk_id": self.data["lk_id_leipzig"],
            "crs": self.data["crs_netcdf"],
            "spatial_aggregation_type": self.data["spatial_aggregation_type_list_multi"],
            "variables": ["pre"]
        })
        if 'ERROR' in result['values']:
            error = True
            error_count += 1
            error_messages['Test 6'] = str(result['values']['ERROR'])

        """
        test simple request
        spatial subset: lk_id
        time: year
        """
        mimetype, result = AP.execute({
            "raster": self.data["raster_pre"],
            "time": ["2021"],
            "lk_id": self.data["lk_id_leipzig"],
            "crs": self.data["crs_netcdf"],
            "spatial_aggregation_type": self.data["spatial_aggregation_type_list_multi"],
            "variables": ["pre"]
        })
        if 'ERROR' in result['values']:
            error = True
            error_count += 1
            error_messages['Test 7'] = str(result['values']['ERROR'])

        """
        test simple request
        spatial subset: lk_id
        time: two dates
        """
        mimetype, result = AP.execute({
            "raster": self.data["raster_pre"],
            "time": ["2021-04-01", "2021-05-01"],
            "lk_id": self.data["lk_id_leipzig"],
            "crs": self.data["crs_netcdf"],
            "spatial_aggregation_type": self.data["spatial_aggregation_type_list_multi"],
            "variables": ["pre"]
        })
        if 'ERROR' in result['values']:
            error = True
            error_count += 1
            error_messages['Test 8'] = str(result['values']['ERROR'])

        """
        test simple request
        format: tiff
        spatial subset: lk_id
        time: years
        """
        mimetype, result = AP.execute({
            "raster": self.data["raster_tif"],
            "time": ["2016", "2018"],
            "lk_id": self.data["lk_id_leipzig"],
            "crs": self.data["crs_tif"],
            "spatial_aggregation_type": self.data["spatial_aggregation_type_list_single"]
        })
        if 'ERROR' in result['values']:
            error = True
            error_count += 1
            error_messages['Test 9'] = str(result['values']['ERROR'])

        print('\n' + '\033[91m' + str(error_count) + ' error(s) occurred at tests' + '\033[0m')
        if error is True:
            print('Error messages:')
            for test_number in error_messages:
                print(test_number + ': ' + error_messages[test_number])

    def test_anomaly_requests(self):
        """

        :return:
        """

        print("\nTesting process anomaly")
        AP = AnomalyProcessor(processor_def={'name': "anomaly"})
        error_count = 0
        error_messages = {}
        error = False

        """
        test simple request
        spatial subset: geometry-point
        time: day
        """
        mimetype, result = AP.execute({
            "raster": self.data["raster_pre"],
            "reference": ["1961", "1990"],
            "time": ["2021-04"],
            "geometry": self.data["geometry_point"],
            "crs": self.data["crs_netcdf"],
            "variables": ["pre"],
            "spatial_aggregation_type": "mean",
            "temporal_aggregation_type": "sum"
        })
        if 'ERROR' in result['values']:
            error = True
            error_count += 1
            error_messages['Test 1'] = str(result['values']['ERROR'])

        """
        test simple request
        spatial subset: geometry-polygon
        time: month
        """
        mimetype, result = AP.execute({
            "raster": self.data["raster_pre"],
            "reference": ["1961", "1990"],
            "time": ["2021-04"],
            "geometry": self.data["geometry_polygon"],
            "crs": self.data["crs_netcdf"],
            "variables": ["pre"],
            "spatial_aggregation_type": "mean",
            "temporal_aggregation_type": "sum"
        })
        if 'ERROR' in result['values']:
            error = True
            error_count += 1
            error_messages['Test 2'] = str(result['values']['ERROR'])

        """
        test simple request
        spatial subset: geometry-multipolygon
        time: month
        """
        mimetype, result = AP.execute({
            "raster": self.data["raster_pre"],
            "reference": ["1971", "2000"],
            "time": ["2021-04"],
            "geometry": self.data["geometry_multi_polygon"],
            "crs": self.data["crs_netcdf"],
            "variables": ["pre"],
            "spatial_aggregation_type": "mean",
            "temporal_aggregation_type": "sum"
        })
        if 'ERROR' in result['values']:
            error = True
            error_count += 1
            error_messages['Test 3'] = str(result['values']['ERROR'])

        """
        test simple request
        spatial subset: bl_id
        time: month
        """
        mimetype, result = AP.execute({
            "raster": self.data["raster_pre"],
            "reference": ["1971", "2000"],
            "time": ["2021-04"],
            "bl_id": self.data["bl_id_sachsen"],
            "crs": self.data["crs_netcdf"],
            "variables": ["pre"],
            "spatial_aggregation_type": "mean",
            "temporal_aggregation_type": "sum"
        })
        if 'ERROR' in result['values']:
            error = True
            error_count += 1
            error_messages['Test 4'] = str(result['values']['ERROR'])

        """
        test simple request
        spatial subset: lk_id
        time: month
        """
        mimetype, result = AP.execute({
            "raster": self.data["raster_pre"],
            "reference": ["1981", "2010"],
            "time": ["2021-04"],
            "lk_id": self.data["lk_id_leipzig"],
            "crs": self.data["crs_netcdf"],
            "variables": ["pre"],
            "spatial_aggregation_type": "mean",
            "temporal_aggregation_type": "sum"
        })
        if 'ERROR' in result['values']:
            error = True
            error_count += 1
            error_messages['Test 5'] = str(result['values']['ERROR'])

        """
        test simple request
        spatial subset: lk_id
        time: year
        """
        mimetype, result = AP.execute({
            "raster": self.data["raster_pre"],
            "reference": ["1981", "2010"],
            "time": ["2021"],
            "lk_id": self.data["lk_id_leipzig"],
            "crs": self.data["crs_netcdf"],
            "variables": ["pre"],
            "spatial_aggregation_type": "mean",
            "temporal_aggregation_type": "sum"
        })
        if 'ERROR' in result['values']:
            error = True
            error_count += 1
            error_messages['Test 6'] = str(result['values']['ERROR'])

        """
        test simple request
        spatial subset: lk_id
        time: two dates
        """
        mimetype, result = AP.execute({
            "raster": self.data["raster_pre"],
            "reference": ["1981", "2010"],
            "time": ["2021-04", "2021-05"],
            "lk_id": self.data["lk_id_leipzig"],
            "crs": self.data["crs_netcdf"],
            "variables": ["pre"],
            "spatial_aggregation_type": "mean",
            "temporal_aggregation_type": "sum"
        })
        if 'ERROR' in result['values']:
            error = True
            error_count += 1
            error_messages['Test 7'] = str(result['values']['ERROR'])

        print('\n' + '\033[91m' + str(error_count) + ' error(s) occurred at tests' + '\033[0m')
        if error is True:
            print('Error messages:')
            for test_number in error_messages:
                print(test_number + ': ' + error_messages[test_number])

    def test_magnitude_requests(self):
        """

        :return:
        """

        print("\nTesting process magnitude")
        AP = MagnitudeProcessor(processor_def={'name': "magnitude"})
        error_count = 0
        error_messages = {}
        error = False

        """
        test simple request
        spatial subset: geometry-point
        time: day
        """
        mimetype, result = AP.execute({
            "raster": self.data["raster_smi"],
            "time": ["2021-04"],
            "geometry": self.data["geometry_point"],
            "crs": self.data["crs_netcdf"],
            "variables": ["SMI"],
            "threshold": 0.2
        })
        if 'ERROR' in result['values']:
            error = True
            error_count += 1
            error_messages['Test 1'] = str(result['values']['ERROR'])

        """
        test simple request
        spatial subset: geometry-polygon
        time: month
        """
        mimetype, result = AP.execute({
            "raster": self.data["raster_smi"],
            "time": ["2021-04"],
            "geometry": self.data["geometry_polygon"],
            "crs": self.data["crs_netcdf"],
            "variables": ["SMI"],
            "threshold": 0.2
        })
        if 'ERROR' in result['values']:
            error = True
            error_count += 1
            error_messages['Test 2'] = str(result['values']['ERROR'])

        """
        test simple request
        spatial subset: geometry-multipolygon
        time: month
        """
        mimetype, result = AP.execute({
            "raster": self.data["raster_smi"],
            "time": ["2021-04"],
            "geometry": self.data["geometry_multi_polygon"],
            "crs": self.data["crs_netcdf"],
            "variables": ["SMI"],
            "threshold": 0.2
        })
        if 'ERROR' in result['values']:
            error = True
            error_count += 1
            error_messages['Test 3'] = str(result['values']['ERROR'])

        """
        test simple request
        spatial subset: bl_id
        time: month
        """
        mimetype, result = AP.execute({
            "raster": self.data["raster_smi"],
            "time": ["2021-04"],
            "bl_id": self.data["bl_id_sachsen"],
            "crs": self.data["crs_netcdf"],
            "variables": ["SMI"],
            "threshold": 0.2
        })
        if 'ERROR' in result['values']:
            error = True
            error_count += 1
            error_messages['Test 4'] = str(result['values']['ERROR'])

        """
        test simple request
        spatial subset: lk_id
        time: month
        """
        mimetype, result = AP.execute({
            "raster": self.data["raster_smi"],
            "time": ["2021-04"],
            "lk_id": self.data["lk_id_leipzig"],
            "crs": self.data["crs_netcdf"],
            "variables": ["SMI"],
            "threshold": 0.2
        })
        if 'ERROR' in result['values']:
            error = True
            error_count += 1
            error_messages['Test 5'] = str(result['values']['ERROR'])

        """
        test simple request
        spatial subset: lk_id
        time: year
        """
        mimetype, result = AP.execute({
            "raster": self.data["raster_smi"],
            "time": ["2021"],
            "lk_id": self.data["lk_id_leipzig"],
            "crs": self.data["crs_netcdf"],
            "variables": ["SMI"],
            "threshold": 0.2
        })
        if 'ERROR' in result['values']:
            error = True
            error_count += 1
            error_messages['Test 6'] = str(result['values']['ERROR'])

        """
        test simple request
        spatial subset: lk_id
        time: two dates
        """
        mimetype, result = AP.execute({
            "raster": self.data["raster_smi"],
            "time": ["2021-04", "2021-05"],
            "lk_id": self.data["lk_id_leipzig"],
            "crs": self.data["crs_netcdf"],
            "variables": ["SMI"],
            "threshold": 0.2
        })
        if 'ERROR' in result['values']:
            error = True
            error_count += 1
            error_messages['Test 7'] = str(result['values']['ERROR'])

        print('\n' + '\033[91m' + str(error_count) + ' error(s) occurred at tests' + '\033[0m')
        if error is True:
            print('Error messages:')
            for test_number in error_messages:
                print(test_number + ': ' + error_messages[test_number])
