import unittest
from src.processor.AnomalyProcessor import AnomalyProcessor


class TestAnomalyProcessor():
    def __init__(self):
        pass

    def test_execute(self):

        print('Difference between data from Deutscher Wetterdienst (DWD) (https://www.dwd.de/DE/leistungen/zeitreihen/zeitreihen.html#buehneTop) and Aggregation-API')
        print('Results: \nDWD - API \nabsolut: difference between absolute results in millimeter \nrelativ: difference between relativ results')

        processor = AnomalyProcessor({'name': 'anomaly'})
        base_input = {
            'spatial_aggregation_type': "mean",
            'temporal_aggregation_type': "sum",
            'crs': "EPSG:4326",
            'raster': "/usr/test-data/pre/pre_2021_r.nc",
            'variables': ["pre"],
            'bl_id': "14"
        }
        absolutes_diff = dict()
        relatives_diff = dict()

        # Juni 2021
        # Sachsen
        # 1961-1990
        base_input['reference'] = ["1961", "1990"]
        base_input['time'] = ["2021-06"]
        mime_type, result = processor.execute(base_input)
        # DWD-Ergebnisse
        # absolute: -1.5
        # relative: -2.0
        absolutes_diff['1'] = -1.5 - float(result['values']['pre']['absolute'])
        relatives_diff['1'] = -2.0 - float(result['values']['pre']['relative'])
        print('Test 1 (Juni, Referenz: 1961-1990): \nabsolut: {}\nrelativ: {}'.format(absolutes_diff['1'], relatives_diff['1']))

        # Juli 2021
        # Sachsen
        # 1961-1990
        base_input['reference'] = ["1961", "1990"]
        base_input['time'] = ["2021-07"]
        mime_type, result = processor.execute(base_input)
        # DWD-Ergebnisse
        # absolute: 53.0
        # relative: 77.0
        absolutes_diff['2'] = 53.0 - float(result['values']['pre']['absolute'])
        relatives_diff['2'] = 77.0 - float(result['values']['pre']['relative'])
        print('Test 2 (Juli, Referenz: 1961-1990): \nabsolut: {}\nrelativ: {}'.format(absolutes_diff['2'], relatives_diff['2']))

        # August 2021
        # Sachsen
        # 1961-1990
        base_input['reference'] = ["1961", "1990"]
        base_input['time'] = ["2021-08"]
        mime_type, result = processor.execute(base_input)
        # DWD-Ergebnisse
        # absolute: 61.0
        # relative: 79.1
        absolutes_diff['3'] = 61.0 - float(result['values']['pre']['absolute'])
        relatives_diff['3'] = 79.1 - float(result['values']['pre']['relative'])
        print('Test 3 (August, Referenz: 1961-1990): \nabsolut: {}\nrelativ: {}'.format(absolutes_diff['3'], relatives_diff['3']))

        # Juni 2021
        # Sachsen
        # 1971-2000
        base_input['reference'] = ["1971", "2000"]
        base_input['time'] = ["2021-06"]
        mime_type, result = processor.execute(base_input)
        # DWD-Ergebnisse
        # absolute: -0.2
        # relative: -0.3
        absolutes_diff['4'] = -0.2 - float(result['values']['pre']['absolute'])
        relatives_diff['4'] = -0.3 - float(result['values']['pre']['relative'])
        print('Test 4 (Juni, Referenz: 1971-2000): \nabsolut: {}\nrelativ: {}'.format(absolutes_diff['4'], relatives_diff['4']))

        # Juli 2021
        # Sachsen
        # 1971-2000
        base_input['reference'] = ["1971", "2000"]
        base_input['time'] = ["2021-07"]
        mime_type, result = processor.execute(base_input)
        # DWD-Ergebnisse
        # absolute: 42.4
        # relative: 53.3
        absolutes_diff['5'] = 42.2 - float(result['values']['pre']['absolute'])
        relatives_diff['5'] = 53.3 - float(result['values']['pre']['relative'])
        print('Test 5 (Juli, Referenz: 1971-2000): \nabsolut: {}\nrelativ: {}'.format(absolutes_diff['5'], relatives_diff['5']))

        # August 2021
        # Sachsen
        # 1971-2000
        base_input['reference'] = ["1971", "2000"]
        base_input['time'] = ["2021-08"]
        mime_type, result = processor.execute(base_input)
        # DWD-Ergebnisse
        # absolute: 62.0
        # relative: 81.6
        absolutes_diff['6'] = 62.0 - float(result['values']['pre']['absolute'])
        relatives_diff['6'] = 81.6 - float(result['values']['pre']['relative'])
        print('Test 6 (August, Referenz: 1971-2000): \nabsolut: {}\nrelativ: {}'.format(absolutes_diff['6'], relatives_diff['6']))

        # Juni 2021
        # Sachsen
        # 1981-2010
        base_input['reference'] = ["1981", "2010"]
        base_input['time'] = ["2021-06"]
        mime_type, result = processor.execute(base_input)
        # DWD-Ergebnisse
        # absolute: 6.5
        # relative: 9.5
        absolutes_diff['7'] = 6.5 - float(result['values']['pre']['absolute'])
        relatives_diff['7'] = 9.5 - float(result['values']['pre']['relative'])
        print('Test 7 (Juni, Referenz: 1981-2010): \nabsolut: {}\nrelativ: {}'.format(absolutes_diff['7'], relatives_diff['7']))

        # Juli 2021
        # Sachsen
        # 1981-2010
        base_input['reference'] = ["1981", "2010"]
        base_input['time'] = ["2021-07"]
        mime_type, result = processor.execute(base_input)
        # DWD-Ergebnisse
        # absolute: 36.2
        # relative: 42.2
        absolutes_diff['8'] = 36.2 - float(result['values']['pre']['absolute'])
        relatives_diff['8'] = 42.2 - float(result['values']['pre']['relative'])
        print('Test 8 (Juli, Referenz: 1981-2010): \nabsolut: {}\nrelativ: {}'.format(absolutes_diff['8'], relatives_diff['8']))

        # August 2021
        # Sachsen
        # 1981-2010
        base_input['reference'] = ["1981", "2010"]
        base_input['time'] = ["2021-08"]
        mime_type, result = processor.execute(base_input)
        # DWD-Ergebnisse
        # absolute: 54.3
        # relative: 64.9
        absolutes_diff['9'] = 54.3 - float(result['values']['pre']['absolute'])
        relatives_diff['9'] = 64.9 - float(result['values']['pre']['relative'])
        print('Test 9 (August, Referenz: 1981-2010): \nabsolut: {}\nrelativ: {}'.format(absolutes_diff['9'], relatives_diff['9']))
