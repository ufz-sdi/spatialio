from src.processor.MagnitudeProcessor import MagnitudeProcessor


class TestMagnitudeProcessor():
    def test_execute(self):
        print('Difference between yearly data from Friedrich Boeing (from 1951 to 2020) and Aggregation-API')

        fb_smi = [
            0.008415426,
            0.02263387,
            0.03546358,
            0.05863124,
            0.004410178,
            0.002256489,
            0.00268538,
            0.001698563,
            0.06838171,
            0.03551951,
            0.005353858,
            0.02043927,
            0.04421609,
            0.03544518,
            0.002595129,
            0.0003054117,
            0.001047025,
            0.0021652,
            0.01109738,
            0.0163194,
            0.02674633,
            0.05061634,
            0.03832663,
            0.01046291,
            0.01104315,
            0.06467436,
            0.01308119,
            0.002903857,
            0.003759283,
            0.0009111639,
            0.002317965,
            0.02010563,
            0.02822107,
            0.004906491,
            0.009509503,
            0.004211781,
            0.0001527099,
            0.006757132,
            0.02358905,
            0.009411129,
            0.02234091,
            0.01674005,
            0.006039499,
            0.003712352,
            0.004935254,
            0.03317672,
            0.01321691,
            0.009861791,
            0.00952244,
            0.007557745,
            0.004217932,
            0.0009408391,
            0.04629596,
            0.007022667,
            0.004987645,
            0.01947995,
            0.01625805,
            0.009395423,
            0.01468225,
            0.005043138,
            0.03574145,
            0.01364964,
            0.004197548,
            0.03142536,
            0.03081298,
            0.01859396,
            0.03664127,
            0.08299832,
            0.04897596,
            0.0433318]

        processor = MagnitudeProcessor({'name': 'magnitude'})
        base_input = {
            'crs': "EPSG:4326",
            'raster': "/usr/test-data/SMI/SMI_SM_5_25cm_1951_2020_day15.nc",
            'variables': ["SMI"],
            'threshold': 0.2
        }

        api_smi = {}
        count = 0
        for year in range(1951, 2020):
            base_input['time'] = [str(year)]
            mime_type, result = processor.execute(base_input)
            api_smi[str(year)] = {'comparison': fb_smi[count],
                                  'api_result': result['values']['SMI']['intensity'],
                                  'absolute_difference': abs(fb_smi[count] - result['values']['SMI']['intensity'])}
            count += 1

        for year in api_smi:
            print('Year {}: \nComparison data: {} \nAPI result: {} \nAbsolute difference: {}'.format(
                year, api_smi[year]['comparison'], api_smi[year]['api_result'], api_smi[year]['absolute_difference']))
