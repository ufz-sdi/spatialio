from tests.test_requests import test_requests
from tests.test_AnomalyProcessor import TestAnomalyProcessor
from tests.test_MagnitudeProcessor import TestMagnitudeProcessor


"""
Tests requests
"""
print("Test processes with different inputs")
TR = test_requests()
TR.test_aggregation_requests()
TR.test_anomaly_requests()
TR.test_magnitude_requests()

print("Test results of anomaly and magnitude processes")
TestAnomalyProcessor().test_execute()
print("\n")
TestMagnitudeProcessor().test_execute()
