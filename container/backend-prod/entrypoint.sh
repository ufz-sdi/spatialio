#!/usr/bin/env bash
service nginx start

gunicorn gdi-admin.wsgi:application --bind 0.0.0.0:8080
