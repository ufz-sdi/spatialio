#!/usr/bin/env bash

if [ ! -d "/home/node/app/node_modules" ]; then
  echo "install node_modules..."
  npm install
fi

npm run dev