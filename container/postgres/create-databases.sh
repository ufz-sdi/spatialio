#!/bin/bash

set -e
set -u

function create_user_and_database() {
	local database=$1

	echo "  Creating user and database '$database'"

	psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
	    CREATE USER $database;
	    CREATE DATABASE $database;
	    GRANT ALL PRIVILEGES ON DATABASE $database TO $database;
EOSQL

}

declare -a databases=("$DJANGO_DATABASE" "$FROST_DATABASE" "$GEONETWORK_DB")

for db in "${databases[@]}"; do
  create_user_and_database $db
done
echo "Databases created"


psql -d $DJANGO_DATABASE -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE SCHEMA geoserver;

    CREATE EXTENSION postgis SCHEMA geoserver;

    ALTER DATABASE django SET search_path = public,geoserver,postgis;
EOSQL


psql -d $FROST_DATABASE -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE EXTENSION postgis;
EOSQL

echo "PostGIS installed"
