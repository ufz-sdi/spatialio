#!/bin/sh


# change default password
USERS=$GEOSERVER_DATA_DIR/security/usergroup/default/users.xml
cp -rf $GEOSERVER_HOME/data_dir/* $GEOSERVER_DATA_DIR
envsubst < $USERS.tmpl > $USERS

# mount S3 buckets

#MINIO_PASS_FILE=/etc/passw-s3fs
#
#echo $MINIO_USERNAME:$MINIO_PASSWORD > $MINIO_PASS_FILE && chmod 600 $MINIO_PASS_FILE
#
#for bucket in $MINIO_BUCKETS; do
#  BUCKET_DIR="/mnt/$bucket"
#  echo $BUCKET_DIR
#  mkdir -p $BUCKET_DIR;
#  s3fs $bucket $BUCKET_DIR -o passwd_file=$MINIO_PASS_FILE,umask=100,use_path_request_style,url=$MINIO_ENDPOINT
#done


# settings for TLS

if [ -n "$GEOSERVER_PROXY_URL" ]; then
  export PROXY_BASE_URL=${GEOSERVER_PROXY_URL}
fi

if [ -n "$GEOSERVER_CSRF_WHITELIST" ]; then
  export GEOSERVER_CSRF_WHITELIST=${GEOSERVER_CSRF_WHITELIST}
fi

# create directory for storing netcdf-hidden-files

mkdir -p ${GEOSERVER_DATA_DIR}/netcdf


# set system resources
# set addititonal configuration to enable netcdf-scale-factor and save netcdf-hidden-files in central directory

export GEOSERVER_OPTS="-Djava.awt.headless=true -server -Xms${INITIAL_MEMORY} -Xmx${MAXIMUM_MEMORY} \
       -Dorg.geotools.coverage.io.netcdf.enhance.ScaleMissing=true -DNETCDF_DATA_DIR=${GEOSERVER_DATA_DIR}/netcdf"

## Prepare the JVM command line arguments
export JAVA_OPTS="${JAVA_OPTS} ${GEOSERVER_OPTS}"


## remove tomcat admin interface for security reasons
for i in ROOT docs examples host-manager manager; do
    FOLDER_PATH="${CATALINA_HOME}/webapps/${i}"
    if [  -d "${FOLDER_PATH}" ]; then
        rm -r "${FOLDER_PATH}"
    fi;
done ;

exec "$@"
