#!/usr/bin/env bash

echo $MATOMO_ID

if ! [ -z "$MATOMO_ID" ]
then
  sed -i -e "s/const matomo_id = 0/const matomo_id = $MATOMO_ID/g" /home/node/app/index.html
else
  echo "Not setting Matomo-ID."
fi

if ! [ -z "$VUE_APP_ROUTE_ALIAS" ]
then
  sed -i -e "s/const routeAlias = ''/const routeAlias = '$VUE_APP_ROUTE_ALIAS'/g" /home/node/app/index.html
fi

npx http-server ./dist -p 3000 --proxy http://localhost:3000/$VITE_APP_ROUTE/?