
<a href="https://www.iosb.fraunhofer.de/de/projekte-produkte/frostserver.html" target="_blank">
<img src="https://www.iosb.fraunhofer.de/de/projekte-produkte/frostserver/jcr:content/contentPar/sectioncomponent/sectionParsys/textwithinlinedimage/imageComponent1/image.img.4col.png/1651077897270/FROST-Server.png" alt="FROST-Logo" height="60px" />
</a>


### Getting started with FROST-Server and the Sensorthings API (STA)

#### General

- FROST-Server/STA is made for the management and visualization of stationary data with timeseries or trajectory data
- Spatial.IO can use the [FROST-Server of the Fraunhofer IOSB](https://www.iosb.fraunhofer.de/de/projekte-produkte/frostserver.html)
- The official documentation can be found here: https://fraunhoferiosb.github.io/FROST-Server/

<br>
<div style="text-align: center" align="left">
  <img src="../../img/sta-data-example.png" alt="sta-data-example" height="240px" />
  <p style="color: lightgray"><i>Example for visualization of STA data with spatial.IO</i></p>
</div>
<br>
<br>

#### Integrating Data in the Frontend

- Spatial.IO has two filter-options on a STA-endpoint:
  - You can select data by things with a certain property, e.g. 'projects'

      `{"projects": ["CSO_DRESDEN"], ...}`

  - You can also select observed-properties with a certain property, e.g. 'CATEGORY'


      `{..., "CATEGORY": "Industrial"}`

- Both filter-options will be combined if necessary
- Importing the data is possible in many ways

<br>

#### Labeling Data in the Frontend

- you can provide a german and english name for observed-properties with this properties, otherwise, the standard field 'name' will be used:


      `{"name_de": "Gabapentin Durchschnittsjahr SSP5-8.5", "name_en": "Gabapentin average year SSP5-8.5", ...}`

- you can show all additional properties of a thing in the frontend
  - you have to select the checkbox in the ThingProperty-FormField
  - you can also set a german and english label

<br>

#### Data Import

> spatial.IO supports any accessible STA-endpoint, so running a FROST-Server in this setup is not necessary in every case.
> For large time series data, it is highly recommended to use an advanced TSM-infrastructure like [time.IO](https://codebase.helmholtz.cloud/ufz-tsm).

- this repository contains two simple scripts for parsing a csv-file and making HTTP-Requests against STA for creating datasets in FROST-Server
  - this should be done locally with a container using the production-database, because execution-time is otherwise very slow for larger CSV-files
  - the CSV-file should have at least these information: ` STATION_ID | STATION_NAME | METRIK | VALUE | TIMESTAMP | UNIT | LAT | LON `

<br>
<div style="text-align: center" align="left">
  <img src="../../img/test-csv.png" alt="sta-test-data" height="240px" />
  <p style="color: lightgray"><i>Example for a compatible csv-file, which can be parsed without much configuration</i></p>
</div>
<br>


#### Limitations
- currently no support for the entities _FeatureOfInterest_, _HistoricalLocations_ and _MultiDatastream_
