from django.http import HttpResponse, HttpResponseServerError   # noqa

import psutil   # noqa


def get_health(request):

    memory_stats = psutil.virtual_memory()
    disk_stats = psutil.disk_usage('/')

    if disk_stats.percent > 80 or (memory_stats.percent > 80):
        is_healthy = False
    else:
        is_healthy = True

    if is_healthy:
        return HttpResponse('ok')
    else:
        return HttpResponseServerError('Resources critical')