from django.db import models    # noqa
from django.contrib.auth.models import User    # noqa
from django.contrib.postgres.fields import ArrayField    # noqa

import re

from django.core.exceptions import ValidationError    # noqa


METADATA_LANGUAGES = (
    (None, ''),
    ('bg', 'Bulgarian'),
    ('cs', 'Czech'),
    ('da', 'Danish'),
    ('nl', 'Dutch'),
    ('en', 'English'),
    ('et', 'Estonian'),
    ('fi', 'Finnish'),
    ('de', 'German'),
    ('el', 'Greek'),
    ('fr', 'French'),
    ('hr', 'Croatian'),
    ('hu', 'Hungarian'),
    ('it', 'Italien'),
    ('ga', 'Irish'),
    ('lv', 'Latvian'),
    ('lt', 'Lithuanian'),
    ('mt', 'Maltese'),
    ('pl', 'Polish'),
    ('pt', 'Portuguese'),
    ('ro', 'Romanian'),
    ('sk', 'Slovak'),
    ('sl', 'Slovenian'),
    ('es', 'Spanish'),
    ('sv', 'Swedish'),
)


def validate_file_path(value):
    if not re.match(r'^[A-Za-z0-9-_./]*$', value):
        raise ValidationError('Only lowercase-letters, numbers and / - _ .  are allowed.')
    for invalid_combination in ["..", "//", "/.", "./"]:
        if invalid_combination in value:
            raise ValidationError("Please enter a valid file path")
    for invalid_start in [".", "/", "-"]:
        if value.startswith(invalid_start):
            raise ValidationError("Please enter a valid file path")
    if not value.endswith('.jsonld'):
        raise ValidationError("FilePath must end with '.jsonld'")


class Organization(models.Model):
    name = models.CharField(max_length=1000)
    ror = models.CharField(max_length=100, unique=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.name + " (" + self.ror + ")"


class Author(models.Model):
    given_name = models.CharField(max_length=100)
    family_name = models.CharField(max_length=100)
    email = models.EmailField(unique=True)
    orcid = models.CharField(max_length=100, unique=True)
    affiliation = models.ForeignKey('Organization', on_delete=models.CASCADE)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.given_name + " " + self.family_name + " (" + self.orcid + ")"


class License(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class DataFormat(models.Model):
    name = models.CharField(max_length=100)
    mime_type = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class MetadataRecord(models.Model):
    bucket = models.ForeignKey("main.Bucket", on_delete=models.CASCADE)
    file_path = models.CharField(max_length=4000, unique=True, validators=[validate_file_path])
    dataset_path = models.CharField(max_length=4000, null=True, blank=True)
    geonetwork_uuid = models.UUIDField(blank=True, null=True)                           # readonly
    is_published = models.BooleanField("Publish record", default=False, help_text='If a record is published, it will be visible without needing to be logged in to Geonetwork')                                 # readonly
    is_doi_check_passed = models.BooleanField("DOI-Check passed", default=False)
    is_doi_requested = models.BooleanField("Request DOI", default=False, help_text='A DOI can only be requested for public records')
    is_doi_approved = models.BooleanField("Approve DOI", default=False)                           # readonly
    doi_notification = models.CharField(max_length=4000, blank=True, null=True, help_text='When a DOI is requested, it will be checked for compliance before accepting.')
    doi = models.URLField(max_length=1000, blank=True, null=True)  # is readonly
    record_id = models.CharField(max_length=32, blank=True, null=True, unique=True)     # readonly
    publisher = models.ForeignKey(Organization, on_delete=models.CASCADE)
    version = models.CharField(max_length=12, blank=True, null=True, help_text='Changing this field will overwrite any previous version. For a new version, create a new Metadata Record.')
    language = models.CharField(max_length=20, choices=METADATA_LANGUAGES, default='en', null=True)
    format = models.ForeignKey(DataFormat, on_delete=models.CASCADE)
    license = models.ForeignKey(License, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    published_at = models.DateField(blank=True, null=True)
    abstract = models.TextField(max_length=4000)
    keywords = ArrayField(models.CharField(max_length=1000, blank=True, null=True), blank=True, null=True, default=list)

    def __str__(self):
        return self.file_path


class MetadataRecordAuthor(models.Model):
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    record = models.ForeignKey(MetadataRecord, on_delete=models.CASCADE)
