import os
import json
from jinja2 import Template
import requests
from requests.auth import HTTPBasicAuth

from metadata.models import MetadataRecord, MetadataRecordAuthor # noqa


class GeonetworkApi:

    def __init__(self):
        self.session = requests.Session()
        self.base_url = os.environ.get("GEONETWORK_URL")
        self.auth = HTTPBasicAuth(os.environ.get("GEONETWORK_USERNAME"), os.environ.get("GEONETWORK_PASSWORD"))

        try:
            # set cookie for XSRF protection
            response = self.session.post('{}eng/info?type=me'.format(self.base_url))

            xsrf_token = response.cookies.get("XSRF-TOKEN")

            self.headers = {
                'accept': 'application/json',
                "X-XSRF-TOKEN": xsrf_token
            }
        except Exception as e:
            print('Cannot connect to GeoNetwork: ' + str(e))

        self.logs = []

        self.group_url = '{}api/groups'.format(self.base_url)

    def log(self, log):
        self.logs.append(str(log))

    def create_record(self, record: MetadataRecord):
        xml = _create_xml_file(record)
        return self._make_api_request(xml, record.bucket.geonetwork_group, "NOTHING", record.is_published)

    def update_record(self, record: MetadataRecord):
        xml = _create_xml_file(record)
        return self._make_api_request(xml, record.bucket.geonetwork_group, "OVERWRITE", record.is_published)

    def _make_api_request(self, xml, geonetwork_group, uuid_processing, publish=False):
        self.headers['Content-Type'] = "application/xml"

        url = '{}api/records?metadataType=METADATA&recursiveSearch=false&publishToAll={}&assignToCatalog=true&uuidProcessing={}&rejectIfInvalid=false&transformWith=_none_&group={}'

        if geonetwork_group:
            try:
                response = self.session.put(
                    url.format(self.base_url, publish, uuid_processing, geonetwork_group),
                    headers=self.headers,
                    data=xml.encode('utf-8'),
                    auth=self.auth
                )

                result = json.loads(response.content)
                if "metadataInfos" in result:
                    metadata = result["metadataInfos"]
                    key = list(metadata.keys())[0]
                    content = metadata[key][0]

                    return content["uuid"]
            except Exception as e:
                self.log('Request to Geonetwork failed: ' + str(e))
                return None

        return None

    def delete_record(self, uuid):
        delete_url = "{}api/records?uuids={}&withBackup=false".format(self.base_url, uuid)
        try:
            response = self.session.delete(delete_url, headers=self.headers, auth=self.auth)
            self.log('Deleted Geonetwork-record: ' + uuid)
        except Exception as e:
            self.log('Deleting Geonetwork-record failed: ' + str(e))

    def create_group(self, group_name):
        self.headers["Content-Type"] = "application/json"

        new_group_id = self.get_new_id_if_group_not_exists(group_name)

        if not new_group_id:
            print("group-name already exists")
            return None

        json_data = '{"id": ' + str(new_group_id) + ', "name": "' + group_name + '"}'

        try:
            response = self.session.put(
                self.group_url,
                headers=self.headers,
                auth=self.auth,
                data=json_data
            )

            if response.status_code == 201:
                group_id = json.loads(response.content)
                print("Group created successfully: " + str(group_id))
                return group_id
        except Exception as e:
            print('Creating Geonetwork-group failed: ' + str(e))

    def get_new_id_if_group_not_exists(self, group_name):
        response = self.session.get(
            self.group_url,
            headers=self.headers,
            auth=self.auth
        )

        groups = json.loads(response.content)

        for group in groups:
            if group["name"] == group_name:
                return None

        return _get_new_group_id(100, groups)

    def check_doi_preconditions(self, uuid):
        url = "{}api/records/{}/doi/checkPreConditions".format(self.base_url, uuid)
        try:
            response = self.session.get(url, headers=self.headers, auth=self.auth)
            if response.status_code == 200:
                print('DOI compliance check passed : ' + str(uuid))
                return True
            else:
                print('DOI compliance check failed : ' + str(uuid))
                print(response.content)
                return False
        except Exception as e:
            self.log('Checking DOI compliance failed: ' + str(e))
            return False

    def get_doi(self, uuid):
        url = "{}api/records/{}/doi".format(self.base_url, uuid)
        try:
            response = self.session.put(url, headers=self.headers, auth=self.auth)
            if response.status_code == 200 or response.status_code == 201:
                doi_content = json.loads(response.content)
                return doi_content['doiUrl']
            else:
                print('Could not assign DOI: ' + str(uuid))
                print(response.status_code)
                print(response.content)
                return None
        except Exception as e:
            self.log('DOI application failed: ' + str(e))
            return None


def _get_new_group_id(new_group_id, groups):
    for group in groups:
        if new_group_id == group["id"]:
            return _get_new_group_id(new_group_id + 1, groups)
    return new_group_id


def _create_xml_file(record: MetadataRecord):

    with open(os.path.dirname(__file__) + '/geonetwork_template.xml', encoding='utf-8') as template:
        template = Template(template.read())

    download_target = record.record_id
    if record.file_path.endswith('.jsonld'):
        download_target = 'zip/' + record.record_id

    return template.render(
        record=record,
        authors=_get_authors(record),
        download_url=os.environ.get('BACKEND_URL') + '/download/' + download_target
    )

def _get_authors(record: MetadataRecord):
    result = []
    authors = MetadataRecordAuthor.objects.filter(record=record)
    for author in authors:
        result.append(author.author)
    return result