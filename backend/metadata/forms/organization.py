from django.contrib import admin # noqa

from metadata.models import Organization # noqa


class OrganizationAdmin(admin.ModelAdmin):
    model = Organization
    list_display = ("name", "ror")

    fieldsets = [
        (None, {
            'fields': ('name', 'ror', 'created_by'),
        }),
    ]

    def get_readonly_fields(self, request, obj):
        if request.user.is_superuser:
            return []
        return ['created_by']

    def has_change_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True
        if obj:
            return request.user.id == obj.created_by_id
        return True

    def has_delete_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True
        if obj:
            return request.user == obj.created_by_id
        return False

    def save_model(self, request, obj: Organization, form, change):
        if not obj.created_by_id:
            obj.created_by = request.user
        super().save_model(request, obj, form, change)
