from django.contrib import admin # noqa

from metadata.models import Author # noqa


class AuthorAdmin(admin.ModelAdmin):
    model = Author
    list_display = ("family_name", "given_name")
    ordering = ("family_name", "given_name")
    search_fields = ["family_name", "given_name"]

    fieldsets = [
        (None, {
            'fields': (('given_name', 'family_name'), 'email', 'orcid', 'affiliation', 'created_by'),
        }),
    ]

    def get_readonly_fields(self, request, obj):
        if request.user.is_superuser:
            return []
        return ['created_by']

    def has_change_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True
        if obj:
            return request.user.id == obj.created_by_id
        return True

    def has_delete_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True
        if obj:
            return request.user == obj.created_by_id
        return False

    def save_model(self, request, obj: Author, form, change):
        if not obj.created_by_id:
            obj.created_by = request.user
        super().save_model(request, obj, form, change)
