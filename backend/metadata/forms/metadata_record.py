import os
from datetime import datetime

from django.db.models.signals import post_delete # noqa
from django.dispatch import receiver # noqa
from django import forms # noqa

from django.contrib import admin, messages # noqa
from django_tabbed_changeform_admin.admin import DjangoTabbedChangeformAdmin # noqa
from django.core.exceptions import ValidationError # noqa

from metadata.models import Author, MetadataRecord, MetadataRecordAuthor # noqa
from metadata.lib import create_jsonld_content # noqa

from main.models import Bucket # noqa
from main.lib.utils.backend import get_user_groups # noqa
from main.lib.utils.s3_utils import upload_file, delete_file # noqa
from main.lib.utils.file import create_file_identifier, get_folder_name # noqa


class AuthorInlineFormset(forms.BaseInlineFormSet):
    model = MetadataRecordAuthor

    def clean(self):
        count_authors = 0
        for form in self.forms:
            cleaned_data = form.cleaned_data

            if cleaned_data == {}:
                continue    # form is empty and will not be saved. Skip validation.

            if not cleaned_data.get('DELETE', False):
                count_authors += 1

            if count_authors == 0:
                raise ValidationError("A Metadata-Record should have at least one assigned author.")


class AuthorInline(admin.StackedInline):
    model = MetadataRecordAuthor
    formset = AuthorInlineFormset
    min_num = 1
    extra = 0
    classes = ['tab-author-inline']
    autocomplete_fields = ['author']


class MetadataRecordAdmin(DjangoTabbedChangeformAdmin, admin.ModelAdmin):
    model = MetadataRecord
    inlines = [AuthorInline]
    list_display = ('title', 'bucket', 'file_path')

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        # if form for creating new MetadataRecord, set placeholder, otherwise make it readonly (get_readonly_fields)

        placeholders = (
            ('version', 'e.g. 1.0'),
            ('keywords', 'e.g. Drought, Precipitation, Campaign-name'),
        )

        for item in placeholders:
            if not obj:
                form.base_fields[item[0]].widget.attrs['placeholder'] = item[1]
                form.base_fields['file_path'].widget.attrs['placeholder'] = 'e.g. subfolder/metadata.jsonld'
            if obj:
                if not obj.version:
                    form.base_fields[item[0]].widget.attrs['placeholder'] = item[1]

        if "keywords" in form.base_fields:
            form.base_fields["keywords"].widget.attrs.update(size="60")
        return form

    fieldsets = [
        (None, {
            'fields': ('bucket', 'file_path', 'dataset_path', 'geonetwork_uuid', 'record_id'),
            'classes': ('tab-basic',),
        }),
        (None, {
            'fields': ('title', 'abstract', 'language', 'format', 'keywords'),
            'classes': ('tab-abstract',),
        }),
        (None, {
            'fields': ('publisher', 'license', 'version'),
            'classes': ('tab-legal',),
        }),
        (None, {
            'fields': ('created_at', 'updated_at', 'published_at'),
            'classes': ('tab-dates',),
        }),
        (None, {
            'fields': ('is_published', 'is_doi_requested', ('is_doi_check_passed', 'doi_notification'), 'is_doi_approved', 'doi'),
            'classes': ('tab-publication',),
        }),
    ]

    tabs = [
        ("Basic Information", ["tab-basic"]),
        ("Data Description", ["tab-abstract"]),
        ("Author Information", ["tab-author-inline"]),
        ("Legal Information", ["tab-legal"]),
        ("Publication", ["tab-publication"]),
        ("Dates", ["tab-dates"]),
    ]

    def has_change_permission(self, request, obj=None):
        if obj:
            if obj.doi:
                return False
        return True

    def has_delete_permission(self, request, obj=None):
        if obj:
            if obj.doi:
                return False
        return True

    def get_readonly_fields(self, request, obj):
        fields = ['dataset_path', 'geonetwork_uuid', 'record_id', 'doi', 'created_at', 'updated_at', 'published_at',
                  'is_doi_check_passed', 'doi_notification']

        if not request.user.is_superuser:
            fields.append('is_doi_approved', )

        if not obj:
            fields.extend(['is_doi_requested', 'is_doi_approved'])

        if obj:
            fields.extend(['bucket', 'file_path'])

            if obj.published_at and (obj.is_doi_requested or obj.is_doi_approved):
                fields.append('is_published')

            if not obj.is_doi_requested or not obj.is_doi_check_passed:
                fields.append('is_doi_approved')

            if not obj.is_published or obj.is_doi_requested:
                fields.append('is_doi_requested')

        return fields

    def formfield_for_foreignkey(self, db_field, request, **kwargs):

        if not request.user.is_superuser:
            # if not superuser, a user can only select his buckets
            if db_field.name == "bucket":
                kwargs["queryset"] = Bucket.objects.filter(group__in=get_user_groups(request))

        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_queryset(self, request):
        # if not superuser, a user can see only see MetadataRecords from his buckets
        qs = super().get_queryset(request)

        if request.user.is_superuser:
            return qs
        return qs.filter(bucket__group__in=get_user_groups(request))

    def save_model(self, request, obj: Bucket, form, change):
        if obj.is_published:
            obj.published_at = datetime.now()
        super().save_model(request, obj, form, change)


    def save_related(self, request, form, formset, change):

        super(MetadataRecordAdmin, self).save_related(request, form, formset, change)

        obj = form.instance
        obj.dataset_path = get_folder_name(obj.file_path)

        if not obj.record_id:
            obj.record_id = create_file_identifier()

        updated_obj = self._check_dependencies(request, obj)

        if updated_obj:
            success = update_jsonld(updated_obj)

            if success:
                super().save_model(request, updated_obj, form, change)
        else:
            messages.error(request, "Changes could not be saved.")

    def _check_dependencies(self, request, obj):

        if not obj.is_published and obj.is_doi_requested:
            messages.warning(request, 'DOI creation can only be requested, when record is published.')
            obj.is_doi_requested = False

        if not obj.is_published and obj.doi:
            obj.is_published = True

        if obj.is_doi_approved and not obj.is_doi_requested:
            messages.warning(request, 'Cannot approve DOI request, if none is requested.')
            obj.is_doi_approved = False

        if obj.is_doi_approved and not obj.is_doi_check_passed:
            messages.warning(request, 'Cannot approve DOI request, if compliance check failed.')
            obj.is_doi_approved = False

        return obj


def update_jsonld(metadata_record: MetadataRecord):
    if metadata_record.file_path:
        file_name = 'tmp.jsonld'

        with open(file_name, "w") as file:
            jsonld_formatted = create_jsonld_content(metadata_record)

            file.write(jsonld_formatted)

        upload_file(metadata_record.bucket.name, metadata_record.file_path, file_name)

        os.remove(file_name)

        return True
    else:
        print("MetadataRecord has no file_path: ")
        return False


@receiver(post_delete, sender=MetadataRecord)
def delete_jsonld_file(sender, instance, using, **kwargs):
    print('Delete Jsonld-file in S3-Bucket')
    delete_file(instance.bucket.name, instance.file_path)
