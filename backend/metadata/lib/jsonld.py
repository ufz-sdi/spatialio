import json
from metadata.models import Author, DataFormat, License, MetadataRecord, METADATA_LANGUAGES, MetadataRecordAuthor, Organization # noqa
from main.lib.utils.file import create_file_identifier # noqa

from datetime import datetime


class JsonLDParser:

    def __init__(self, fs):
        self.fs = fs

    def update_metadata_record(self, record: MetadataRecord, absolute_path: str):
        try:
            with self.fs.open(absolute_path, 'rb') as jsonld_fs:
                jsonld_dict = json.load(jsonld_fs)

                # check if the uploaded JSON-LD contains schema.org compliant content
                if 'schema.org' in jsonld_dict["@context"]["@vocab"]:

                    # easy-read fields
                    record.version = jsonld_dict["version"]
                    record.language = get_language_if_corresponds(jsonld_dict["inLanguage"])
                    record.format = get_or_create_data_format(jsonld_dict["encodingFormat"])
                    record.license = get_or_create_license(jsonld_dict["license"])
                    record.title = jsonld_dict["name"]
                    record.abstract = jsonld_dict["abstract"]
                    record.keywords = jsonld_dict["keywords"]

                    # identifier
                    doi = get_doi(jsonld_dict)
                    record.doi = doi

                    # publisher
                    record.publisher = get_or_create_organization(jsonld_dict["publisher"])

                    # set updated date to now
                    record.updated = datetime.today().strftime("%Y-%m-%d")

                    record.save()

                    # authors
                    connect_metadata_record_authors(record, jsonld_dict["author"])
                else:
                    print('Uploaded JSON-LD does not match Metadata form.')

        except Exception as e:
            print("Error while reading JSON-LD: " + str(e))


def create_jsonld_content(record: MetadataRecord):
    jsonld_formatted = {
        "@context": {
            "@vocab": "https://schema.org/",
        },
        "@type": "Dataset",
        "version": record.version if record.version else "0.1",
        "inLanguage": record.language,
        "encodingFormat": record.format.name,
        "license": record.license.name,
        "name": record.title,
        "identifier": get_identifier_as_jsonld(record),
        "dateCreated": record.created_at.strftime("%Y-%m-%d"),
        "dateModified": record.updated_at.strftime("%Y-%m-%d"),
        "datePublished": get_date_published(record),
        "author": get_authors_as_jsonld(record),
        "publisher": get_organization_as_jsonld(record.publisher),
        "abstract": record.abstract,
        "keywords": record.keywords if record.keywords else [],
    }
    return json.dumps(jsonld_formatted, indent=4)


def get_date_published(record: MetadataRecord):
    if not record.published_at and record.is_published:
        return datetime.today().strftime("%Y-%m-%d")
    if record.published_at:
        return record.published_at.strftime("%Y-%m-%d")
    else:
        return None


def get_doi(jsonld_dict):
    doi = None
    for identifier in jsonld_dict["identifier"]:
        if identifier["propertyID"] == "DOI":
            if identifier["value"] != "":
                doi = identifier["value"]
    return doi


def get_identifier_as_jsonld(record: MetadataRecord):
    identifier = [
        {
            "@type": "PropertyValue",
            "propertyID": "DOI",
            "value": record.doi if record.doi else ""
        },
        {
            "@type": "PropertyValue",
            "propertyID": "UUID",
            "value": str(record.geonetwork_uuid) if record.geonetwork_uuid else ""
        }
    ]
    return identifier


def connect_metadata_record_authors(record: MetadataRecord, jsonld_authors):
    authors = get_authors(jsonld_authors)

    connected_authors = MetadataRecordAuthor.objects.filter(record=record).select_related('author')
    authors_to_keep = []
    for connected_author in connected_authors:
        if connected_author.author in authors:
            authors_to_keep.append(connected_author.author.id)
        else:
            connected_author.delete()

    for author in authors:
        if author.id not in authors_to_keep:
            metadata_record_author = MetadataRecordAuthor(record=record, author=author)
            metadata_record_author.save()


def get_authors(jsonld_authors):
    authors = []
    for jsonld_author in jsonld_authors:
        try:
            author = Author.objects.get(email=jsonld_author["email"], orcid=jsonld_author["identifier"]["value"])
        except Author.DoesNotExist:
            author = Author(
                given_name=jsonld_author["givenName"],
                family_name=jsonld_author["familyName"],
                email=jsonld_author["email"],
                orcid=jsonld_author["identifier"]["value"],
                affiliation=get_or_create_organization(jsonld_author["affiliation"]),
                created_by=None
            )
            author.save()
        authors.append(author)
    return authors


def get_authors_as_jsonld(record: MetadataRecord):
    authors = []
    record_authors = MetadataRecordAuthor.objects.filter(record=record)
    for author in record_authors:
        author = {
            "givenName": author.author.given_name,
            "familyName": author.author.family_name,
            "email": author.author.email,
            "identifier": {
                "@type": "PropertyValue",
                "name": "ORCID",
                "value": author.author.orcid
            },
            "affiliation": get_organization_as_jsonld(author.author.affiliation)
        }
        authors.append(author)
    return authors


def get_or_create_organization(jsonld_org):
    try:
        organization = Organization.objects.get(ror=jsonld_org["identifier"]["value"])
    except Organization.DoesNotExist:
        organization = Organization(
            name=jsonld_org["name"],
            ror=jsonld_org["identifier"]["value"],
            created_by=None
        )
        organization.save()
    return organization


def get_or_create_data_format(jsonld_data_format):
    try:
        data_format = DataFormat.objects.get(name=jsonld_data_format)
    except DataFormat.DoesNotExist:
        data_format = DataFormat(
            name=jsonld_data_format
        )
        data_format.save()
    return data_format


def get_or_create_license(jsonld_license):
    try:
        data_license = License.objects.get(name=jsonld_license)
    except DataFormat.DoesNotExist:
        data_license = License(
            name=jsonld_license
        )
        data_license.save()
    return data_license


def get_language_if_corresponds(jsonld_lang):
    for system_lang in METADATA_LANGUAGES:
        if system_lang[0] == jsonld_lang:
            return jsonld_lang
    return None


def get_organization_as_jsonld(publisher):
    if publisher:
        return {
            "@type": "Organization",
            "name": publisher.name if publisher.name else "",
            "identifier": {
                "@type": "PropertyValue",
                "name": "ROR",
                "value": publisher.ror if publisher.ror else ""
            }
        }
    return {
        "@type": "Organization",
        "name": "",
        "identifier": {
            "@type": "PropertyValue",
            "name": "ROR",
            "value": ""
        }
    }


def get_formatted_date(date_string):
    date_formats = ["%d-%m-%Y", "%Y-%m-%d", "%m-%Y", "%Y-%m", "%Y"]
    formatted_date = None
    for date_format in date_formats:
        try:
            formatted_date = datetime.strptime(date_string, date_format).date()
        except Exception:
            pass
    return formatted_date
