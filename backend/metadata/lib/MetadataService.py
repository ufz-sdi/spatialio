from metadata.models import MetadataRecord # noqa
from metadata.api import GeonetworkApi # noqa


class MetadataService:

    def __init__(self, api: GeonetworkApi):
        self.api = api
        self.has_error = False

    def submit_to_geonetwork(self, record: MetadataRecord):
        try:
            if record.geonetwork_uuid:
                self.api.update_record(record)
                self.api.log("Geonetwork-Record updated.")
            else:
                uuid = self.api.create_record(record)
                if uuid:
                    record.geonetwork_uuid = uuid
                    record.save()
                    self.api.log("Geonetwork-Record created.")
        except Exception as e:
            self.api.log("Error in creating Geonetwork-Record: " + str(e))
            self.has_error = True

        if record.is_doi_requested:
            if not record.is_doi_approved:
                self.check_doi_conditions(record)
            elif record.is_doi_approved and not record.doi:
                self.create_doi(record)

    def check_doi_conditions(self, record: MetadataRecord):
        record.is_doi_check_passed = self.api.check_doi_preconditions(record.geonetwork_uuid)

        if record.is_doi_check_passed:
            record.doi_notification = 'Record passed DOI preconditions.'
        else:
            record.doi_notification = 'Record did not pass DOI preconditions.'
        record.save()

    def create_doi(self, record: MetadataRecord):
        try:
            doi = self.api.get_doi(record.geonetwork_uuid)
            if doi:
                record.doi = doi
                record.save()
                print(record.__dict__)
                self.api.log("Doi assigned.")
        except Exception as e:
            self.api.log("Error in creating DOI: " + str(e))
            self.has_error = True

    def delete_record(self, record: MetadataRecord):
        try:
            self.api.delete_record(record.geonetwork_uuid)

            print("Delete record with UUID: {}".format(record.geonetwork_uuid))
            record.delete()
        except Exception as e:
            print('Could not delete related geonetwork_record: ' + str(e))
            self.has_error = True
