"""gdi-admin URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path, re_path

from main.views import views, aggregation_requests, download_requests, timeseries_request, sta_requests, wfs_requests   # noqa
from main.about import basic_site

from health_check.views import get_health

urlpatterns = [
    path('gdi-backend/health/', get_health),
    path('gdi-backend/admin/', admin.site.urls),
    path('gdi-backend/about/', basic_site.admin_view(basic_site.about), name='about'),
    path('gdi-backend/download/zip/<record_id>', download_requests.get_zip_download, name='zipDownload'),
    path('gdi-backend/countries', aggregation_requests.get_countries, name='countries'),
    path('gdi-backend/sta-locations/<int:sta_layer_id>', sta_requests.proxy_sta_locations, name='sta-locations'),
    path('gdi-backend/sta-obs-properties/<int:sta_layer_id>', sta_requests.proxy_sta_obs_properties, name='sta-obs-properties'),
    path('gdi-backend/sta-thing/<int:sta_endpoint_id>/<int:thing_id>', sta_requests.proxy_sta_thing, name='sta-thing'),
    path('gdi-backend/sta-obs-properties-by-thing/<int:sta_layer_id>/<int:thing_id>', sta_requests.proxy_sta_obs_properties_by_thing, name='sta-obs-properties-by-thing'),
    path('gdi-backend/sta-datastream-properties/<int:sta_endpoint_id>/<int:thing_id>/<int:obs_property_id>', sta_requests.proxy_sta_datastream_properties, name='sta-datastream-properties'),
    path('gdi-backend/sta-all-datastreams/<int:sta_endpoint_id>/<int:thing_id>/<int:obs_property_id>', sta_requests.proxy_sta_all_datastreams, name='sta-all-datastreams'),
    path('gdi-backend/wfs-features/<int:layer_id>', wfs_requests.wfs_features, name='wfs-features'),
    re_path(r'^gdi-backend/timeseries/(?P<locale>[a-z]{2})', timeseries_request.request_all_time_series, name='request_time_series'),
    re_path(r'^gdi-backend/process', aggregation_requests.call_process, name='aggregation'),
    re_path(r'^gdi-backend/districts/(?P<country_id>[0-9][0-9])$', aggregation_requests.get_districts_by_country, name='districts'),
    path('gdi-backend/uncertainty/<path:file_path>', download_requests.get_uncertainty, name='uncertainty'),
    re_path(r'^gdi-backend/project/(?P<locale>[a-z]{2})/(?P<project_name>[a-z0-9-]*)$', views.get_project_information, name='getProjectInformation'),
    re_path(r'^gdi-backend/projects/(?P<locale>[a-z]{2})', views.get_projects, name='getProjects'),
    re_path(r'^gdi-backend/area-names/(?P<locale>[a-z]{2})/(?P<project_name>[a-z0-9-]*)$', views.get_project_area_names, name='getProjectAreaNames'),
    path('gdi-backend/area/<str:locale>/<int:area_id>', views.get_project_area, name='getProjectArea'),
    path('gdi-backend/filter/<str:locale>/<int:area_id>', views.get_filters, name='getProjectFilters'),
    re_path(r'^_nested_admin/', include('nested_admin.urls')),
]
