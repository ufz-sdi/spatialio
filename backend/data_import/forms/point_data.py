from django.contrib import admin    # noqa
from django_tabbed_changeform_admin.admin import DjangoTabbedChangeformAdmin # noqa

from data_import.models import PointData, ExtendedPointData    # noqa
from main.lib.utils.backend import get_user_groups


class ExtendedPointDataInline(admin.TabularInline):
    model = ExtendedPointData
    min_num = 0
    extra = 1
    classes = ["tab-extended-point-data-inline"]


class PointDataAdmin(DjangoTabbedChangeformAdmin, admin.ModelAdmin):
    list_display = ('thing_name', 'result_value', 'result_time', 'validation_error')
    inlines = [ExtendedPointDataInline]

    def get_readonly_fields(self, request, obj):
        if obj is not None:
            if obj.import_job:
                return ['import_job', 'validation_error', 'thing_name', 'location_name', 'coord_lat', 'coord_lon', 'property', 'result_value', 'result_unit', 'result_time', 'sensor',]
        else:
            return ['import_job', 'validation_error']

    fieldsets = [
        (None, {
            'fields': ('thing_name', 'location_name', ('coord_lat', 'coord_lon'), 'property', ('result_value', 'result_unit', 'result_time'), 'sensor', ),
            'classes': ('tab-basic',),
        }),
        (None, {
            'fields': ('import_job', 'validation_error', 'is_scheduled' ),
            'classes': ('tab-workflow',),
        }),
    ]

    tabs = [
        ("Basic Information", ["tab-basic"]),
        ("Additional Values", ["tab-extended-point-data-inline"]),
        ("Workflow", ["tab-workflow"]),
    ]

    def get_queryset(self, request):
        # if not superuser, a user can only see point-data of his import-jobs
        qs = super().get_queryset(request)

        if request.user.is_superuser:
            return qs
        return qs.filter(import_job__bucket__group__in=get_user_groups(request))
