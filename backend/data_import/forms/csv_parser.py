from django.contrib import admin    # noqa
from django_tabbed_changeform_admin.admin import DjangoTabbedChangeformAdmin # noqa

from data_import.models import CsvParser, CsvParserExtraColumn, CsvIncludeCriteria    # noqa
from main.lib.utils.backend import get_user_groups


class CsvIncludeCriteriaInline(admin.TabularInline):
    model = CsvIncludeCriteria
    min_num = 0
    extra = 0
    classes = ["tab-include-criteria-inline"]


class CsvParserExtraColumnInline(admin.TabularInline):
    model = CsvParserExtraColumn
    min_num = 0
    extra = 0
    classes = ["tab-extra-columns-inline"]


class CsvParserAdmin(DjangoTabbedChangeformAdmin, admin.ModelAdmin):
    model = CsvParser
    inlines = [CsvParserExtraColumnInline, CsvIncludeCriteriaInline]

    fieldsets = [
        (None, {
            'fields': ('station_col_num', 'property_col_num', ('lat_col_num', 'lon_col_num'), ('value_col_num', 'unit_col_num'), 'time_col_num' ),
            'classes': ('tab-basic',),
        }),
        (None, {
            'fields': ('location_id_col_num', 'property_id_col_num', 'country_col_num', 'community_col_num', 'skip_first_row' ),
            'classes': ('tab-optional',),
        }),
        (None, {
            'fields': ('target', 'group', ),
            'classes': ('tab-group',),
        }),
    ]

    tabs = [
        ("Basic Information", ["tab-basic"]),
        ("Optional Information", ["tab-optional"]),
        ("Extra-Columns", ["tab-extra-columns-inline"]),
        ("Include-Criteria", ["tab-include-criteria-inline"]),
        ("Group", ["tab-group"]),
    ]

    def get_queryset(self, request):
        # if not superuser, a user can only see parsers of his groups
        qs = super().get_queryset(request)

        if request.user.is_superuser:
            return qs
        return qs.filter(group__in=get_user_groups(request))

    def formfield_for_foreignkey(self, db_field, request, **kwargs):

        if not request.user.is_superuser:
            # if not superuser, a user can only select his groups
            if db_field.name == "group":
                kwargs["queryset"] = get_user_groups(request)

        return super().formfield_for_foreignkey(db_field, request, **kwargs)
