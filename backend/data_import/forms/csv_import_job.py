from django.contrib import admin    # noqa

from data_import.models import CsvImportJob  # noqa
from main.lib.utils.backend import get_user_groups


class CsvImportJobAdmin(admin.ModelAdmin):
    model = CsvImportJob
    list_display = (
        's3_file',
        'bucket',
        'target',
        'is_processed',
        'is_running',
        'is_success',
        'execution_time',
        'time_created'
    )
    ordering = ('-created_at',)

    fieldsets = [
        (None, {
            'fields': (('bucket'), ('s3_file', 'file_size', 'num_rows'), 'target', ('time_created', 'is_running', 'is_processed'), ('started_at', 'finished_at', 'execution_time' )),
        }),
        ('Results', {
            'fields': ('is_success', ('data_points_created', 'data_points_failed'), 'validation_error', ),
        }),
        ('Status', {
            'fields': ('is_data_deleted', ),
        }),
    ]

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def time_created(self, obj):
        return obj.created_at.strftime("%d.%m.%Y %H:%M:%S")

    def get_queryset(self, request):
        # if not superuser, a user can only see import-jobs of his buckets
        qs = super().get_queryset(request)

        if request.user.is_superuser:
            return qs
        return qs.filter(bucket__group__in=get_user_groups(request))
