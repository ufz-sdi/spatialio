
from data_import.models import CsvIncludeCriteria # noqa


def is_row_included(row, include_criteria: list[CsvIncludeCriteria]) -> bool:
    if len(include_criteria) > 0:
        for criteria in include_criteria:
            if row[criteria.col_num] == criteria.text_value:
                return True
        return False
    else:
        return True

def create_wfs_data_store_name(bucket_name: str, relative_file_path: str):
    name_parts = relative_file_path.split('/')
    del name_parts[-1]
    if len(name_parts) == 0:
        name = bucket_name
    else:
        name = "_".join(name_parts)
    return name.replace('-', '_')
