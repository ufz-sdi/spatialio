import csv

from django.db import connections   # noqa

from data_import.models import CsvParser, CsvImportJob, CsvParserExtraColumn, CsvIncludeCriteria  # noqa
from data_import.models import WfsImportRecord, WFS, STA, STA_THING, STA_PROPERTY, STA_OBSERVATION # noqa
from data_import.lib.utils import create_wfs_data_store_name, is_row_included   # noqa

class PostgisImporter:

    def __init__(self, job: CsvImportJob):

        self.job = job
        self.parser = job.bucket.csv_parser

        data_store_name = create_wfs_data_store_name(job.bucket.name, job.s3_file)

        self.feature_table = "{}_feature".format(data_store_name)
        self.property_table = "{}_property".format(data_store_name)
        self.timeseries_table = "{}_timeseries".format(data_store_name)
        self.timerecord_table = "{}_timerecord".format(data_store_name)

        self.error = False
        self.logs = []


    def import_csv_to_wfs(self):

        self.create_tables_if_not_exist()

        file_name = self.job.s3_file.split('/')[-1]

        with open(file_name, newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')

            batch_size = 1000
            batch = []
            count = 0

            p = self.parser
            extra_columns = CsvParserExtraColumn.objects.filter(parser=p)
            include_criteria = CsvIncludeCriteria.objects.filter(parser=p)

            if p.skip_first_row:
                next(reader)  # skip header

            for row in reader:

                if len(row) == 0:
                    continue
                if is_row_included(row, include_criteria):

                    location_props = {}
                    property_props = {}
                    record_props = {}

                    for ec in extra_columns:
                        if ec.related_entity == STA_THING:
                            location_props[ec.col_name] = row[ec.col_num]
                        if ec.related_entity == STA_PROPERTY:
                            property_props[ec.col_name] = row[ec.col_num]
                        if ec.related_entity == STA_OBSERVATION:
                            record_props[ec.col_name] = row[ec.col_num]

                    if row[p.time_col_num] == 'NA':
                        continue

                    # if is point data
                    # TODO: adapt also for polygon data

                    batch.append(
                        WfsImportRecord(
                            location_name=row[p.station_col_num],
                            lat=row[p.lat_col_num],
                            lon=row[p.lon_col_num],
                            value=row[p.value_col_num],
                            date=row[p.time_col_num],
                            unit=row[p.unit_col_num],
                            property_name=row[p.property_col_num],
                            bucket_name=self.job.bucket.name,
                            import_job=self.job.id,
                            location_id=row[p.location_id_col_num] if p.location_id_col_num else None,
                            property_id=row[p.property_id_col_num] if p.property_id_col_num else None,
                            country=row[p.country_col_num] if p.country_col_num else None,
                            community=row[p.community_col_num] if p.community_col_num else None,
                            location_properties=location_props,
                            property_properties=property_props,
                            record_properties=record_props
                        )
                    )

                    count += 1
                    if count % batch_size == 0:
                        WfsImportRecord.objects.using('geoserver').bulk_create(batch)
                        batch = []
                        print('{} records created.'.format(count))


    def write_data(self):

        c = connections["geoserver"].cursor()

        self.write_features(c)
        self.write_properties(c)
        self.write_timeseries(c)
        self.write_timerecords(c)

        self.update_properties_bbox(c)
        self.update_timeseries_values(c)
        self.update_features(c)

        try:
            c.execute("""DELETE FROM geoserver.data_import_wfsimportrecord""")
            print('Import done.')
        except Exception as e:
            self.logs.append(str(e))
            self.error = True
        finally:
            c.close()


    def write_features(self, c):

        try:
            c.execute("""
                insert into geoserver.{table} (name, lat, lon, geom, gtype, srid, country, import_id, created_at{custom_columns})
                    select 
                        d.location_name,
                        cast(d.lat as double precision),
                        cast(d.lon as double precision),
                        st_point(cast(d.lon as double precision), cast(d.lat as double precision), 4326),
                        'Point',
                        '4326',
                        max(d.country),
                        max(d.import_job),
                        now()
                        {custom_column_selects}
                    from geoserver.data_import_wfsimportrecord d
                    group by (d.location_name, d.lat, d.lon, d.location_properties)
                    on conflict do nothing;
            """.format(
                    table=self.feature_table,
                    custom_columns=self.get_custom_columns(STA_THING),
                    custom_column_selects=self.get_custom_column_selects(STA_THING, 'd.location_properties')
                ))

        except Exception as e:
            self.logs.append(str(e))
            self.error = True
        print('Features inserted.')


    def write_properties(self, c):

        try:
            c.execute("""
                insert into geoserver.{table} (name, technical_id, min_lat, min_lon, max_lat, max_lon{custom_columns})
                    select 
                        d.property_name, 
                        d.property_id, 
                        MIN(cast(d.lat as double precision)), 
                        MIN(cast(d.lon as double precision)), 
                        MAX(cast(d.lat as double precision)), 
                        MAX(cast(d.lon as double precision))
                        {custom_column_selects}
                    from geoserver.data_import_wfsimportrecord d
                    group by (d.property_name, d.property_id, d.property_properties)
                    on conflict do nothing;
            """.format(
                    table=self.property_table,
                    custom_columns=self.get_custom_columns(STA_PROPERTY),
                    custom_column_selects=self.get_custom_column_selects(STA_PROPERTY, 'd.property_properties')
                )
            )

            print('Properties inserted.')

        except Exception as e:
            self.logs.append(str(e))
            self.error = True

    def write_timeseries(self, c):

        # if point => take over Point-Geometry
        # if polygon => create BBox
        try:
            c.execute("""
            insert into geoserver.{timeseries_table} (feature_id, property_id, unit, min_value, max_value, min_date, max_date, lat, lon, geom, gtype, srid)
                select
                    (select f.id from {feature_table} f where f.name = d.location_name and f.lat = cast(d.lat as double precision) and f.lon = cast(d.lon as double precision)),
                    (select p.id from {property_table} p where p.name = d.property_name),
                    d.unit,
                    MIN(cast(d.value as double precision)),
                    MAX(cast(d.value as double precision)),
                    MIN(to_timestamp(d.date, 'YYYY-MM-DDThh24:mi:ss')::timestamp without time zone at time zone 'Etc/UTC'),
                    MAX(to_timestamp(d.date, 'YYYY-MM-DDThh24:mi:ss')::timestamp without time zone at time zone 'Etc/UTC'),
                    cast(d.lat as double precision),
                    cast(d.lon as double precision),
                    st_point(cast(d.lon as double precision), cast(d.lat as double precision), 4326),
                    'Point',
                    '4326'
                    from geoserver.data_import_wfsimportrecord d
                    group by (
                        d.location_name,
                        d.lat,
                        d.lon,
                        d.property_name,
                        d.unit
                    )
                on conflict do nothing;
            """.format(timeseries_table=self.timeseries_table, feature_table=self.feature_table, property_table=self.property_table))

        except Exception as e:
            self.logs.append(str(e))
            self.error = True

        print('TimeSeries inserted.')


    def write_timerecords(self, c):

        try:
            c.execute("""
                insert into geoserver.{timerecord_table} (timeseries_id, date, value_text, value_number, lat, lon, geom, gtype, srid, import_id{custom_columns})
                    select
                        (select id from {timeseries_table} t where t.feature_id = cf.id and t.property_id = cp.id),
                        to_timestamp(d.date, 'YYYY-MM-DDThh24:mi:ss')::timestamp without time zone at time zone 'Etc/UTC',
                        d.value,
                        cast(d.value as double precision),
                        cast(d.lat as double precision),
                        cast(d.lon as double precision),
                        st_point(cast(d.lon as double precision), cast(d.lat as double precision), 4326),
                        'Point',
                        '4326',
                        d.import_job
                        {custom_column_selects}
                        from geoserver.data_import_wfsimportrecord d
                        inner join geoserver.{feature_table} cf on d.location_name = cf.name and cast(d.lat as double precision) = cf.lat and cast(d.lon as double precision) = cf.lon
                        inner join geoserver.{property_table} cp on d.property_name = cp.name;
            """.format(
                timerecord_table=self.timerecord_table,
                feature_table=self.feature_table,
                property_table=self.property_table,
                timeseries_table=self.timeseries_table,
                custom_columns=self.get_custom_columns(STA_OBSERVATION),
                custom_column_selects=self.get_custom_column_selects(STA_OBSERVATION, 'd.record_properties')
            ))

        except Exception as e:
            self.logs.append(str(e))
            self.error = True

        print('TimeRecords inserted.')


    def update_features(self, c):

        stmt_params = [
            ('min_timerecord', 'min_date', 'MIN'),
            ('max_timerecord', 'max_date', 'MAX'),
        ]

        try:
            for params in stmt_params:
                c.execute("""
                    update geoserver.{feature_table} cf
                        set {col1} = x.aggr_date
                        from (
                            select
                                {aggr_func}(ts.{col2}) as aggr_date,
                                ts.feature_id
                            from {timeseries_table} ts
                            group by ts.feature_id
                        ) as x
                        where cf.id = x.feature_id
                    """.format(
                        feature_table=self.feature_table,
                        timeseries_table=self.timeseries_table,
                        col1=params[0],
                        col2=params[1],
                        aggr_func=params[2])
                )

        except Exception as e:
            self.logs.append(str(e))
            self.error = True

        print('Min- and Max-Timerecords of Features updated.')

    def update_properties_bbox(self, c):

        stmt_params = [
            ('min_lat', 'lat', 'MIN', '<'),
            ('min_lon', 'lon', 'MIN', '<'),
            ('max_lat', 'lat', 'MAX', '>'),
            ('max_lon', 'lon', 'MAX', '>'),
        ]

        try:
            for params in stmt_params:
                c.execute("""
                    update geoserver.{table_name} cp
                        set {col1} = x.aggr_value
                        from (
                            select {aggr_func}(cast(d.{col2} as double precision)) as aggr_value,
                            property_name
                            from geoserver.data_import_wfsimportrecord d
                            group by (d.property_name)
                        ) as x
                        where cp.name = x.property_name
                        and x.aggr_value {operator} cp.{col1}
                """.format(
                    table_name=self.property_table,
                    col1=params[0],
                    col2=params[1],
                    aggr_func=params[2],
                    operator=params[3])
                )

            c.execute("""
                update geoserver.{} cp
                    set geom = ST_MakeEnvelope(cp.min_lon, cp.min_lat, cp.max_lon, cp.max_lat),
                    gtype = 'Point',
                    srid = '4326'
            """.format(self.property_table))

        except Exception as e:
            self.logs.append(str(e))
            self.error = True

        print('Bounding-Box of Properties updated.')

    def update_timeseries_values(self, c):

        stmt_params = [
            ('min_value', 'cast(d.value as double precision)', 'MIN', '<'),
            ('max_value', 'cast(d.value as double precision)', 'MAX', '>'),
            ('min_date', "to_timestamp(d.date, 'YYYY-MM-DDThh24:mi:ss')::timestamp without time zone at time zone 'Etc/UTC'", 'MIN', '<'),
            ('max_date', "to_timestamp(d.date, 'YYYY-MM-DDThh24:mi:ss')::timestamp without time zone at time zone 'Etc/UTC'", 'MAX', '>'),
        ]

        try:
            for params in stmt_params:
                c.execute("""
                    update geoserver.{timeseries_table} ts
                        set {col1} = x.updated_value
                        from (
                            select {aggr_func}({col2}) as updated_value,
                            d.property_name,
                            d.location_name,
                            cast(d.lat as double precision),
                            cast(d.lon as double precision),
                            d.unit
                            from geoserver.data_import_wfsimportrecord d
                            group by (
                                d.property_name,
                                d.location_name,
                                d.lat,
                                d.lon,
                                d.unit
                            )
                        ) as x
                        where ts.property_id = (select p.id from geoserver.{property_table} p where x.property_name = p.name)
                        and ts.feature_id = (select f.id from geoserver.{feature_table} f where x.location_name = f.name and x.lat = f.lat and x.lon = f.lon)
                        and ts.unit = x.unit
                        and x.updated_value {operator} ts.{col1};
                """.format(
                    timeseries_table=self.timeseries_table,
                    feature_table=self.feature_table,
                    property_table=self.property_table,
                    col1=params[0],
                    col2=params[1],
                    aggr_func=params[2],
                    operator=params[3])
                )

        except Exception as e:
            self.logs.append(str(e))
            self.error = True

        print('TimeSeries updated.')


    def create_tables_if_not_exist(self):
        c = connections["geoserver"].cursor()

        stmts = []
        stmts.append("""
            create table if not exists geoserver.data_import_wfsimportrecord
            (
                id                  bigint generated by default as identity primary key,
                location_id         varchar(1000),
                location_name       varchar(1000),
                type_name           varchar(1000),
                period_start        varchar(1000),
                period_end          varchar(1000),
                lat                 varchar(1000),
                lon                 varchar(1000),
                srid                varchar(1000),
                geom                varchar(4000),
                gtype               varchar(1000),
                country             varchar(1000),
                community           varchar(1000),
                corine              varchar(1000),
                value               varchar(1000),
                date                varchar(1000),
                unit                varchar(1000),
                property_name       varchar(1000),
                property_id         varchar(1000),
                location_properties jsonb,
                property_properties jsonb,
                record_properties   jsonb,
                import_job          integer                  not null
                    constraint data_import_wfsimportrecord_import_job_check
                        check (import_job >= 0),
                created_at          timestamp with time zone not null,
                bucket_name         varchar                  not null
            );""")

        stmts.append("""
            create table if not exists geoserver.{feature_table}
            (
                id         integer generated always as identity
                    constraint {feature_table}_pk
                        primary key,
                name       varchar not null,
                lat        double precision,
                lon        double precision,
                geom       geometry,
                gtype      varchar,
                srid       varchar,
                country    varchar,
                community  varchar,
                import_id  integer not null,
                min_timerecord timestamp,
                max_timerecord timestamp,
                created_at timestamp
                {properties}
            );""".format(feature_table=self.feature_table, properties=self.get_custom_column_defs(STA_THING)))

        stmts.append("create unique index if not exists unique_{table} on geoserver.{table} (name, lat, lon);".format(table=self.feature_table))

        stmts.append(
           """ 
            create table if not exists geoserver.{property_table}
            (
                id           integer generated always as identity
                    constraint {property_table}_pk
                        primary key,
                name         varchar not null,
                technical_id varchar,
                min_lat      numeric,
                min_lon      numeric,
                max_lat      numeric,
                max_lon      numeric,
                geom         geometry,
                gtype        varchar,
                srid         varchar
                {properties}
            );""".format(property_table=self.property_table, properties=self.get_custom_column_defs(STA_PROPERTY)))

        stmts.append("create unique index if not exists unique_{table} on geoserver.{table} (name);".format(table=self.property_table))

        stmts.append("""
            create table if not exists geoserver.{timeseries_table}
            (
                id          integer generated always as identity
                    constraint {timeseries_table}_pk
                        primary key,
                feature_id  integer not null
                    constraint {timeseries_table}_{feature_table}_id_fk
                        references geoserver.{feature_table},
                property_id integer not null
                    constraint {timeseries_table}_{property_table}_id_fk
                        references geoserver.{property_table},
                unit        varchar,
                lat         numeric,
                lon         numeric,
                geom        geometry,
                gtype       varchar,
                srid        varchar,
                min_value   double precision,
                max_value   double precision,
                min_date    timestamp,
                max_date    timestamp
            );""".format(timeseries_table=self.timeseries_table, feature_table=self.feature_table, property_table=self.property_table))

        stmts.append("create unique index if not exists unique_{table} on geoserver.{table} (feature_id, property_id, unit);".format(table=self.timeseries_table))

        stmts.append("create index if not exists {table}_timeseries_id_index on geoserver.{table} (feature_id);".format(table=self.timeseries_table))

        stmts.append("""
            create table if not exists geoserver.{timerecord_table}
            (
                id            integer generated always as identity
                    constraint {timerecord_table}_pk
                        primary key,
                timeseries_id integer   not null
                    constraint "{timerecord_table}_{timeseries_table}_id_fk"
                        references geoserver.{timeseries_table},
                date          timestamp not null,
                value_text    varchar,
                lat           numeric,
                lon           numeric,
                geom          geometry,
                gtype         varchar,
                import_id     integer   not null,
                value_number  double precision,
                srid          varchar
                {properties}
            );
        """.format(
                timerecord_table=self.timerecord_table,
                timeseries_table=self.timeseries_table,
                properties=self.get_custom_column_defs(STA_OBSERVATION)
            )
        )

        stmts.append("create index if not exists {table}_timeseries_id_index on geoserver.{table} (timeseries_id);".format(table=self.timerecord_table))

        tables = [
            "data_import_wfsimportrecord",
            self.feature_table,
            self.property_table,
            self.timeseries_table,
            self.timerecord_table
        ]

        try:
            for stmt in stmts:
                c.execute(stmt)

            for table in tables:
                c.execute("alter table geoserver.{table} owner to admin;".format(table=table))

        except Exception as e:
            self.logs.append(str(e))
            self.error = True
        finally:
            c.close()

    def get_custom_columns(self, entity_type: str):
        result = ""
        for extra_column in CsvParserExtraColumn.objects.filter(parser=self.parser).order_by("id"):
            if extra_column.related_entity == entity_type:
                result += ", " + extra_column.col_name
        return result

    def get_custom_column_selects(self, entity_type: str, field: str):
        columns = []
        for extra_column in CsvParserExtraColumn.objects.filter(parser=self.parser):

            if extra_column.related_entity == entity_type:
                value = field + "->>'" + extra_column.col_name + "'"

                if extra_column.type_in_db == 'timestamp':
                    columns.append("to_timestamp({}, 'YYYY-MM-DDThh24:mi:ss')::timestamp without time zone at time zone 'Etc/UTC'".format(value))
                elif extra_column.type_in_db == 'double precision':
                    columns.append("(SELECT CASE WHEN {value}~E'[0-9]+\.?[0-9]*' THEN CAST({value} as double precision) ELSE NULL END)".format(value=value))
                elif extra_column.type_in_db == 'integer':
                    columns.append("(SELECT CASE WHEN {value}~E'[0-9]+' THEN CAST({value} as integer) ELSE NULL END)".format(value=value))
                else:
                    columns.append(value)

        result = ""
        if len(columns) > 0:
            result = ",\n" + ",\n".join(columns)
        return result

    def get_custom_column_defs(self, entity_type: str):
        columns = []
        for extra_column in CsvParserExtraColumn.objects.filter(parser=self.parser):
            if extra_column.related_entity == entity_type:
                columns.append(extra_column.col_name + " " + extra_column.type_in_db)

        result = ""
        if len(columns) > 0:
            result = ",\n" + ",\n".join(columns)
        return result