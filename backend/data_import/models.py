from django.db import models # noqa
from django.contrib.auth.models import Group    # noqa
from django.core.exceptions import ValidationError    # noqa

STA_THING = 'thing'
STA_PROPERTY = 'property'
STA_OBSERVATION = 'observation'

CSV_ENTITIES = (
    (STA_THING, 'thing'),
    (STA_PROPERTY, 'property'),
    (STA_OBSERVATION, 'observation'),
)

DB_COLUMN_TYPES = (
    ('varchar', 'string'),
    ('integer', 'integer'),
    ('double precision', 'float'),
    ('timestamp', 'date'),
)

WFS = 'WFS'
STA = 'STA'

TARGETS = (
    (WFS, 'Web Feature Service'),
    (STA, 'Sensorthings API'),
)


class CsvParser(models.Model):
    lat_col_num = models.IntegerField()
    lon_col_num = models.IntegerField()
    station_col_num = models.IntegerField()
    property_col_num = models.IntegerField()
    value_col_num = models.IntegerField()
    unit_col_num = models.IntegerField()
    time_col_num = models.IntegerField()
    location_id_col_num = models.IntegerField(blank=True, null=True)
    property_id_col_num = models.IntegerField(blank=True, null=True)
    country_col_num = models.IntegerField(blank=True, null=True)
    community_col_num = models.IntegerField(blank=True, null=True)
    target = models.CharField(max_length=20, choices=TARGETS, default=WFS)
    group = models.ForeignKey(Group, on_delete=models.CASCADE, null=True)
    skip_first_row = models.BooleanField(default=True)

    class Meta:
        verbose_name = "CSV-Parser"
        verbose_name_plural = "CSV-Parser"

    def __str__(self):
        return '{} Parser ({}) {}'.format(self.target, str(self.group), str(self.id))

class CsvParserExtraColumn(models.Model):
    parser = models.ForeignKey(CsvParser, on_delete=models.CASCADE)
    related_entity = models.CharField(max_length=20, choices=CSV_ENTITIES, default=STA_THING)
    col_num = models.IntegerField()
    col_name = models.CharField(max_length=100)
    type_in_db = models.CharField(max_length=20, choices=DB_COLUMN_TYPES, default='varchar')

class CsvIncludeCriteria(models.Model):
    parser = models.ForeignKey(CsvParser, on_delete=models.CASCADE)
    col_num = models.IntegerField()
    text_value = models.CharField(max_length=100)

class CsvImportJob(models.Model):
    bucket = models.ForeignKey("main.Bucket", on_delete=models.CASCADE)
    s3_file = models.CharField(max_length=1000)
    is_processed = models.BooleanField(default=False)
    is_running = models.BooleanField(default=False)
    is_success = models.BooleanField(default=False)
    execution_time = models.CharField(max_length=1000, blank=True, null=True)
    file_size = models.IntegerField(blank=True, null=True)
    num_rows = models.IntegerField(blank=True, null=True)
    validation_error = models.CharField(max_length=4000, blank=True, null=True)
    data_points_created = models.IntegerField(blank=True, null=True)
    data_points_failed = models.IntegerField(blank=True, null=True)
    is_data_deleted = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    started_at = models.DateTimeField(blank=True, null=True)
    finished_at = models.DateTimeField(blank=True, null=True)
    target = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        verbose_name = "CSV-Import-Job"
        verbose_name_plural = "CSV-Import-Jobs"

class PointData(models.Model):
    import_job = models.ForeignKey(CsvImportJob, on_delete=models.CASCADE)
    thing_name = models.CharField(max_length=1000)
    location_name = models.CharField(max_length=1000)
    coord_lat = models.CharField(max_length=1000, blank=True, null=True)
    coord_lon = models.CharField(max_length=1000, blank=True, null=True)
    # geometry = models.CharField(max_length=1000, blank=True, null=True)
    property = models.CharField(max_length=1000)
    sensor = models.CharField(max_length=1000, blank=True, null=True)
    result_value = models.CharField(max_length=100, blank=True, null=True)
    result_unit = models.CharField(max_length=100, blank=True, null=True)
    result_time = models.CharField(max_length=100, blank=True, null=True)
    validation_error = models.CharField(max_length=4000, blank=True, null=True)
    is_scheduled = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Point-Data"
        verbose_name_plural = "Point-Data"


class ExtendedPointData(models.Model):
    point_data = models.ForeignKey(PointData, on_delete=models.CASCADE)
    related_entity = models.CharField(max_length=20, choices=CSV_ENTITIES, default='thing')
    name = models.CharField(max_length=100)
    value = models.CharField(max_length=1000)

    class Meta:
        verbose_name = "Additional Value"
        verbose_name_plural = "Additional Values"


class WfsImportRecord(models.Model):
    location_id = models.CharField(max_length=1000, blank=True, null=True)
    location_name = models.CharField(max_length=1000, blank=True, null=True)
    type_name = models.CharField(max_length=1000, blank=True, null=True)
    period_start = models.CharField(max_length=1000, blank=True, null=True)
    period_end = models.CharField(max_length=1000, blank=True, null=True)
    lat = models.CharField(max_length=1000, blank=True, null=True)
    lon = models.CharField(max_length=1000, blank=True, null=True)
    srid = models.CharField(max_length=1000, blank=True, null=True)
    geom = models.CharField(max_length=4000, blank=True, null=True)
    gtype = models.CharField(max_length=1000, blank=True, null=True)
    country = models.CharField(max_length=1000, blank=True, null=True)
    community = models.CharField(max_length=1000, blank=True, null=True)
    corine = models.CharField(max_length=1000, blank=True, null=True)
    value = models.CharField(max_length=1000, blank=True, null=True)
    date = models.CharField(max_length=1000, blank=True, null=True)
    unit = models.CharField(max_length=1000, blank=True, null=True)
    property_name = models.CharField(max_length=1000, blank=True, null=True)
    property_id = models.CharField(max_length=1000, blank=True, null=True)
    location_properties = models.JSONField(blank=True, null=True)
    property_properties = models.JSONField(blank=True, null=True)
    record_properties = models.JSONField(blank=True, null=True)
    bucket_name = models.CharField(max_length=1000)
    import_job = models.PositiveIntegerField()
    created_at = models.DateTimeField(auto_now_add=True)