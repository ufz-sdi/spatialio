from django.contrib import admin # noqa

from main.models import Bucket, BucketEvent # noqa
from main.lib.utils.backend import get_user_groups # noqa


class BucketFilter(admin.SimpleListFilter):
    title = 'Bucket'
    parameter_name = 'bucket'

    def lookups(self, request, model_admin):
        result = []
        if request.user.is_superuser:
            buckets = Bucket.objects.all()
        else:
            buckets = Bucket.objects.filter(group__in=get_user_groups(request))

        for bucket in buckets:
            result.append((bucket.id, bucket.name))

        result.append((0, 'None'))

        return result

    def queryset(self, request, query_set):

        if self.value():
            if int(self.value()) == 0:
                return query_set.filter(bucket__isnull=True)

            return query_set.filter(bucket=self.value())
        else:
            return query_set

class BucketEventAdmin(admin.ModelAdmin):
    model = BucketEvent
    list_display = ('__str__', 'get_user', 'get_event_name', 'event_time', 'is_success')
    list_filter = (BucketFilter, 'is_success')
    ordering = ('-event_time',)

    search_fields = ["event_data",]

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_readonly_fields(self, request, obj):
        return ['bucket', 'event_time', 'event_data', 'logs', 'is_new', 'is_success']

    def get_queryset(self, request):
        # if not superuser, a user can see only bucketevents from his buckets
        qs = super().get_queryset(request)

        if request.user.is_superuser:
            return qs
        return qs.filter(bucket__group__in=get_user_groups(request))
