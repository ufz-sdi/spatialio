from django.contrib import admin     # noqa

from main.models import ListFilterItem, LayerListFilterItem # noqa
from main.lib.utils.backend import get_user_groups   # noqa

from main.models import AreaLayer


class LayerListFilterItemInline(admin.StackedInline):
    model = LayerListFilterItem
    min_num = 0
    extra = 0

    def has_change_permission(self, request, obj=None):
        return False

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        # reduce queryset to layers of the area
        if db_field.name == "layer":
            item = ListFilterItem.objects.get(id=request.resolver_match.kwargs['object_id'])
            kwargs["queryset"] = AreaLayer.objects.filter(area__id=item.list_filter.area.id)

        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class ListFilterItemAdmin(admin.ModelAdmin):
    model = ListFilterItem
    inlines = [LayerListFilterItemInline]
    list_display = ('name_de', 'list_filter',)
    min_num = 0
    extra = 0

    fieldsets = [
        (None, {
            'fields': (('name_de', 'name_en'), 'list_filter'),
        }),
    ]

    def get_readonly_fields(self, request, obj):
        return ['list_filter']

    def get_queryset(self, request):
        # if not superuser, a user can see only see ListFilter from his areas/projects
        qs = super().get_queryset(request)

        if request.user.is_superuser:
            return qs
        return qs.filter(list_filter__area__project__bucket__group__in=get_user_groups(request))
