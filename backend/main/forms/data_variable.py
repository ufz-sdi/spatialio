from django.contrib import admin
from django.contrib.auth.models import Group

from main.models import DataVariable
from main.lib.utils.backend import get_user_groups


class DataVariableAdmin(admin.ModelAdmin):
    model = DataVariable

    def formfield_for_foreignkey(self, db_field, request, **kwargs):

        if not request.user.is_superuser:
            # if not superuser, a user can only select his groups
            if db_field.name == "group":
                kwargs["queryset"] = Group.objects.filter(id__in=get_user_groups(request))

        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_queryset(self, request):
        # if not superuser, a user can see only variables from his groups
        qs = super().get_queryset(request)

        if request.user.is_superuser:
            return qs
        return qs.filter(group__in=get_user_groups(request))
