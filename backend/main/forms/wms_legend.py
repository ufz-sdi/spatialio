import re

from django.contrib import admin
from django.contrib.auth.models import Group
from django import forms
from django.core.exceptions import ValidationError

from main.models import WmsColor, WmsLegend
from main.lib.utils.backend import get_user_groups
from main.lib.legends.legend import create_sld_file
from main.lib.api.GeoserverApi import GeoServerApi


class WmsColorInlineFormset(forms.BaseInlineFormSet):
    model = WmsColor

    def clean(self):
        last_value = None
        for form in self.forms:
            cleaned_data = form.cleaned_data

            if cleaned_data == {}:
                continue    # form is empty and will not be saved. Skip validation.

            if not cleaned_data.get('DELETE', False):
                if last_value is not None:
                    if last_value >= cleaned_data['upper_limit']:
                        raise ValidationError("Upper-Limits of Colors have to be in ascending order!")
                last_value = cleaned_data['upper_limit']


class WmsColorInline(admin.TabularInline):
    model = WmsColor
    formset = WmsColorInlineFormset


class WmsLegendForm(forms.ModelForm):

    def clean(self):
        cleaned_data = super().clean()
        name_de = cleaned_data.get("name_de")
        name_en = cleaned_data.get("name_en")

        if name_de == name_en:
            msg = "Names in 'de' and 'en' cannot be the same."
            self.add_error("name_de", msg)
            self.add_error("name_en", msg)

        pattern = r'^[\w\s-]*$'
        msg = 'Allowed characters are: letters, numbers, hyphen, underscore, whitespace'

        if not re.match(pattern, name_de, re.UNICODE):
            self.add_error("name_de", msg)
        if not re.match(pattern, name_en, re.UNICODE):
            self.add_error("name_en", msg)


class WmsLegendAdmin(admin.ModelAdmin):
    form = WmsLegendForm
    model = WmsLegend
    save_as = True
    inlines = [WmsColorInline]
    ordering = ['name_de']
    search_fields = ['name_de']

    def formfield_for_foreignkey(self, db_field, request, **kwargs):

        if not request.user.is_superuser:
            # if not superuser, a user can only select his groups
            if db_field.name == "group":
                kwargs["queryset"] = Group.objects.filter(id__in=get_user_groups(request))

        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_queryset(self, request):
        # if not superuser, a user can see only see legends from his groups
        qs = super().get_queryset(request)

        if request.user.is_superuser:
            return qs
        return qs.filter(group__in=get_user_groups(request))

    def save_related(self, request, form, formset, change):
        super(WmsLegendAdmin, self).save_related(request, form, formset, change)

        legend = form.instance
        try:
            geoserver_api = GeoServerApi()

            sld = create_sld_file(legend, "de")
            geoserver_api.create_style(legend.name_de, sld)

            sld = create_sld_file(legend, "en")
            geoserver_api.create_style(legend.name_en, sld)

        except Exception as e:
            print('Error: ' + str(e))
