from django.contrib import admin  # noqa
from django.contrib.auth.models import Group, User  # noqa
from django import forms  # noqa
from django.core.exceptions import ValidationError  # noqa
from django.utils.html import format_html  # noqa
from django.urls import reverse  # noqa

from main.models import Bucket, BucketUser, S3User  # noqa
from main.lib.utils.backend import get_user_groups  # noqa
from metadata.api import GeonetworkApi  # noqa

from main.lib.utils.s3_utils import *  # noqa

from main.models import S3User, StaEndpoint  # noqa
from data_import.models import CsvParser  # noqa


class BucketUserInlineForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.s3user = kwargs.pop('s3user')
        super(BucketUserInlineForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()

        current_s3user = self.s3user

        if 's3user' not in cleaned_data:
            raise ValidationError("A bucket should have at least one assigned user.")

        connected_user = cleaned_data['s3user'].name

        if cleaned_data['id']:
            old_connected_user = self.instance.s3user.name
            if connected_user != current_s3user:
                if old_connected_user == current_s3user:
                    raise ValidationError("You cannot switch your bucket-assignment to another user.")

        if cleaned_data.get('DELETE', False):
            if connected_user == current_s3user:
                raise ValidationError("Your assignment to the bucket can only be removed by another administrator.")

        return cleaned_data

class BucketUserInlineFormset(forms.BaseInlineFormSet):
    model = BucketUser

    def clean(self):
        count_users = 0
        for form in self.forms:
            cleaned_data = form.cleaned_data

            if cleaned_data == {}:
                continue    # form is empty and will not be saved. Skip validation.

            if not cleaned_data.get('DELETE', False):
                count_users += 1

            if count_users == 0:
                raise ValidationError("A bucket should have at least one assigned user.")

class BucketUserInline(admin.TabularInline):
    model = BucketUser
    form = BucketUserInlineForm
    formset = BucketUserInlineFormset
    min_num = 1
    extra = 0

    def formfield_for_foreignkey(self, db_field, request, **kwargs):

        if not request.user.is_superuser:
            # if not superuser, a user can only select users of his groups
            if db_field.name == "s3user":
                users = User.objects.filter(groups__in=get_user_groups(request))
                kwargs["queryset"] = S3User.objects.filter(user__in=users)

        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class BucketForm(forms.ModelForm):
    model = Bucket

    def clean(self):
        cleaned_data = super().clean()

        group = cleaned_data.get("group")
        sta = cleaned_data.get("sta_endpoint")
        parser = cleaned_data.get("csv_parser")

        if group and sta:
            if group != sta.group:
                self.add_error("sta_endpoint", "Must select StaEndpoint of same Group as Bucket.")

        if group and parser:
            if group != parser.group:
                self.add_error("csv_parser", "Must select CsvParser of same Group as Bucket.")

class BucketAdmin(admin.ModelAdmin):
    model = Bucket
    form = BucketForm
    inlines = [BucketUserInline]
    list_display = ('name', 'group', 'connect_to_geoserver', 'connect_to_thredds', 'connect_to_sta', 'connect_to_wfs', 'link_to_event_logs')
    ordering = ('name',)

    fieldsets = [
        (None, {
            'fields': ('name', 'group', 'public_folder', ('quota', 'size_unit'), ),
        }),
        (None, {
            'fields': (('connect_to_geoserver', 'wms_file_suffix', 'wfs_file_suffix'), 'connect_to_thredds', ('connect_to_sta', 'sta_endpoint'), 'connect_to_wfs', 'csv_parser', ),
        })
    ]

    def link_to_event_logs(self, obj):
        link = reverse('admin:main_bucketevent_changelist')
        return format_html('<a href="{}?bucket={}">Logs {}</a>'.format(link, obj.id, obj))

    def get_formset_kwargs(self, request, obj, inline, prefix):
        try:
            s3user = S3User.objects.get(user=request.user)
            s3user = s3user.name
        except(S3User.DoesNotExist):
            s3user = ""
        return {
            **super().get_formset_kwargs(request, obj, inline, prefix),
            "form_kwargs": {"s3user": s3user},
        }

    def get_readonly_fields(self, request, obj=None):
        if request.user.is_superuser:
            if obj:
                return ["name", "public_folder"]
            else:
                return []
        else:
            return ["name", "group", "public_folder", "quota", "size_unit"]

    def formfield_for_foreignkey(self, db_field, request, **kwargs):

        if not request.user.is_superuser:
            # if not superuser, a user can only select parsers of his groups
            if db_field.name == "csv_parser":
                kwargs["queryset"] = CsvParser.objects.filter(group__in=get_user_groups(request))
            # if not superuser, a user can only select StaEndpoints of his groups
            if db_field.name == "sta_endpoint":
                kwargs["queryset"] = StaEndpoint.objects.filter(group__in=get_user_groups(request))

        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_queryset(self, request):
        # if not superuser, a user can see only his buckets
        qs = super().get_queryset(request)

        if request.user.is_superuser:
            return qs
        return qs.filter(group__in=get_user_groups(request))

    def has_change_permission(self, request, obj=None):
        return has_admin_permission(request, obj)

    def has_delete_permission(self, request, obj=None):
        return has_admin_permission(request, obj)

    def save_model(self, request, obj: Bucket, form, change):
        # if obj.id is None:
        #     try:
        #         create_bucket(obj.name)
        #         if obj.public_folder:
        #             create_public_folder_in_bucket(obj.name)
        #             upload_file(obj.name, "public/MINIO_README.txt", "main/fixtures/MINIO_README.txt")
        #     except Exception as e:
        #         print('Cannot create Bucket: ' + str(e))
        if obj.geonetwork_group is None:
            try:
                geonetwork_api = GeonetworkApi()
                obj.geonetwork_group = geonetwork_api.create_group(obj.name)
            except Exception as e:
                print('Cannot create Geonetwork-group: ' + str(e))

        # try:
        #     set_quota(obj.name, str(obj.quota) + obj.size_unit)
        # except Exception as e:
        #     print('Cannot set Quota for bucket: ' + str(e))

        super().save_model(request, obj, form, change)

    # def save_related(self, request, form, formset, change):
    #     super(BucketAdmin, self).save_related(request, form, formset, change)
    #
    #     bucket = form.instance
    #     bucket_users = BucketUser.objects.filter(bucket=bucket).select_related('s3user')
    #
    #     for bucket_user in bucket_users:
    #         try:
    #             if bucket_user.has_write_permission:
    #                 give_write_access_to_bucket(bucket.name, bucket_user.s3user)
    #             else:
    #                 remove_access_to_bucket(bucket.name, bucket_user.s3user)    # if changed from write to read, remove write access
    #                 give_read_access_to_bucket(bucket.name, bucket_user.s3user)
    #
    #         except Exception as e:
    #             print('Cannot assign user {} to bucket {}: {}'.format(bucket_user.s3user.name, bucket.name, str(e)))

def has_admin_permission(request, bucket: Bucket):
    if request.user.is_superuser:
        return True
    try:
        bucket_user = BucketUser.objects.get(bucket=bucket, s3user__user=request.user)
        if bucket_user.is_bucket_admin:
            return True
    except:
        return False
