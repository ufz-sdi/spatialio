from django.contrib import admin
from django_tabbed_changeform_admin.admin import DjangoTabbedChangeformAdmin

from django.db import models
from django.forms import Textarea
from django.utils.html import format_html

from main.models import Area, Bucket, Project
from main.lib.utils.backend import get_user_groups


class ProjectAdmin(DjangoTabbedChangeformAdmin, admin.ModelAdmin):
    model = Project
    list_display = ('__str__', 'link_to_viewer')

    formfield_overrides = {
        models.TextField: {'widget': Textarea(
            attrs={'rows': 15,
                   'cols': 100})},
    }
    fieldsets = [
        (None, {
             'fields': ('bucket', ('name_de', 'name_en'), ('is_english_version_active', 'enable_print_function')),
            'classes': ('tab-basic',),
        }),
        (None, {
            'fields': (('default_lon', 'default_lat',  'default_zoom'),),
            'classes': ('tab-basic',),
        }),
        (None, {
            'fields': (('show_in_gallery', 'gallery_image',  'position_in_gallery'), 'base_map',),
            'classes': ('tab-basic',),
        }),
        ('Imprint-Page', {
            'fields': (('imprint_text_de', 'imprint_text_en',),('print_annotation_de', 'print_annotation_en',),),
            'classes': ('tab-imprint',),
        }),
        ('About-Page', {
            'fields': (('about_text_de', 'about_text_en',),),
            'classes': ('tab-about',),
        }),
        ('FAQ-Page', {
            'fields': (('faq_text_de', 'faq_text_en',),),
            'classes': ('tab-faq',),
        }),
        ('Teaser-Text', {
            'fields': (('teaser_de', 'teaser_en',),),
            'classes': ('tab-teaser',),
        }),
        ('Admin-Configuration', {
            'fields': (('name_in_url', 'name_in_backend'), ('maintenance_notification_de', 'maintenance_notification_en'), ('viewer'), 'theme',),
            'classes': ('tab-admin',),
        }),
    ]

    tabs = [
        ("Basic Information", ["tab-basic"]),
        ("Imprint", ["tab-imprint"]),
        ("About", ["tab-about"]),
        ("FAQ", ["tab-faq"]),
        ("Teaser", ["tab-teaser"]),
        ("Admin", ["tab-admin"]),
    ]

    def link_to_viewer(self, obj):
        link = "http://localhost:3000/gdi/" + obj.name_in_backend
        if obj.viewer:
            link = obj.viewer
        return format_html('<a target="_blank" href="{}">{}</a>'.format(link, link))

    def get_readonly_fields(self, request, obj):
        fields = ['bucket', 'name_in_url', 'name_in_backend', 'maintenance_notification_de', 'maintenance_notification_en', 'theme', 'position_in_gallery', 'viewer']
        if request.user.is_superuser:
            return []
        return fields

    def get_queryset(self, request):
        # if not superuser, a user can see only projects from his groups
        qs = super().get_queryset(request)

        buckets = Bucket.objects.filter(group__in=get_user_groups(request))
        if request.user.is_superuser:
            return qs
        return qs.filter(bucket__in=buckets)
