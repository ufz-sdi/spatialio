from django import forms # noqa
from django.contrib import admin # noqa
from django_tabbed_changeform_admin.admin import DjangoTabbedChangeformAdmin # noqa

from main.models import Area, AreaLayer, DataVariable, Bucket, Project, WmsLayer, WmsLayerProcess, WmsLayerLegend, WmsLegend, ConnectedLayer # noqa
from main.forms.layer.area_layer import FkAreaLayerInline # noqa
from main.lib.admin.filter import AreaFilter, BucketFilter # noqa

from main.lib.utils.backend import get_user_groups, NETCDF_HELPER_VARIABLES # noqa
from main.lib.api.GeoserverApi import GeoServerApi # noqa

from django.db import models # noqa
from django.forms import Textarea # noqa


class DataVariableFilter(admin.SimpleListFilter):
    title = 'Variable'
    parameter_name = 'variable'

    def lookups(self, request, model_admin):
        result = []
        if request.user.is_superuser:
            variables = DataVariable.objects.exclude(name__in=NETCDF_HELPER_VARIABLES)
        else:
            variables = DataVariable.objects.filter(group__in=get_user_groups(request)).exclude(name__in=NETCDF_HELPER_VARIABLES)

        for variable in variables:
            result.append((variable.id, variable.name))

        result.append((0, 'None'))

        return result

    def queryset(self, request, query_set):

        if self.value():
            if int(self.value()) == 0:
                return query_set.filter(variable__isnull=True)

            return query_set.filter(variable=self.value())
        else:
            return query_set


class WmsLayerLegendInline(admin.TabularInline):
    model = WmsLayerLegend
    autocomplete_fields = ['legend']
    min_num = 0
    extra = 0
    classes = ["tab-legend-inline"]

    def formfield_for_foreignkey(self, db_field, request, **kwargs):

        if not request.user.is_superuser:
            # if not superuser, a user can only select legends from his groups
            if db_field.name == "legend":
                kwargs["queryset"] = WmsLegend.objects.filter(group__in=get_user_groups(request))

        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class LayerProcessInline(admin.TabularInline):
    model = WmsLayerProcess
    min_num = 0
    extra = 0
    classes = ["tab-process-inline"]

    def get_readonly_fields(self, request, obj):
        if request.user.is_superuser:
            return []
        return ['in_cache']


class ConnectedLayerInline(admin.TabularInline):
    model = ConnectedLayer
    autocomplete_fields = ['referenced_wms_layer']
    fk_name = 'wms_layer'
    min_num = 0
    extra = 0
    classes = ["tab-connection-inline"]

    def formfield_for_foreignkey(self, db_field, request, **kwargs):

        if not request.user.is_superuser:
            # if not superuser, a user can only select layers from his buckets
            if db_field.name == "referenced_wms_layer":
                kwargs["queryset"] = WmsLayer.objects.filter(variable__group__in=get_user_groups(request))

        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class WmsLayerAdmin(DjangoTabbedChangeformAdmin, admin.ModelAdmin):
    model = WmsLayer
    inlines = [FkAreaLayerInline, WmsLayerLegendInline, ConnectedLayerInline, LayerProcessInline ]
    list_display = ('__str__', 'variable', 'layer_name', 'get_position', 'get_is_active', 'get_is_default')
    list_filter = (BucketFilter, AreaFilter, DataVariableFilter,)
    save_as = True

    search_fields = ["layer_name", "name_de", "name_en", "info_de", "info_en", "wms_url"]

    formfield_overrides = {
        models.TextField: {'widget': Textarea(
            attrs={'rows': 7,
                   'cols': 100})},
    }

    fieldsets = [
        (None, {
            'fields': (('name_de', 'name_en'), 'bucket', 'file_path', ),
            'classes': ('tab-basic',),
        }),
        (None, {
            'fields': ('layer_name', 'wms_url', 'variable', 'epsg_code', ('axis_label_x', 'axis_label_y'), 'time_steps', 'no_data_value', 'store_in_geowebcache'),
            'classes': ('tab-wms',),
        }),
        (None, {
            'fields': (('unit_de', 'unit_en'), ('info_de', 'info_en'), ),
            'classes': ('tab-description',),
        }),
        (None, {
            'fields': (('y_axis_min', 'y_axis_max'), 'time_format', 'scale_factor', 'opacity', 'is_point_request_enabled', 'num_decimals', 'uncertainty_path',),
            'classes': ('tab-config',),
        })
    ]

    tabs = [
        ("Basic Information", ["tab-basic"]),
        ("WMS", ["tab-wms"]),
        ("Config", ["tab-config"]),
        ("Description", ["tab-description"]),
        ("Legends", ["tab-legend-inline"]),
        ("Processes", ["tab-process-inline"]),
        ("Connected Layers", ["tab-connection-inline"]),
        ("Layer-Group (Area)", ["tab-area-inline"]),
    ]

    @admin.display(description='is active')
    def get_is_active(self, obj):
        area_layer = AreaLayer.objects.get(wms_layer=obj)
        if area_layer:
            return area_layer.is_active
        return False

    @admin.display(description='is default')
    def get_is_default(self, obj):
        area_layer = AreaLayer.objects.get(wms_layer=obj)
        if area_layer:
            return area_layer.is_default_on_start
        return False

    @admin.display(description='position')
    def get_position(self, obj):
        area_layer = AreaLayer.objects.get(wms_layer=obj)
        if area_layer:
            return area_layer.position
        return False

    def get_readonly_fields(self, request, obj):
        fields = ['wms_url', 'layer_name', 'bucket', 'file_path', 'variable', 'axis_label_x', 'axis_label_y', 'time_steps', 'store_in_geowebcache']
        if request.user.is_superuser:
            return ['bucket']
        return fields

    def get_queryset(self, request):
        # if not superuser, a user can see only see layers from his groups
        qs = super().get_queryset(request)

        if request.user.is_superuser:
            return qs
        return qs.filter(bucket__group__in=get_user_groups(request))

    def save_related(self, request, form, formset, change):
        super(WmsLayerAdmin, self).save_related(request, form, formset, change)

        wms_layer = form.instance
        try:
            layer_legends = WmsLayerLegend.objects.filter(wms_layer=wms_layer).order_by('position')
            geoserver_api = GeoServerApi()
            for index, layer_legend in enumerate(layer_legends):
                if index == 0:
                    geoserver_api.add_style(layer_legends[0].legend.name_de, wms_layer.layer_name, True)
                    geoserver_api.add_style(layer_legends[0].legend.name_en, wms_layer.layer_name, True)
                else:
                    geoserver_api.add_style(layer_legend.legend.name_de, wms_layer.layer_name)
                    geoserver_api.add_style(layer_legend.legend.name_en, wms_layer.layer_name)

                if wms_layer.store_in_geowebcache:
                    self.modify_gwc(wms_layer.layer_name, layer_legend.legend.name_en, "seed")
                    self.modify_gwc(wms_layer.layer_name, layer_legend.legend.name_en, "seed")


        except Exception as e:
            print('{}'.format(str(e)))
