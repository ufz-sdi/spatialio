from django.contrib import admin    # noqa
from django_tabbed_changeform_admin.admin import DjangoTabbedChangeformAdmin    # noqa

from main.models import AreaLayer, Bucket, GeojsonLayer    # noqa
from main.forms.layer.area_layer import FkAreaLayerInline    # noqa
from main.lib.admin.filter import AreaFilter, BucketFilter    # noqa

from main.lib.utils.backend import get_user_groups    # noqa


class GeojsonLayerAdmin(DjangoTabbedChangeformAdmin, admin.ModelAdmin):
    model = GeojsonLayer
    list_display = ('__str__', 'bucket', 'file_path', 'get_position', 'get_is_active', 'get_is_default')
    inlines = [FkAreaLayerInline ]
    list_filter = (BucketFilter, AreaFilter)
    save_as = True

    fieldsets = [
        (None, {
            'fields': (('name_de', 'name_en'), 'bucket', 'url', 'file_path' ),
            'classes': ('tab-basic',),
        }),
        (None, {
            'fields': (('info_de', 'info_en'), ),
            'classes': ('tab-description',),
        })
    ]

    tabs = [
        ("Basic Information", ["tab-basic"]),
        ("Description", ["tab-description"]),
        ("Layer-Group (Area)", ["tab-area-inline"]),
    ]

    @admin.display(description='is active')
    def get_is_active(self, obj):
        area_layer = AreaLayer.objects.get(geojson_layer=obj)
        if area_layer:
            return area_layer.is_active
        return False

    @admin.display(description='is default')
    def get_is_default(self, obj):
        area_layer = AreaLayer.objects.get(geojson_layer=obj)
        if area_layer:
            return area_layer.is_default_on_start
        return False

    @admin.display(description='position')
    def get_position(self, obj):
        area_layer = AreaLayer.objects.get(geojson_layer=obj)
        if area_layer:
            return area_layer.position
        return False

    def get_queryset(self, request):
        # if not superuser, a user can see only see geojson-layers from his buckets
        qs = super().get_queryset(request)

        if request.user.is_superuser:
            return qs
        return qs.filter(bucket__group__in=get_user_groups(request))

    def formfield_for_foreignkey(self, db_field, request, **kwargs):

        if not request.user.is_superuser:
            # if not superuser, a user can only select buckets from his groups
            if db_field.name == "bucket":
                kwargs["queryset"] = Bucket.objects.filter(group__in=get_user_groups(request))

        return super().formfield_for_foreignkey(db_field, request, **kwargs)