from django.contrib import admin    # noqa
from django_tabbed_changeform_admin.admin import DjangoTabbedChangeformAdmin    # noqa

from main.models import AreaLayer, Bucket, StaLayer, StaObservedPropertyFilter    # noqa
from main.forms.layer.area_layer import FkAreaLayerInline    # noqa
from main.lib.admin.filter import AreaFilter    # noqa

from main.lib.utils.backend import get_user_groups    # noqa


class StaObservedPropertyFilterInline(admin.TabularInline):
    model = StaObservedPropertyFilter
    min_num = 0
    extra = 0
    classes = ["tab-property-inline"]


class StaLayerAdmin(DjangoTabbedChangeformAdmin, admin.ModelAdmin):
    model = StaLayer
    inlines = [FkAreaLayerInline, StaObservedPropertyFilterInline]
    list_display = ('__str__', 'get_position', 'get_is_active', 'get_is_default')
    list_filter = (AreaFilter,)
    save_as = True

    fieldsets = [
        (None, {
            'fields': (('name_de', 'name_en'), 'endpoint', 'legend' ),
            'classes': ('tab-basic',),
        }),
        (None, {
            'fields': (('info_de', 'info_en'), ('y_axis_min', 'y_axis_max'), 'time_format', ),
            'classes': ('tab-description',),
        }),
        (None, {
            'fields': ('cluster_minimum_size', 'cluster_scale_factor'),
            'classes': ('tab-optional',),
        })
    ]

    tabs = [
        ("Basic Information", ["tab-basic"]),
        ("Description", ["tab-description"]),
        ("Properties", ["tab-property-inline"]),
        ("Optional", ["tab-optional"]),
        ("Layer-Group (Area)", ["tab-area-inline"]),
    ]

    @admin.display(description='is active')
    def get_is_active(self, obj):
        area_layer = AreaLayer.objects.get(sta_layer=obj)
        if area_layer:
            return area_layer.is_active
        return False

    @admin.display(description='is default')
    def get_is_default(self, obj):
        area_layer = AreaLayer.objects.get(sta_layer=obj)
        if area_layer:
            return area_layer.is_default_on_start
        return False

    @admin.display(description='position')
    def get_position(self, obj):
        area_layer = AreaLayer.objects.get(sta_layer=obj)
        if area_layer:
            return area_layer.position
        return False

    def get_queryset(self, request):
        # if not superuser, a user can see only see sta-layers from his groups
        qs = super().get_queryset(request)

        if request.user.is_superuser:
            return qs
        return qs.filter(endpoint__group__in=get_user_groups(request))
