from django.contrib import admin

from main.models import Area, AreaLayer, LayerListFilterItem
from main.lib.utils.backend import get_user_groups


class FkAreaLayerInline(admin.StackedInline):
    model = AreaLayer
    classes = ["tab-area-inline"]

    fieldsets = [
        (None, {
            'fields': (('area', 'is_active'), ),
        }),
    ]

    min_num = 0
    max_num = 1
    extra = 1

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if not request.user.is_superuser:
            # if not superuser, a user can only select areas from projects belonging to the bucket
            if db_field.name == "area":
                kwargs["queryset"] = Area.objects.filter(project__bucket__group__in=get_user_groups(request))

        return super().formfield_for_foreignkey(db_field, request, **kwargs)
