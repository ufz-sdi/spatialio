from django.contrib import admin

from main.models import StaEndpoint, StaThingProperty

from main.lib.utils.backend import get_user_groups


class StaThingPropertyInline(admin.TabularInline):
    model = StaThingProperty
    min_num = 0
    extra = 0


class StaEndpointAdmin(admin.ModelAdmin):
    model = StaEndpoint

    inlines = [StaThingPropertyInline]
    save_as = True

    fieldsets = [
        (None, {
            'fields': ('group', 'base_url', ('username', 'password') ),
        }),
    ]

    def get_queryset(self, request):
        # if not superuser, a user can see only see sta-endpoints from his groups
        qs = super().get_queryset(request)

        if request.user.is_superuser:
            return qs
        return qs.filter(group__in=get_user_groups(request))
