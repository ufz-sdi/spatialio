from django import forms # noqa
from django.contrib import admin # noqa
from django_tabbed_changeform_admin.admin import DjangoTabbedChangeformAdmin # noqa
from main.forms.layer.area_layer import FkAreaLayerInline    # noqa
from main.lib.utils.backend import get_user_groups # noqa
from main.lib.api.GeoserverApi import GeoServerApi # noqa

from main.models import WfsLayer # noqa
from data_import.lib.utils import create_wfs_data_store_name   # noqa

from django.db import models # noqa
from django.forms import Textarea # noqa

class WfsLayerAdmin(DjangoTabbedChangeformAdmin, admin.ModelAdmin):
    model = WfsLayer
    list_display = ('__str__', 'workspace', 'data_store_name')
    save_as = True
    inlines = [FkAreaLayerInline ]

    search_fields = ["name_de", "name_en", "info_de", "info_en", "workspace", "data_store_name"]

    formfield_overrides = {
        models.TextField: {'widget': Textarea(
            attrs={'rows': 7,
                   'cols': 100})},
    }

    fieldsets = [
        (None, {
            'fields': (('name_de', 'name_en'), 'bucket', 'file_path', ),
            'classes': ('tab-basic',),
        }),
        (None, {
            'fields': ('url', 'workspace', 'data_store_name', 'publish', 'is_created', 'logs'),
            'classes': ('tab-wfs',),
        }),
        (None, {
            'fields': ('cluster_minimum_size', 'cluster_scale_factor'),
            'classes': ('tab-optional',),
        }),
        (None, {
            'fields': ('info_de', 'info_en'),
            'classes': ('tab-description',),
        }),
    ]

    tabs = [
        ("Basic Information", ["tab-basic"]),
        ("WFS", ["tab-wfs"]),
        ("Optional", ["tab-optional"]),
        ("Description", ["tab-description"]),
        ("Layer-Group (Area)", ["tab-area-inline"]),
    ]

    def get_readonly_fields(self, request, obj):
        fields = ['workspace', 'data_store_name', 'is_created', 'logs']
        if obj:
            fields.extend(['bucket', 'file_path'])
            if obj.publish:
                fields.append('publish')
        return fields

    def get_queryset(self, request):
        # if not superuser, a user can see only see layers from his groups
        qs = super().get_queryset(request)

        if request.user.is_superuser:
            return qs
        return qs.filter(bucket__group__in=get_user_groups(request))

    def save_model(self, request, obj: WfsLayer, form, change):

        if not obj.workspace:
            obj.workspace = obj.bucket.name

        if not obj.data_store_name:
            obj.data_store_name = create_wfs_data_store_name(obj.workspace, obj.file_path)

        super().save_model(request, obj, form, change)

        if obj.publish:
            if not obj.is_created:
                api = GeoServerApi()
                api.create_postgis_datastore(obj)
                obj.logs = api.logs
                if not api.has_error:
                    obj.is_created = True
                super().save_model(request, obj, form, change)
