from django.contrib import admin

from main.models import S3User


class S3UserAdmin(admin.ModelAdmin):
    model = S3User
    list_display = ('name', "user_name")
    ordering = ('name',)
    search_fields = ["name"]

    def user_name(self, obj):
        if obj.user:
            return obj.user.first_name + " " + obj.user.last_name
        return '-'

    def get_queryset(self, request):
        # Prefetch related objects
        return super(S3UserAdmin, self).get_queryset(request).select_related('user')
