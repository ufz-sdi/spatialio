import nested_admin

from django.db import models
from django.forms import Textarea
from django_tabbed_changeform_admin.admin import DjangoTabbedChangeformAdmin

from main.models import Area, AreaLayer, ListFilter, ListFilterItem
from main.lib.utils.backend import get_user_groups


class AreaLayerInline(nested_admin.NestedTabularInline):
    model = AreaLayer
    classes = ['tab-area-layer-inline']

    fieldsets = [
        (None, {
            'fields': (('position', 'is_active', 'is_default_on_start'), ),
        }),
    ]

    def has_add_permission(self, request, obj=None):
        return False


class ListFilterItemInline(nested_admin.NestedStackedInline):
    model = ListFilterItem
    min_num = 0
    extra = 0
    classes = ['collapse']

    fieldsets = [
        (None, {
            'fields': (('name_de', 'name_en'), 'list_filter'),
        }),
    ]


class ListFilterInline(nested_admin.NestedStackedInline):
    model = ListFilter
    inlines = [ListFilterItemInline]
    min_num = 0
    extra = 0
    classes = ["tab-filter-inline"]


class AreaAdmin(DjangoTabbedChangeformAdmin, nested_admin.NestedModelAdmin):
    model = Area
    inlines = [ListFilterInline, AreaLayerInline]
    list_display = ('__str__', 'project', 'position', 'is_default_on_start', 'is_active')
    ordering = ('project', 'position')

    fieldsets = [
        (None, {
            'fields': (('project', 'position', 'is_active'), ('name_de', 'name_en'), ),
            'classes': ('tab-basic',),
        }),
        (None, {
            'fields': (('info_de', 'info_en'), ),
            'classes': ('tab-description',),
        }),
    ]

    tabs = [
        ("Basic Information", ["tab-basic"]),
        ("Description", ["tab-description"]),
        ("Filter", ["tab-filter-inline"]),
        ("Layer", ["tab-area-layer-inline"]),
    ]

    formfield_overrides = {
        models.TextField: {'widget': Textarea(
            attrs={'rows': 15,
                   'cols': 100})},
    }

    def get_queryset(self, request):
        # if not superuser, a user can see only areas of his projects
        qs = super().get_queryset(request)

        if request.user.is_superuser:
            return qs
        return qs.filter(project__bucket__group__in=get_user_groups(request))
