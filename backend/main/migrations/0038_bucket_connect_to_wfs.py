# Generated by Django 4.2.16 on 2024-11-15 10:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0037_bucket_wfs_file_suffix_wfslayer_arealayer_wfs_layer'),
    ]

    operations = [
        migrations.AddField(
            model_name='bucket',
            name='connect_to_wfs',
            field=models.BooleanField(default=False),
        ),
    ]
