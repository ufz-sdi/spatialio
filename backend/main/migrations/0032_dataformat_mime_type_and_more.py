# Generated by Django 4.2.13 on 2024-09-27 06:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0031_alter_author_created_by_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='dataformat',
            name='mime_type',
            field=models.CharField(default='tbd', max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='metadatarecord',
            name='doi_request_approved',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='metadatarecord',
            name='passed_doi_preconditions',
            field=models.CharField(blank=True, help_text='When a DOI is requested, it will be checked for compliance before accepting.', max_length=4000, null=True),
        ),
        migrations.AddField(
            model_name='metadatarecord',
            name='publish_record',
            field=models.BooleanField(default=False, help_text='If a record is published, it will be visible without needing to be logged in to Geonetwork'),
        ),
        migrations.AddField(
            model_name='metadatarecord',
            name='request_doi',
            field=models.BooleanField(default=False, help_text='A DOI can only be requested for public records'),
        ),
        migrations.AlterField(
            model_name='metadatarecord',
            name='language',
            field=models.CharField(choices=[(None, ''), ('bg', 'Bulgarian'), ('cs', 'Czech'), ('da', 'Danish'), ('nl', 'Dutch'), ('en', 'English'), ('et', 'Estonian'), ('fi', 'Finnish'), ('de', 'German'), ('el', 'Greek'), ('fr', 'French'), ('hr', 'Croatian'), ('hu', 'Hungarian'), ('it', 'Italien'), ('ga', 'Irish'), ('lv', 'Latvian'), ('lt', 'Lithuanian'), ('mt', 'Maltese'), ('pl', 'Polish'), ('pt', 'Portuguese'), ('ro', 'Romanian'), ('sk', 'Slovak'), ('sl', 'Slovenian'), ('es', 'Spanish'), ('sv', 'Swedish')], default='en', max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='metadatarecord',
            name='version',
            field=models.CharField(blank=True, help_text='Changing this field will overwrite any previous version. For a new version, create a new Metadata Record.', max_length=12, null=True),
        ),
    ]
