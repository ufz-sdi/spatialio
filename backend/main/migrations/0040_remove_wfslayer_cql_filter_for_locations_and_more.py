# Generated by Django 4.2.16 on 2024-11-29 07:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0039_wfslayer_cql_filter_for_locations_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='wfslayer',
            name='cql_filter_for_locations',
        ),
        migrations.RemoveField(
            model_name='wfslayer',
            name='cql_filter_for_properties',
        ),
        migrations.RemoveField(
            model_name='wfslayer',
            name='layer_name',
        ),
        migrations.RemoveField(
            model_name='wfslayer',
            name='wfs_url',
        ),
        migrations.AddField(
            model_name='geojsonlayer',
            name='url',
            field=models.CharField(default='', max_length=4000),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='wfslayer',
            name='prefix',
            field=models.CharField(default='', max_length=1000),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='wfslayer',
            name='workspace',
            field=models.CharField(default='', max_length=1000),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='wfslayer',
            name='file_path',
            field=models.CharField(blank=True, max_length=4000, null=True),
        ),
    ]
