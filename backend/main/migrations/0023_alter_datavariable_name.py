# Generated by Django 4.2.11 on 2024-04-10 08:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0022_alter_area_options_alter_geojsonlayer_options_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='datavariable',
            name='name',
            field=models.CharField(max_length=1000, unique=True),
        ),
    ]
