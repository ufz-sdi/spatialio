# Generated by Django 4.2.10 on 2024-03-14 13:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0018_bucketevent_bucket'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bucketevent',
            name='job_status',
        ),
        migrations.AddField(
            model_name='bucketevent',
            name='is_success',
            field=models.BooleanField(default=False, null=True),
        ),
    ]
