# Generated by Django 4.2.18 on 2025-02-13 18:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0042_wmslayer_store_in_geowebcache'),
    ]

    operations = [
        migrations.AddField(
            model_name='wfslayer',
            name='is_created',
            field=models.BooleanField(default=False),
        ),
    ]
