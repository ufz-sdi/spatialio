# Generated by Django 4.1.7 on 2023-09-26 14:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='bucket',
            name='connect_to_thredds',
            field=models.BooleanField(default=False),
        ),
    ]
