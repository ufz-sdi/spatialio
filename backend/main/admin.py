from django.contrib import admin     # noqa

from django.contrib.auth.admin import User, UserAdmin, Group, GroupAdmin     # noqa
from data_import.models import CsvParser, CsvImportJob, PointData     # noqa

from admin_interface.models import Theme     # noqa
from admin_interface.admin import ThemeAdmin     # noqa

from .forms.layer.geojson_layer import GeojsonLayerAdmin
from .forms.layer.sta_endpoint import StaEndpointAdmin
from .forms.layer.sta_layer import StaLayerAdmin
from .forms.layer.wms_layer import WmsLayerAdmin
from .forms.layer.wfs_layer import WfsLayerAdmin
from .forms.project import ProjectAdmin
from .forms.data_variable import DataVariableAdmin
from .forms.wms_legend import WmsLegendAdmin
from .forms.list_filter import ListFilterItemAdmin
from .forms.sta_legend import StaLegendAdmin
from .forms.bucket import BucketAdmin
from .forms.bucket_event import BucketEventAdmin
from .forms.s3user import S3UserAdmin
from .forms.area import AreaAdmin

from metadata.forms.metadata_record import MetadataRecordAdmin
from metadata.forms.author import AuthorAdmin
from metadata.forms.organization import OrganizationAdmin

from data_import.forms.csv_parser import CsvParserAdmin     # noqa
from data_import.forms.csv_import_job import CsvImportJobAdmin     # noqa
from data_import.forms.point_data import PointDataAdmin     # noqa

from .models import *
from metadata.models import Author, DataFormat, License, MetadataRecord, Organization


class CustomAdminSite(admin.AdminSite):
    def index(self, request, extra_context=None):
        extra_context = extra_context or {}

        if not request.user.is_superuser:
            extra_context['title'] = ''
            self.index_template = 'admin/user_app_list.html'
        else:
            self.index_template = 'admin/index.html'
        return super(CustomAdminSite, self).index(request, extra_context)

    def app_index(self, request, app_label, extra_context=None):
        if not extra_context:
            extra_context = {}

        if not request.user.is_superuser:
            extra_context['title'] = ''
            self.app_index_template = 'admin/user_app_list.html'
        else:
            self.app_index_template = 'admin/app_index.html'

        return super(CustomAdminSite, self).app_index(request, app_label, extra_context=extra_context)


admin.site = CustomAdminSite()

admin.site.register(User, UserAdmin)
admin.site.register(Group, GroupAdmin)
admin.site.register(Theme, ThemeAdmin)

admin.site.register(CsvParser, CsvParserAdmin)
admin.site.register(CsvImportJob, CsvImportJobAdmin)
admin.site.register(PointData, PointDataAdmin)

admin.site.register(Project, ProjectAdmin)
admin.site.register(Area, AreaAdmin)
admin.site.register(DataVariable, DataVariableAdmin)
admin.site.register(WmsLegend, WmsLegendAdmin)
admin.site.register(GeojsonLayer, GeojsonLayerAdmin)
admin.site.register(StaEndpoint, StaEndpointAdmin)
admin.site.register(StaLegend, StaLegendAdmin)
admin.site.register(StaLayer, StaLayerAdmin)
admin.site.register(WmsLayer, WmsLayerAdmin)
admin.site.register(WfsLayer, WfsLayerAdmin)
admin.site.register(Bucket, BucketAdmin)
admin.site.register(BucketEvent, BucketEventAdmin)
admin.site.register(S3User, S3UserAdmin)
admin.site.register(MetadataRecord, MetadataRecordAdmin)
admin.site.register(Author, AuthorAdmin)
admin.site.register(Organization, OrganizationAdmin)
admin.site.register(ListFilterItem, ListFilterItemAdmin)
admin.site.register(Process)
admin.site.register(License)
admin.site.register(DataFormat)

admin.site.enable_nav_sidebar = False
