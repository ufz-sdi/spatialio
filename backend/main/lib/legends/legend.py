import os

from jinja2 import Template
from main.models import WmsColor, WmsLegend


def create_sld_file(legend: WmsLegend, locale: str):

    with open(os.path.dirname(__file__) + '/legend_template.xml', encoding='utf-8') as template:
        template = Template(template.read())
    colors = []

    for color in WmsColor.objects.filter(legend=legend).order_by('position'):

        color_name = color.name_en if (locale == 'en') else color.name_de
        colors.append({
            'color': color.color,
            'upper_limit': color.upper_limit,
            'name': color_name,
            'is_invisible': color.is_invisible
        })

    legend_name = legend.name_en if (locale == 'en') else legend.name_de

    return template.render(
        name=legend_name,
        title=legend_name,
        legend_type=legend.type,
        colors=colors
    )
