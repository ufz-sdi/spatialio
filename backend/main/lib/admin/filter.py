from django.contrib import admin # noqa
from main.models import Area, Bucket # noqa
from main.lib.utils.backend import get_user_groups # noqa


class BucketFilter(admin.SimpleListFilter):
    title = 'Bucket'
    parameter_name = 'bucket'

    def lookups(self, request, model_admin):
        result = []
        if request.user.is_superuser:
            buckets = Bucket.objects.all()
        else:
            buckets = Bucket.objects.filter(group__in=get_user_groups(request))

        for bucket in buckets:
            result.append((bucket.id, bucket.name))

        return result

    def queryset(self, request, query_set):

        if self.value():
            return query_set.filter(bucket=self.value())
        else:
            return query_set


class AreaFilter(admin.SimpleListFilter):
    title = 'Areas'
    parameter_name = 'area_layer'

    def lookups(self, request, model_admin):
        result = []
        if request.user.is_superuser:
            areas = Area.objects.all()
        else:
            areas = Area.objects.filter(project__bucket__group__in=get_user_groups(request))

        for area in areas:
            result.append((area.id, area.name_de))

        result.append((0, 'None'))

        return result

    def queryset(self, request, query_set):

        if self.value():
            if int(self.value()) == 0:
                return query_set.filter(arealayer__area_id__isnull=True)

        if self.value():
            print(self.value())
            return query_set.filter(arealayer__area=self.value())
        else:
            return query_set