import os
from main.models import WmsColor    # noqa


GDI_ADMIN_PERMISSION_GROUP = 'GDI-Users'

TIME_BNDS = 'time_bnds'
NETCDF_HELPER_VARIABLES = [TIME_BNDS, 'easting', 'northing']


def get_user_groups(request):
    return request.user.groups.all().exclude(name=GDI_ADMIN_PERMISSION_GROUP)


def get_geoserver_url_for_frontend(url=None):
    if not url:
        url = os.environ.get('GEOSERVER_URL').rstrip(' /')
    if 'http://geoserver:8080' in url:
        return url.replace('geoserver:8080', 'localhost:8080')
    return url


def create_legend_map(legend, locale):

    prop = "name_en" if locale == "en" else "name_de"

    result = []
    for color in WmsColor.objects.filter(legend=legend).order_by('position'):
        result.append({
            "color": color.color,
            "label": getattr(color, prop),
            "quantity": int(color.upper_limit)
        })

    return result

