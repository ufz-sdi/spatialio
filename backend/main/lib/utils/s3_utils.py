import os
import json
import s3fs  # noqa
from minio import Minio  # noqa
from minio.notificationconfig import (NotificationConfig, QueueConfig)  # noqa
from minio_cli_wrapper.mc import Mc  # noqa

from main.models import S3User  # noqa


def get_s3_filesystem():

    params = {
        "endpoint_url": os.environ.get("MINIO_ENDPOINT")
    }

    region_name = os.environ.get("MINIO_REGION_NAME", None)
    if region_name:
        params["region_name"] = region_name

    return s3fs.S3FileSystem(
        client_kwargs=params,
        key=os.environ.get("MINIO_USERNAME"),
        secret=os.environ.get("MINIO_PASSWORD")
    )


def download_minio_file(bucket_name, object_key, output_file_path):
    minio_client = get_minio_client()
    try:
        minio_client.fget_object(bucket_name, object_key, output_file_path)
        return True
    except:
        return False


def get_minio_download_link(bucket, object_key):
    minio_client = get_minio_client()
    return minio_client.presigned_get_object(bucket, object_key)


def list_minio_folder(bucket_name, prefix=None):
    minio_client = get_minio_client()
    if prefix is None:
        return minio_client.list_objects(bucket_name, recursive=True)
    else:
        return minio_client.list_objects(bucket_name, prefix, recursive=True)


def create_bucket(bucket_name):
    minio_client = get_minio_client()
    try:
        minio_client.make_bucket(bucket_name)

        # apply bucket-event-notifications in the postgres database
        config = NotificationConfig(
            queue_config_list=[
                QueueConfig(
                    "arn:minio:sqs:test:PRIMARY:postgresql",
                    ["s3:ObjectCreated:*", "s3:ObjectRemoved:*"]
                )
            ]
        )

        minio_client.set_bucket_notification(bucket_name, config)
        return True
    except Exception as e:
        print(e)
        return False


def create_public_folder_in_bucket(bucket_name):
    minio_client = get_minio_client()
    try:
        policy = {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Principal": {"AWS": "*"},
                    "Action": "s3:GetObject",
                    "Resource": "arn:aws:s3:::" + bucket_name + "/public/*",
                },
            ],
        }
        minio_client.set_bucket_policy(bucket_name, json.dumps(policy))

        return True
    except Exception as e:
        print(e)
        return False


def upload_file(bucket_name, object_key, file_name):
    minio_client = get_minio_client()
    try:
        minio_client.fput_object(bucket_name, object_key, file_name)
        return True
    except:
        return False


def delete_file(bucket_name, object_key):
    minio_client = get_minio_client()
    try:
        minio_client.remove_object(bucket_name, object_key)
        return True
    except:
        return False


def get_minio_client(use_cli_client=False):
    endpoint = os.environ.get("MINIO_ENDPOINT")
    if 'https://' in endpoint:
        host = endpoint.replace('https://', '')
        use_https = True
    else:
        host = endpoint.replace('http://', '')
        use_https = False

    if not use_cli_client:
        return Minio(
            endpoint=host,
            access_key=os.environ.get("MINIO_USERNAME"),
            secret_key=os.environ.get("MINIO_PASSWORD"),
            secure=use_https
        )
    else:
        return Mc(
            host,
            secure=use_https,
            access_key=os.environ.get("MINIO_USERNAME"),
            secret_key=os.environ.get("MINIO_PASSWORD")
        )


def give_write_access_to_bucket(bucket_name: str, s3user: S3User):
    mc = get_minio_client(True)

    policy_name = bucket_name + "_rw"  # test-bucket_rw

    policies = mc.get_policy_entities(s3user.name)

    if policy_name not in policies:
        mc.policy_add(
            policy_name,
            create_bucket_policy_write(bucket_name)
        )

    if s3user.ldap_dn:
        mc.policy_set_ldap_user(policy_name, s3user.ldap_dn)
    else:
        mc.policy_set_user(policy_name, s3user.name)


def give_read_access_to_bucket(bucket_name: str, s3user: S3User):
    mc = get_minio_client(True)

    policy_name = bucket_name + "_r"  # test-bucket_r

    policies = mc.get_policy_entities(s3user.name)

    if policy_name not in policies:
        mc.policy_add(
            policy_name,
            create_bucket_policy_read(bucket_name)
        )

    if s3user.ldap_dn:
        mc.policy_set_ldap_user(policy_name, s3user.ldap_dn)
    else:
        mc.policy_set_user(policy_name, s3user.name)


def remove_access_to_bucket(bucket_name: str, s3user: S3User):
    mc = get_minio_client(True)
    for access_type in ["_rw", "_r"]:
        policy_name = bucket_name + access_type

        if s3user.ldap_dn:
            mc.policy_unset_ldap_user(policy_name, s3user.ldap_dn)
        else:
            mc.policy_unset_user(policy_name, s3user.name)

def create_bucket_policy_write(bucket_name):
    return {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": [
                    "s3:ListBucketMultipartUploads",
                    "s3:GetBucketLocation",
                    "s3:ListBucket"
                ],
                "Resource": [
                    "arn:aws:s3:::{}".format(bucket_name)
                ]
            },
            {
                "Effect": "Allow",
                "Action": [
                    "s3:AbortMultipartUpload",
                    "s3:DeleteObject",
                    "s3:GetObject",
                    "s3:ListMultipartUploadParts",
                    "s3:PutObject"
                ],
                "Resource": [
                    "arn:aws:s3:::{}/*".format(bucket_name)
                ]
            }
        ]
    }

def create_bucket_policy_read(bucket_name):
    return {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": [
                    "s3:GetBucketLocation",
                    "s3:GetObject",
                    "s3:ListBucket",
                    "s3:ListBucketMultipartUploads"
                ],
                "Resource": [
                    "arn:aws:s3:::{}".format(bucket_name)
                ]
            }
        ]
    }

def set_quota(bucket_name: str, quota: str):
    mc = get_minio_client(True)
    mc.set_bucket_quota(bucket_name, quota)
