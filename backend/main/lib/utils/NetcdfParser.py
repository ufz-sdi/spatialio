import xarray as xr     # noqa


class NetcdfParser:

    def __init__(self, fs):
        self.fs = fs

    def get_nc_variables(self, file_path: str):
        variables =  []
        try:
            with self.fs.open(file_path, 'rb') as netcdf_file:

                file = xr.open_dataset(netcdf_file, engine='h5netcdf')

                for k, v in file.data_vars.items():
                    variables.append(k)
                file.close()

        except Exception as e:
            print("Could not read header from netcdf-file: {}".format(e))
        return variables
