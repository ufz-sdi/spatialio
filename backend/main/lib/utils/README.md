
### Example Notification

    {
        "Records": [{
                "s3": {
                    "bucket": {
                        "arn": "arn:aws:s3:::test", 
                        "name": "test", 
                        "ownerIdentity":{
                            "principalId": "AKIAIOSFODNN7EXAMPLE"
                        }
                    }, 
                    "object": {
                        "key": "minio.png", 
                        "sequencer": "169A3A94F7A0B939"
                    }, 
                    "configurationId": "Config", 
                    "s3SchemaVersion": "1.0"
                }, 
                "source": {
                    "host": "172.18.0.5", 
                    "port": "", 
                    "userAgent": "MinIO (linux; amd64) minio-go/v7.0.13 MinIO Console/(dev)"
                }, 
                "awsRegion": "", 
                "eventName": "s3:ObjectRemoved:Delete", 
                "eventTime": "2021-08-11T10:43:46.731Z", 
                "eventSource": "minio:s3", 
                "eventVersion": "2.0", 
                "userIdentity": {
                    "principalId": "AKIAIOSFODNN7EXAMPLE"
                }, 
                "responseElements": {
                    "content-length": "116", 
                    "x-amz-request-id": "169A3A94F78DEF1A", 
                    "x-minio-deployment-id": "4cd81e43-e911-49b6-95eb-468fa89d9995", 
                    "x-minio-origin-endpoint": "http://172.18.0.5:9000"
                }, 
                "requestParameters": {
                    "region": "", 
                    "principalId": "AKIAIOSFODNN7EXAMPLE", 
                    "sourceIPAddress": "172.18.0.5"
                }
            }
        ]
    }
