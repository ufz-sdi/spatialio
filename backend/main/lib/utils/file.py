import secrets
import string
from datetime import datetime

from main.models import Bucket # noqa
from metadata.models import MetadataRecord # noqa


class File:
    def __init__(self, bucket_name: str, relative_path):
        self.bucket_name: str = bucket_name
        self.suffix: str = bucket_name
        self.relative_path: str = relative_path
        self.absolute_path: str = '{}/{}'.format(bucket_name, self.relative_path)

    def is_netcdf(self) -> bool:
        return self.relative_path.endswith('.nc')

    def is_geotiff(self) -> bool:
        if not self.relative_path.endswith('*.tif') and self.relative_path.endswith('.tif'):
            return True
        return False

    def is_csv(self) -> bool:
        return self.relative_path.endswith('.csv')

    def is_shapefile(self) -> bool:
        return self.relative_path.endswith('.zip')

    def get_file_name(self) -> str:
        name_parts = self.relative_path.split('/')
        file_name = name_parts[-1]
        return file_name.split('.')[0]


def is_wms_file(bucket: Bucket, file: File):
    suffix = bucket.wms_file_suffix
    if suffix:
        for file_type in ["tif", "nc"]:
            ending = "{}.{}".format(suffix.lower(), file_type)
            if file.relative_path.lower().endswith(ending):
                return True
        return False
    else:
        return True


def is_zip_file_for_import(bucket: Bucket, file: File):
    suffix = bucket.wfs_file_suffix
    if suffix:
        for file_type in ["zip"]:
            ending = "{}.{}".format(suffix.lower(), file_type)
            if file.relative_path.lower().endswith(ending):
                return True
        return False
    else:
        return True


def create_file_identifier():
    chars = string.ascii_letters + string.digits
    result = ''

    for _ in range(32):
        result += secrets.choice(chars)

    return result


def get_or_create_metadata_record(bucket, file_path):
    metadata_record = get_metadata_record(bucket, file_path)
    if metadata_record:
        return metadata_record
    else:
        metadata_record = MetadataRecord(
            bucket=bucket,
            file_path=file_path,
            dataset_path=get_folder_name(file_path),
            record_id=create_file_identifier(),
        )
        return metadata_record


def get_metadata_record(bucket: Bucket, file_path: str):
    try:
        metadata_record = MetadataRecord.objects.get(bucket=bucket, file_path=file_path)
        return metadata_record
    except MetadataRecord.DoesNotExist:
        return None


def get_folder_name(path: str):
    path = path.strip('/')
    pieces = path.split('/')

    if len(pieces) > 1:
        del pieces[-1]
        return '/'.join(pieces)
    else:
        return None


def search_metadata_record(bucket, pieces):
    del pieces[-1]

    if len(pieces) == 0:
        search_path = None
    else:
        search_path = '/'.join(pieces)

    try:
        return MetadataRecord.objects.get(bucket=bucket, dataset_path=search_path)
    except MetadataRecord.DoesNotExist:
        if search_path:
            return search_metadata_record(bucket, pieces)
        else:
            return None
