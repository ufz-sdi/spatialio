import os
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry  # noqa
from xml.etree import ElementTree

import json
from main.models import WfsLayer # noqa

from geoserver.catalog import Catalog  # noqa

GEOSERVER_MOSAIC_DIR = "/opt/geoserver/data_dir/mosaic"


class GeoServerApi:

    def __init__(self):

        self.url = os.environ.get("GEOSERVER_URL")
        self.username = os.environ.get("GEOSERVER_USERNAME")
        self.password = os.environ.get("GEOSERVER_PASSWORD")

        self.cat = Catalog(self.url + "/rest/", self.username, self.password)

        self.coverages = []

        self.logs = []
        self.has_error = False
        self.has_timeout = False

        retry_strategy = Retry(
            total=3,
            backoff_factor=2,
            allowed_methods=['DELETE', 'GET', 'HEAD', 'OPTIONS', 'PUT', 'POST']
        )
        adapter = HTTPAdapter(max_retries=retry_strategy)
        self.session = requests.Session()
        self.session.mount("https://", adapter)
        self.session.mount("http://", adapter)


    def log(self, log):
        self.logs.append(str(log))

    def reset(self):
        self.logs = []
        self.has_error = False
        self.has_timeout = False

    def create_workspace(self, workspace_name):
        # check for existing workspace
        workspace = self.cat.get_workspace(workspace_name)

        if workspace is None:
            try:
                self.cat.create_workspace(workspace_name, workspace_name)
                self.log("Workspace created: {}".format(workspace_name))
            except Exception as e:
                self.log("Workspace could not be created: {}".format(e))
                self.has_error = True
        else:
            self.log("Workspace already exists. Existing Workspace will be used: {}".format(workspace_name))

    def create_datastore(self, workspace, data_store, object_url, data_type, store_type='coveragestores'):

        self.create_workspace(workspace)

        if store_type == 'datastores':
            headers = {'Content-Type': 'application/zip'}
        else:
            headers = {"Content-type": "text/plain"}
        url = '{0}/rest/workspaces/{1}/{2}/{3}/{4}'.format(
            self.url, workspace, store_type, data_store, data_type)

        try:
            response = self.session.put(url, data=object_url, auth=(self.username, self.password), headers=headers, timeout=5)
            if response.status_code != 201:
                self.log("Create status not ok:")
                self.log(response.content)
                self.has_error = True
            else:
                self.log("Datastore {} created successfully".format(data_store))
        except requests.exceptions.RetryError:
            self.log("Timeout, datastore could not be created: {}".format(data_store))
            self.has_timeout = True
        except Exception as e:
            self.log("Error, datastore could not be created: {}".format(e))
            self.has_error = True

    def create_postgis_datastore(self, layer: WfsLayer):
        self.create_workspace(layer.workspace)
        url = '{0}/rest/workspaces/{1}/datastores'.format(self.url, layer.workspace, layer.data_store_name)
        headers = {'Content-Type': 'application/json'}
        body = {
            'dataStore': {
                'name': layer.data_store_name,
                'connectionParameters': {
                    'entry': [
                        {"@key": "host", "$": os.environ.get('POSTGRES_HOST')},
                        {"@key": "port", "$": "5432"},
                        {"@key": "database", "$": os.environ.get('POSTGRES_DB')},
                        {"@key": "schema", "$": "geoserver"},
                        {"@key": "user", "$": os.environ.get('POSTGRES_USER')},
                        {"@key": "passwd", "$": os.environ.get('POSTGRES_PASSWORD')},
                        {"@key": "dbtype", "$": "postgis"},
                    ]
                }
            }
        }

        try:
            response = self.session.post(url, auth=(self.username, self.password), headers=headers, data=json.dumps(body), timeout=5)
            if response.status_code != 201:
                self.log("Create status not ok:")
                self.log(response.content)
                self.has_error = True
            else:
                self.log("POSTGIS Datastore {} created successfully".format(layer.data_store_name))
        except requests.exceptions.RetryError:
            self.log("Timeout, datastore could not be created: {}".format(layer.data_store_name))
            self.has_timeout = True
        except Exception as e:
            self.log("Error, datastore could not be created: {}".format(e))
            self.has_error = True

        for table in ["feature", "property", "timerecord", "timeseries"]:
            self._add_csv_layer(layer.workspace, layer.data_store_name, layer.data_store_name + "_" + table)

    def _add_csv_layer(self, workspace, data_store_name, layer):
        url = "{0}/rest/workspaces/{1}/datastores/{2}/featuretypes".format(self.url, workspace, data_store_name)
        headers = {'Content-Type': 'application/xml'}
        data = f"""<featureType><name>{layer}</name></featureType>"""

        try:
            response = self.session.post(url, auth=(self.username, self.password), headers=headers, data=data, timeout=5)
            if response.status_code != 201:
                self.log("Create status not ok:")
                self.log(response.content)
                self.has_error = True
            else:
                self.log("POSTGIS Layer {} created successfully".format(layer))
        except requests.exceptions.RetryError:
            self.log("Timeout, layer could not be created: {}".format(layer))
            self.has_timeout = True
        except Exception as e:
            self.log("Error, layer could not be created: {}".format(e))
            self.has_error = True

    def modify_gwc(self, layer_name, style_name, type):
        url = '{0}/gwc/rest/seed/{1}.json'.format(self.url, layer_name)
        headers = {"content-type": "application/json"}
        seed_data = {
            "seedRequest": {
                "name": layer_name,
                "gridSetId": "EPSG:4326",
                "zoomStart": 0,
                "zoomStop": 10,
                "type": type,
                "threadCount": 4,
                "tileFormat": "image/png",
                "parameter": {
                    "STYLES": style_name
                }
            }
        }

        try:
            response = self.session.post(url, auth=(self.username, self.password), headers=headers, data=json.dumps(seed_data), timeout=5)
            if response.status_code not in [200, 201]:
                self.log("Create status not ok:")
                self.log(response.content)
                self.has_error = True
            else:
                self.log("GWC created successfully")
        except Exception as e:
            print("GeoWebCache could not be created: {}".format(e))
            self.has_error = True


    def _create_new_style(self, style_xml):
        try:
            headers = {"Content-type": "application/vnd.ogc.sld+xml"}
            url = '{0}/rest/styles?raw=true'.format(self.url)
            response = self.session.post(url=url, auth=(self.username, self.password), headers=headers, data=style_xml, timeout=5)
            if response.status_code != 201:
                print('Style could not be created: {}'.format(response.content))
                self.has_error = True
            else:
                print('Style created successfully')
        except Exception as e:
            print('Style could not be filled/updated: {}'.format(str(e)))
            self.has_error = True

    def _update_style(self, style_name, style_xml):
        try:
            headers = {"Content-type": "application/vnd.ogc.sld+xml"}
            url = '{0}/rest/styles/{1}?raw=true'.format(self.url, style_name)
            response = self.session.put(url=url, auth=(self.username, self.password), headers=headers, data=style_xml, timeout=5)
            if response.status_code != 200:
                print('Style could not be updated: {}'.format(response.content))
                self.has_error = True
            else:
                print('Style updated successfully')
        except Exception as e:
            print('Style could not be filled/updated: {}'.format(str(e)))
            self.has_error = True

    def create_style(self, style_name, style_xml):
        try:
            url = '{0}/rest/styles/{1}.sld'.format(self.url, style_name)
            response = self.session.get(url=url, auth=(self.username, self.password), timeout=5)

            style_xml = style_xml.encode('utf-8')

            if response.status_code != 200:
                print('Style does not exist yet. Style will be added as new.')
                self._create_new_style(style_xml)
            else:
                print('Update existing style')
                self._update_style(style_name, style_xml)
        except Exception as e:
            print('Could not retrieve style: {}'.format(str(e)))
            self.has_error = True


    def add_style(self, style_name, layer_name, default=False):

        headers = {"content-type": "application/json"}
        url = '{0}/rest/layers/{1}/styles?default={2}'.format(self.url, layer_name, default)

        body = {
            "style": {
                "name": style_name,
                "filename": style_name + '.sld'
            }
        }

        try:
            response = self.session.post(url, data=json.dumps(body), auth=(self.username, self.password), headers=headers, timeout=5)

            if response.status_code != 201:
                print("Create status not ok: {}".format(response.content))
                self.has_error = True
            else:
                print("Style linked successfully")

        except Exception as e:
            print("Style {0} could not be linked to layer {1}: {2}".format(style_name, layer_name, e))
            self.has_error = True

    def configure_layer(self, workspace, coverage_store, native_name):

        url = "{0}/rest/workspaces/{1}/coveragestores/{2}/coverages/{3}".format(
            self.url, workspace, coverage_store, native_name)
        headers = {"content-type": "application/xml"}
        time_data = (
            "<coverage>"
            "<name>" + coverage_store + "_" + native_name + "</name>"
            "<title>" + coverage_store + "_" + native_name + "</title>"
            "<nativeName>" + native_name + "</nativeName>"
            "<enabled>true</enabled>"
            "<metadata>"
            "<entry key='time'>"
            "<dimensionInfo>"
            "<enabled>true</enabled>"
            "<presentation>LIST</presentation>"
            "<units>ISO8601</units>"
            "<defaultValue>"
            "<strategy>MAXIMUM</strategy>"
            "</defaultValue>"
            "</dimensionInfo>"
            "</entry>"
            "</metadata>"
            "</coverage>"
        )
        try:
            response = self.session.put(url, data=time_data, auth=(self.username, self.password), headers=headers, timeout=5)
            if response.status_code not in [200, 201]:
                self.log("Time dimension status for coverage {} not ok: {}".format(native_name, response.content))
                self.has_error = True
            else:
                self.log("Time dimension for coverage {} enabled successfully".format(native_name))
        except requests.exceptions.RetryError:
            self.log("Timeout, coverage_store could not be configured: {}".format(coverage_store))
            self.has_timeout = True
        except Exception as e:
            self.log("Time dimension for coverage {} could not be enabled: {}".format(native_name, e))
            self.has_error = True


    def configure_wfs_layer_name(self, workspace, coverage_store, native_name):
        url = "{0}/rest/workspaces/{1}/datastores/{2}/featuretypes/{3}".format(
            self.url, workspace, coverage_store, native_name)
        headers = {"content-type": "application/xml"}
        data = (
            "<featureType>"
            "<name>" + coverage_store + "</name>"
            "<title>" + coverage_store + "</title>"
            "<nativeName>" + native_name + "</nativeName>"
            "<enabled>true</enabled>"
            "</featureType>"
        )
        try:
            response = self.session.put(url, data=data, auth=(self.username, self.password), headers=headers, timeout=5)
            if response.status_code not in [200, 201]:
                raise Exception(response.content)
            else:
                self.log("Setting name for coverage {} successfully".format(native_name))
        except Exception as e:
            self.log("Setting name for coverage {} not ok: {}".format(native_name, e))
            self.has_error = True


    def get_wms_timesteps(self, workspace, layer_name):
        wms_url = '{}/ows?service=wms&version=1.3.0&request=GetCapabilities&namespace={}'.format(os.environ.get("GEOSERVER_URL"), workspace)

        try:
            response = self.session.get(wms_url)
            xml = ElementTree.fromstring(response.text)

            for item in xml.findall(".//{http://www.opengis.net/wms}Layer"):
                if item.get("queryable") is not None:

                    name = item.find('{http://www.opengis.net/wms}Name').text
                    if name == layer_name:
                        for dimension in item.findall("{http://www.opengis.net/wms}Dimension"):
                            if dimension.get('name') == 'time':
                                time_steps = dimension.text.strip()
                                self.log("Layer-timesteps fetched")
                                return time_steps.split(',')
            return []

        except Exception as e:
            self.log("Could not fetch timesteps {}: {}".format(layer_name, str(e)))
            self.has_error = True

    def delete_datastore(self, workspace, data_store, store_type='coveragestores'):
        url = "{0}/rest/workspaces/{1}/{2}/{3}?recurse=true".format(self.url, workspace, store_type, data_store)
        try:
            response = self.session.delete(url, auth=(self.username, self.password), timeout=5)
            if response.status_code not in [200, 201]:
                self.log("Error in deleting datastore {}: {}".format(data_store, response.content))
                self.has_error = True
            else:
                self.log("Deleting datastore successful: {}".format(data_store))
        except Exception as e:
            self.log("Error in deleting datastore {}: {}".format(data_store, e))
            self.has_error = True

    def add_allowed_url(self, title, allowed_url):

        url = "{}rest/urlchecks".format(self.url)
        headers = {"content-type": "application/xml"}

        # ^https://minio.ufz.de.*$
        body = (
            "<regexUrlCheck>"
            "<name>" + title + "</name>"
            "<description></description>"
            "<enabled>true</enabled>"
            "<regex>^" + allowed_url + ".*$</regex>"
            "</regexUrlCheck>"
        )

        try:
            response = self.session.post(
                url,
                data=body,
                auth=(self.username, self.password),
                headers=headers,
                timeout=5
            )
            if response.status_code not in [200, 201]:
                print("Error in setting url-check: {}".format(response.content))
            else:
                print("Adding URL-Check successful: {}".format(allowed_url))

        except Exception as e:
            print("Error in setting url-check: {}".format(e))
