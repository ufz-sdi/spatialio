import requests
from django.shortcuts import get_object_or_404
from main.models import Process, ProcessCache, WmsLayer, WmsLayerProcess, DataVariable


def _scale_and_round(value, scale_factor, decimal_count):
    return str(round((float(value) / scale_factor), decimal_count))


def _create_request_body(layer, process, polygon, time_steps):
    try:
        file_path = layer.file_path

        data = dict()
        data["raster"] = file_path
        data["crs"] = layer.epsg_code
        data["no_data_value"] = layer.no_data_value

        if file_path.endswith('tif'):
            data["raster_format"] = "tiff"
        else:
            data["raster_format"] = "netcdf"

        data[polygon[0]] = polygon[1]

        if len(time_steps) > 0:
            time_steps_reformat = []
            for time_step in time_steps:
                time_steps_reformat.append(time_step.split('T')[0])
            data["time"] = time_steps_reformat
        data["variable"] = get_object_or_404(DataVariable, id=layer.variable_id).name

        data["spatial_aggregation_type"] = process.spatial_aggregation_type
        if process.temporal_aggregation_type is not None:
            data["temporal_aggregation_type"] = process.temporal_aggregation_type
        if process.type == 'magnitude':
            data["threshold"] = float(process.sub_type)
        if process.type == 'anomaly':
            reference = process.sub_type.split(':')[1]
            reference = reference.split('-')
            data["reference"] = [reference[0], reference[1]]

        return data
    except Exception as e:
        print('Request body for process call could not be created: ' + str(e))
        return None


class AggregationApi:
    def __init__(self, url):
        self.url = url

    def call_process(self, layer, process, polygon, time_steps):
        url = self.url + "processes/" + process.type + "/execution"
        header = {"Content-type": "application/json"}
        inputs = _create_request_body(layer, process, polygon, time_steps)
        request_data = {'inputs': inputs}
        try:
            response = requests.post(url=url, json=request_data, headers=header)
            results = response.json()["results"]

            if process.type == 'aggregation':
                scaled_and_rounded = list()
                for value in results["values"]:
                    scaled_and_rounded.append(_scale_and_round(value, layer.scale_factor, 2))
                results["values"] = scaled_and_rounded
            elif process.type == 'magnitude':
                results["magnitude"] = _scale_and_round(results["magnitude"], layer.scale_factor, 2)
                results["intensity"] = _scale_and_round(results["intensity"], layer.scale_factor, 2)
            elif process.type == 'anomaly':
                results["absolute"] = _scale_and_round(results["absolute"], layer.scale_factor, 2)
                results["relative"] = _scale_and_round(results["relative"], layer.scale_factor, 2)

            return results
        except Exception as e:
            print('Process throws an error: {}'.format(str(e)))
            return None

    def get_cache(self, layer, process, polygon, time_steps):
        try:
            layer_process = WmsLayerProcess.objects.get(wms_layer=layer, process=process)
        except WmsLayerProcess.DoesNotExist:
            print('No cache settings for layer')
            return None

        if layer_process.in_cache is True:

            try:
                cache_entry = ProcessCache.objects.get(
                    wms_layer=layer,
                    process=process,
                    time_steps=time_steps,
                    polygon_id=polygon[1]
                )
                return cache_entry.results

            except ProcessCache.DoesNotExist:
                print('No result found in cache')
                return None

        else:
            print('Cache inactive for this layer-process-combination')
            return None
