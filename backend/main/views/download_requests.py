
from django.http import HttpResponse, HttpResponseRedirect # noqa
from django.shortcuts import get_object_or_404 # noqa

from zipfile import ZipFile
from io import BytesIO

from metadata.models import MetadataRecord # noqa
from main.lib.utils.s3_utils import get_minio_download_link, get_s3_filesystem, list_minio_folder # noqa


def get_zip_download(request, record_id):

    metadata_record = get_object_or_404(MetadataRecord, record_id=record_id)

    file_paths = _get_download_targets(metadata_record)

    s3 = get_s3_filesystem()
    zipped_files = BytesIO()
    with ZipFile(zipped_files, 'w') as zip_file:
        for file in file_paths:
            s3_file = s3.open(file, 'rb')
            zip_file.writestr(file.removeprefix(metadata_record.dataset_path), s3_file.read())

    metadata_filename = metadata_record.title + '_' + str(metadata_record.geonetwork_uuid)

    # response
    response = HttpResponse(zipped_files.getvalue())
    response['Content-Type'] = 'application/zip'
    response['Content-Disposition'] = 'attachment; filename="' + metadata_filename + '.zip"'
    return response


def _get_download_targets(metadata_record: MetadataRecord):

    bucket_name = metadata_record.bucket.name
    filenames = []
    data_files = list_minio_folder(bucket_name, metadata_record.dataset_path)
    for file in data_files:
        filenames.append(bucket_name + '/' + file.object_name)
    return filenames


def get_uncertainty(request, file_path):
    s3 = get_s3_filesystem()
    s3_file = s3.open(file_path, mode='rb')
    return HttpResponse(s3_file)
