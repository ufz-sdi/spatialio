import os
import json
from django.http import JsonResponse

from main.models import AreaLayer, Process
from main.lib.api.AggregationApi import AggregationApi

from django.views.decorators.csrf import csrf_exempt


def get_countries(request):
    with open("main/geojson/countries/2_hoch.geo.json") as f:
        data = json.load(f)
    return JsonResponse(data)


def get_districts_by_country(request, country_id):
    with open("main/geojson/districts/{}_admin.geojson".format(country_id)) as f:
        data = json.load(f)
    return JsonResponse(data)


def _get_layer(layer_id):
    try:
        layer = AreaLayer.objects.get(id=layer_id)
        return layer.wms_layer
    except AreaLayer.DoesNotExist:
        return None


def _get_process(type, sub_type):
    try:
        return Process.objects.get(type=type, sub_type=sub_type)
    except Process.DoesNotExist:
        return None


@csrf_exempt
def call_process(request):
    body = json.loads(request.body)
    proc_api = AggregationApi(os.environ.get("AGGREGATION_API_URL"))

    layer = _get_layer(body['layer_id'])
    process = _get_process(body['process_type'], body['process_sub_type'])

    if layer is not None and process is not None:
        cache_result = proc_api.get_cache(layer, process, body['polygon'], body['time_steps'])
        if cache_result is not None:
            cache_result['status'] = True
            return JsonResponse(cache_result)

        results = proc_api.call_process(layer, process, body['polygon'], body['time_steps'])
        if results is not None:
            results['status'] = True
            return JsonResponse(results)

    return JsonResponse({'status': False})
