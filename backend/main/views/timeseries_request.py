from datetime import datetime
import os
import json
import math
import requests

from django.http import HttpResponseNotFound, JsonResponse    # noqa
from django.shortcuts import get_object_or_404    # noqa
from pyproj import Transformer    # noqa
from main.models import AreaLayer, WfsLayer, WmsLayer, WmsLayerLegend, WmsColor, ConnectedLayer, REQUEST_TYPE_1, REQUEST_TYPE_2    # noqa

from main.views import views, sta_requests, wfs_requests    # noqa

from multiprocessing import Pool

coord_x = None
coord_y = None
locale_gl = ''
time = ''


def request_all_time_series(request, locale: str):

    global locale_gl
    locale_gl = locale

    chart_name = ''

    timeseries_collection = []
    map_popup_result = None
    single_result_collection = []

    # collect STA-Data

    sta_layer_id = request.GET.get('staLayer')
    sta_thing = request.GET.get('staThing')
    sta_property = request.GET.get('staObsProperty')

    if sta_layer_id and sta_thing and sta_property:
        try:
            area_layer = AreaLayer.objects.select_related('sta_layer').get(sta_layer_id=sta_layer_id)
        except AreaLayer.DoesNotExist:
            return HttpResponseNotFound('Error while retrieving sta-layer.')

        sta_layer = area_layer.sta_layer

        timeseries = _request_sta_timeseries(request, area_layer, sta_layer, sta_thing, sta_property, locale)

        timeseries_collection.append(timeseries)

        chart_name = timeseries['name']


    # collect WFS-Data

    wfs_layer_id = request.GET.get('wfsLayer')
    wfs_timeseries_id = request.GET.get('wfsTimeSeries')
    if wfs_layer_id and wfs_timeseries_id:
        try:
            wfs_layer = WfsLayer.objects.get(id=wfs_layer_id)
            timeseries = _request_wfs_timeseries(wfs_layer, wfs_timeseries_id)
            timeseries_collection.append(timeseries)
        except WfsLayer.DoesNotExist:
            return HttpResponseNotFound('Error while retrieving wfs-layer.')

    # collect WMS-Data

    wms_layer_id = request.GET.get('wmsLayer')
    global coord_x, coord_y
    coord_x = request.GET.get('x')
    coord_y = request.GET.get('y')

    if wms_layer_id and coord_x and coord_y:

        global time
        time = request.GET.get('time')

        try:
            area_layer = AreaLayer.objects.select_related('wms_layer').get(wms_layer_id=wms_layer_id)
        except AreaLayer.DoesNotExist:
            return HttpResponseNotFound('Error while retrieving wms-layer.')

        main_data_points = _request_time_series(area_layer.wms_layer, coord_x, coord_y)

        if len(main_data_points['x_values']) > 0:

            timeseries = _create_time_series(
                    area_layer,
                    area_layer.wms_layer,
                    main_data_points,
                    locale,
                    True
            )
            timeseries_collection.append(timeseries)

            if chart_name == '':
                chart_name = timeseries['name']

        map_popup_result = _create_single_result(area_layer.wms_layer, main_data_points, locale, time)

        single_result_collection = []

        layer_collection = []
        for connected_layer in ConnectedLayer.objects.filter(wms_layer=area_layer.wms_layer.id):
            layer = connected_layer.__dict__
            layer['ref_area_layer'] = AreaLayer.objects.get(wms_layer_id=connected_layer.referenced_wms_layer)
            layer['ref_wms_layer'] = WmsLayer.objects.get(id=layer['ref_area_layer'].wms_layer.id)
            layer_collection.append(layer)

        pool = Pool(processes=10)
        results = pool.map_async(_request_connected_layer_time_series, layer_collection).get()
        pool.close()

        for result in results:
            if result['timeSeries']:
                for timeSeries in result['timeSeries']:
                    timeseries_collection.append(timeSeries)
            if result['singleResult']:
                for singleResult in result['singleResult']:
                    single_result_collection.append(singleResult)

    data = {
        'chartName': chart_name,
        'mapPopupResult': map_popup_result,
        'timeSeriesCollection': timeseries_collection,
        'singleResultCollection': single_result_collection
    }

    return JsonResponse(data)


def _request_connected_layer_time_series(connected_layer):
    result = {'timeSeries': [], 'singleResult': []}

    connected_data_points = _request_time_series(connected_layer['ref_wms_layer'], coord_x, coord_y)

    if connected_layer['type'] == REQUEST_TYPE_1:
        if len(connected_data_points['x_values']) > 0:
            result['timeSeries'].append(
                _create_time_series(
                    connected_layer['ref_area_layer'],
                    connected_layer['ref_wms_layer'],
                    connected_data_points,
                    locale_gl,
                    'legendonly'
                )
            )

    if connected_layer['type'] == REQUEST_TYPE_2:
        result['singleResult'].append(
            _create_single_result(
                connected_layer['ref_wms_layer'],
                connected_data_points,
                locale_gl,
                time
            )
        )

    return result


def _request_time_series(wms_layer, coord_x, coord_y):
    transformer = Transformer.from_crs("EPSG:3857", wms_layer.epsg_code, always_xy=True)
    coord_transformed = transformer.transform(coord_x, coord_y)

    x = coord_transformed[0]
    y = coord_transformed[1]

    params = [
        'service=WCS',
        'version=2.0.1',
        'request=GetCoverage',
        'coverageId={}'.format(wms_layer.layer_name),
        'format=application%2Fprs.coverage%2Bjson',
        'subset={}({})'.format(wms_layer.axis_label_x, x),
        'subset={}({})'.format(wms_layer.axis_label_y, y)
    ]

    url = os.environ.get("GEOSERVER_URL") + '/ows'
    url = url + '?' + "&".join(params)

    response = requests.get(url)

    if response.status_code != 200:
        return {
            'x_values': [],
            'y_values': []
        }
    else:
        return parse_covjson(response.json(), wms_layer)


def _create_time_series(area_layer, wms_layer, data_points, locale, is_visible):

    name = wms_layer.name_de if locale == "de" else wms_layer.name_en
    unit = wms_layer.unit_de if locale == "de" else wms_layer.unit_en

    return {
        'x': data_points['x_values'],
        'y': data_points['y_values'],
        'type': 'scatter',
        'mode': 'lines+markers',
        'name': name,
        'position': area_layer.position,
        'timeFormat': wms_layer.time_format,
        'unit': unit,
        'yMin': wms_layer.y_axis_min,
        'yMax': wms_layer.y_axis_max,
        'visible': is_visible
    }


def _is_timeseries(data):
    if 't' in data['domain']['axes'] and len(_get_values(data)) > 1:
        return True
    return False


def _get_values(data):
    ranges = data['ranges']
    first_key = list(ranges.keys())[0]
    return ranges[first_key]['values']


def parse_covjson(data, wms_layer):

    digits = wms_layer.num_decimals if wms_layer.num_decimals else 4

    result = {
        'x_values': [],
        'y_values': []
    }

    values = _get_values(data)

    if _is_timeseries(data):

        timestamps = data['domain']['axes']['t']['values']

        if len(timestamps) != len(values):
            raise Exception('covJson result-set error')

        data_points = []

        for i in range(len(timestamps)):
            if values[i] == wms_layer.no_data_value or math.isnan(float(values[i])):
                continue

            value = round(values[i], digits)

            data_points.append({
                'timestamp': timestamps[i],
                'value': value / wms_layer.scale_factor
            })

        data_points.sort(key=lambda x: datetime.strptime(x['timestamp'], "%Y-%m-%dT%H:%M:%SZ"))

        for data_point in data_points:
            result['x_values'].append(data_point['timestamp'])
            result['y_values'].append(data_point['value'])

    else:

        if len(values) == 1:
            if values[0] != 'NaN':
                for value in values:
                    result['y_values'].append(round(value, digits) / wms_layer.scale_factor)

    return result


def _create_single_result(wms_layer, data_points, locale: str, time):

    value = None
    error = False

    if len(data_points['y_values']) > 0:

        if time:
            time = time.replace('.000Z', 'Z')
            for i, timestamp in enumerate(data_points['x_values']):
                if time == timestamp:
                    value = data_points['y_values'][i]

        if not value:
            value = data_points['y_values'][-1]

    else:
        error = True

    name = wms_layer.name_de if locale == "de" else wms_layer.name_en
    unit = wms_layer.unit_de if locale == "de" else wms_layer.unit_en

    return {
        'label': name,
        'unit': unit,
        'value': value,
        'time': time,
        'timeFormat': wms_layer.time_format,
        'textValue': replace_value_by_corresponding_label(value, wms_layer, locale),
        'error': error
    }


def _request_sta_timeseries(request, area_layer, sta_layer, thing_id, obs_property_id, locale):

    name = ""
    unit = ""

    response = sta_requests.proxy_sta_all_datastreams(request, sta_layer.endpoint_id, thing_id, obs_property_id)

    observations = json.loads(response.content)

    response = sta_requests.proxy_sta_datastream_properties(request, sta_layer.endpoint_id, thing_id, obs_property_id)

    datastreams = json.loads(response.content)

    for datastream in datastreams['value']:
        datastream_unit = datastream['unitOfMeasurement']['symbol']

        name = _get_name_for_sta_data(datastream['ObservedProperty'], locale)

        if unit == '':
            unit = datastream_unit

        if unit != datastream_unit:
            raise Exception('Datastreams have different units!')

    time_series = {
        'x': [],
        'y': [],
        'type': 'scatter',
        'mode': 'lines+markers',
        'name': name,
        'position': area_layer.position,
        'timeFormat': sta_layer.time_format,
        'unit': unit,
        'yMin': sta_layer.y_axis_min,
        'yMax': sta_layer.y_axis_max,
        'visible': True
    }

    for observation in observations['value']:
        time_series['x'].append(observation['resultTime'])
        time_series['y'].append(observation['result'])

    return time_series


def _request_wfs_timeseries(wfs_layer, timeseries_id):

    unit = ''

    response = wfs_requests.get_time_records(wfs_layer, timeseries_id)

    time_records = json.loads(response.content)

    time_series = {
        'x': [],
        'y': [],
        'type': 'scatter',
        'mode': 'lines+markers',
        'name': 'wfs',
        'position': 0,
        'timeFormat': None,
        'unit': unit,
        'yMin': None,
        'yMax': None,
        'visible': True
    }

    for time_record in time_records['features']:
        time_series['x'].append(time_record['properties']['date'])
        time_series['y'].append(time_record['properties']['value_number'])

    return time_series


def _get_name_for_sta_data(obs_property, locale):
    name = ''

    if obs_property['name']:
        name = obs_property['name']

    if 'properties' in obs_property:
        if locale == "de":
            if 'name_de' in obs_property['properties']:
                name = obs_property['properties']['name_de']
        else:
            if obs_property['properties']['name_en']:
                name = obs_property['properties']['name_en']

    return name


def replace_value_by_corresponding_label(value, wms_layer, locale):
    result = ''

    if not value:
        return result

    for layer_legend in WmsLayerLegend.objects.filter(wms_layer=wms_layer.id).order_by('position'):

        if layer_legend.replace_values_in_viewer_by_label:

            for color in WmsColor.objects.filter(legend=layer_legend.legend).order_by('position'):
                if int(value) == int(color.upper_limit):
                    result = color.name_en if locale == "en" else color.name_de

    return result
