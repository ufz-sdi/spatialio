import requests
from jinja2 import Template
from django.shortcuts import get_object_or_404  # noqa
from django.http import HttpResponse, JsonResponse    # noqa
from main.models import WfsLayer    # noqa


def get_time_records(wfs_layer: WfsLayer, timeseries_id: int):

    url = """
        {url}/{workspace}/ows?service=WFS
        &version=2.0.0
        &request=GetFeature
        &typeName={workspace}%3A{data_store_name}_timerecord
        &outputFormat=application%2Fjson
        &CQL_FILTER=timeseries_id%20=%20{timeseries_id}
        &propertyName=value_number,date
        &sortBy=date+A
    """.format(url=wfs_layer.url, workspace=wfs_layer.workspace, data_store_name=wfs_layer.data_store_name, timeseries_id=timeseries_id)

    return requests.get(url, timeout=(5, 60))


def wfs_features(request, layer_id):

        wfs_layer = get_object_or_404(WfsLayer, id=layer_id)

        data = _create_request_body(request, wfs_layer.workspace, wfs_layer.data_store_name)

        url = '{}/{}/ows?service=WFS&version=2.0.0&outputFormat=application%2Fjson&count=1'.format(wfs_layer.url, wfs_layer.workspace)

        response = requests.post(url, data=data)
        return HttpResponse(response, content_type='application/json')


def _create_request_body(request, workspace, data_store_name):

    count = request.GET.get('count')
    # bottomLeftLon, bottomLeftLat, topRightLon, topRightLat
    bbox = request.GET.get('bbox')
    coords = bbox.split(',')

    property_id = request.GET.get('property')
    begin_date = request.GET.get('beginDate')
    end_date = request.GET.get('endDate')
    threshold_value = request.GET.get('threshold')

    if property_id:

        filter_xml = """<?xml version="1.0" encoding="UTF-8"?>
            <wfs:GetFeature service="WFS" version="2.0.0" count="{{ count|e }}"
                xmlns:wfs="http://www.opengis.net/wfs/2.0"
                xmlns:fes="http://www.opengis.net/fes/2.0"
                xmlns:gml="http://www.opengis.net/gml/3.2"
                xmlns:sf="http://www.openplans.org/spearfish"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xsi:schemaLocation="http://www.opengis.net/wfs/2.0
                                    http://schemas.opengis.net/wfs/2.0/wfs.xsd
                                    http://www.opengis.net/gml/3.2
                                    http://schemas.opengis.net/gml/3.2.1/gml.xsd">
                <wfs:Query typeNames="{{ workspace|e }}:{{ data_store_name|e }}_feature {{ workspace|e }}:{{ data_store_name|e }}_timeseries" aliases="a b">
        
                  <fes:Filter>
                     <fes:And>
                        
                        <fes:BBOX>
                           <fes:ValueReference>a/geom</fes:ValueReference>
                           <gml:Envelope srsName="http://www.opengis.net/def/crs/epsg/0/4326">
                              <gml:lowerCorner>{{ lowerCornerLat|e }} {{ lowerCornerLon|e }}</gml:lowerCorner>
                              <gml:upperCorner>{{ upperCornerLat|e }} {{ upperCornerLon|e }}</gml:upperCorner>
                           </gml:Envelope>
                        </fes:BBOX>
                        
                        <fes:PropertyIsEqualTo>
                           <fes:ValueReference>a/id</fes:ValueReference>
                           <fes:ValueReference>b/feature_id</fes:ValueReference>
                        </fes:PropertyIsEqualTo>
                        
                        {% if property_id %}
                            <fes:PropertyIsEqualTo>
                               <fes:ValueReference>b/property_id</fes:ValueReference>
                               <fes:Literal>{{ property_id|e }}</fes:Literal>
                            </fes:PropertyIsEqualTo>
                        {% endif %}
                        
                        {% if begin_date %}
                           <fes:PropertyIsGreaterThanOrEqualTo>
                                <fes:ValueReference>b/max_date</fes:ValueReference>
                                <fes:Function name="dateParse">
                                    <fes:Literal>yyyy-MM-dd</fes:Literal>
                                    <fes:Literal>{{ begin_date|e }}</fes:Literal>
                                </fes:Function>
                           </fes:PropertyIsGreaterThanOrEqualTo>
                        {% endif %}

                        {% if end_date %}
                           <fes:PropertyIsLessThanOrEqualTo>
                                <fes:ValueReference>b/min_date</fes:ValueReference>
                                <fes:Function name="dateParse">
                                    <fes:Literal>yyyy-MM-dd</fes:Literal>
                                    <fes:Literal>{{ end_date|e }}</fes:Literal>
                                </fes:Function>
                           </fes:PropertyIsLessThanOrEqualTo>
                        {% endif %}
                        
                        {% if threshold_value %}
                           <fes:PropertyIsGreaterThanOrEqualTo>
                                <fes:ValueReference>b/max_value</fes:ValueReference>
                                <fes:Literal>{{ threshold_value|e }}</fes:Literal>
                           </fes:PropertyIsGreaterThanOrEqualTo>
                        {% endif %}
                        
                     </fes:And>
                  </fes:Filter>
                </wfs:Query>
            </wfs:GetFeature>"""

    else:
        filter_xml = """<?xml version="1.0" encoding="UTF-8"?>
            <wfs:GetFeature service="WFS" version="2.0.0" count="{{ count|e }}"
                xmlns:wfs="http://www.opengis.net/wfs/2.0"
                xmlns:fes="http://www.opengis.net/fes/2.0"
                xmlns:gml="http://www.opengis.net/gml/3.2"
                xmlns:sf="http://www.openplans.org/spearfish"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xsi:schemaLocation="http://www.opengis.net/wfs/2.0
                                    http://schemas.opengis.net/wfs/2.0/wfs.xsd
                                    http://www.opengis.net/gml/3.2
                                    http://schemas.opengis.net/gml/3.2.1/gml.xsd">
                <wfs:Query typeNames="{{ workspace|e }}:{{ data_store_name|e }}_feature">
                  <fes:Filter>
                     <fes:And>
                  
                        <fes:BBOX>
                           <fes:ValueReference>geom</fes:ValueReference>
                           <gml:Envelope srsName="http://www.opengis.net/def/crs/epsg/0/4326">
                              <gml:lowerCorner>{{ lowerCornerLat|e }} {{ lowerCornerLon|e }}</gml:lowerCorner>
                              <gml:upperCorner>{{ upperCornerLat|e }} {{ upperCornerLon|e }}</gml:upperCorner>
                           </gml:Envelope>
                        </fes:BBOX>
                            
                        {% if begin_date %}
                           <fes:PropertyIsGreaterThanOrEqualTo>
                                <fes:ValueReference>max_timerecord</fes:ValueReference>
                                <fes:Function name="dateParse">
                                    <fes:Literal>yyyy-MM-dd</fes:Literal>
                                    <fes:Literal>{{ begin_date|e }}</fes:Literal>
                                </fes:Function>
                           </fes:PropertyIsGreaterThanOrEqualTo>
                        {% endif %}
    
                        {% if end_date %}
                           <fes:PropertyIsLessThanOrEqualTo>
                                <fes:ValueReference>min_timerecord</fes:ValueReference>
                                <fes:Function name="dateParse">
                                    <fes:Literal>yyyy-MM-dd</fes:Literal>
                                    <fes:Literal>{{ end_date|e }}</fes:Literal>
                                </fes:Function>
                           </fes:PropertyIsLessThanOrEqualTo>
                        {% endif %}
                     </fes:And>
                  </fes:Filter>
                </wfs:Query>
            </wfs:GetFeature>"""

    template = Template(filter_xml)

    return template.render(
        workspace=workspace,
        data_store_name=data_store_name,
        count=count if count else 1000,
        lowerCornerLon=float(coords[0]),
        lowerCornerLat=float(coords[1]),
        upperCornerLon=float(coords[2]),
        upperCornerLat=float(coords[3]),
        property_id=property_id,
        begin_date=begin_date,
        end_date=end_date,
        threshold_value=threshold_value
    )
