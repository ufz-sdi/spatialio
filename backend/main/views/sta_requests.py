import requests

from django.http import HttpResponse  # noqa
from django.shortcuts import get_object_or_404  # noqa

from main.models import StaEndpoint, StaLayer, StaThingProperty, StaObservedPropertyFilter  # noqa


def proxy_sta_locations(request, sta_layer_id: int):

    url = "v1.1/Things?"

    filter_params = _get_datastream_filter_params(request)

    sta_layer = get_object_or_404(StaLayer, id=sta_layer_id)
    endpoint = sta_layer.endpoint
    for thing_filter in _get_filter_for_things(endpoint, ''):
        filter_params.append(thing_filter)

    url = url + "$expand=Locations($select=location)"

    obs_property_id = request.GET.get('property')
    if obs_property_id:
        # continue "expand"
        url = url + ",Datastreams($filter=ObservedProperty/id eq {};$top=1;$orderBy=resultTime desc;$expand=Observations($select=result;$top=1;$orderBy=resultTime desc))".format(obs_property_id)
    else:
        op_filters = _get_filter_for_observed_properties(sta_layer, 'Datastreams/ObservedProperty/')
        for op_filter in op_filters:
            filter_params.append(op_filter)

    max_results = request.GET.get('count')
    if max_results:
        url = url + "&$count=true&$select=id&$top=" + str(max_results)
    else:
        url = url + "&$count=true&$select=id&$resultFormat=GeoJSON&$orderBy=Locations/location/coordinates/0,Locations/location/coordinates/1"

    # try to make request with selecting locations by Bounding-Box
    advanced_filter_params = filter_params + _get_bbox_filter_params(request)
    advanced_url = url + "&$filter=" + " and ".join(advanced_filter_params)

    advanced_url = endpoint.base_url + advanced_url
    response = requests.get(advanced_url, auth=(endpoint.username, endpoint.password), timeout=(5, 60))

    if response.status_code == 500:
        # if Bounding-Box is not supported by STA-Endpoint use only the other filters
        standard_url = url
        if len(filter_params) > 0:
          standard_url = url + "&$filter=" + " and ".join(filter_params)
        return _request_sta(standard_url, endpoint)

    return HttpResponse(response, content_type='application/json')


def proxy_sta_obs_properties(request, sta_layer_id: int):
    url = "v1.1/ObservedProperties?$select=id,name,properties&$orderBy=name"

    sta_layer = get_object_or_404(StaLayer, id=sta_layer_id)

    filter_params = []

    op_filters = _get_filter_for_observed_properties(sta_layer, 'Datastreams/ObservedProperty/')
    for op_filter in op_filters:
        filter_params.append(op_filter)

    endpoint = sta_layer.endpoint

    for thing_filter in _get_filter_for_things(endpoint, 'Datastreams/Thing/'):
        filter_params.append(thing_filter)

    if len(filter_params) > 0:
        url = url + "&$filter=" + " and ".join(filter_params)

    return _request_sta(url, endpoint)


def proxy_sta_thing(request, sta_endpoint_id: int, thing_id: int):
    url = 'v1.1/Things?$filter=id eq {}'.format(thing_id)

    endpoint = get_object_or_404(StaEndpoint, id=sta_endpoint_id)

    return _request_sta(url, endpoint)


def proxy_sta_obs_properties_by_thing(request, sta_layer_id: int, thing_id: int):
    url = 'v1.1/ObservedProperties?$orderBy=name&$filter=Datastreams/Thing/id eq {}'.format(thing_id)

    sta_layer = get_object_or_404(StaLayer, id=sta_layer_id)

    op_filters = _get_filter_for_observed_properties(sta_layer, '')
    for op_filter in op_filters:
        url += ' and ' + op_filter

    return _request_sta(url, sta_layer.endpoint)


def proxy_sta_datastream_properties(request, sta_endpoint_id: int, thing_id: int, obs_property_id: int):
    url = 'v1.1/Datastreams?$filter=ObservedProperty/id eq {} and Thing/id eq {}&$select=unitOfMeasurement/symbol&$expand=ObservedProperty'.format(obs_property_id, thing_id)

    endpoint = get_object_or_404(StaEndpoint, id=sta_endpoint_id)

    return _request_sta(url, endpoint)


def proxy_sta_all_datastreams(request, sta_endpoint_id: int, thing_id: int, obs_property_id: int):
    url = 'v1.1/Observations?$filter=Datastream/ObservedProperty/id eq {} and Datastream/Thing/id eq {}&$select=result,resultTime&$orderby=resultTime asc'.format(obs_property_id, thing_id)

    endpoint = get_object_or_404(StaEndpoint, id=sta_endpoint_id)

    return _request_sta(url, endpoint)


def _request_sta(url, endpoint):
    url = endpoint.base_url + url
    response = requests.get(url, auth=(endpoint.username, endpoint.password), timeout=(5, 60))

    return HttpResponse(response, content_type='application/json')


def _get_filter_for_things(endpoint: int, sta_path: str):
    filters = []
    for thing_filter in StaThingProperty.objects.filter(endpoint=endpoint, apply_as_filter=True):
        filters.append(
            "substringof('{}',{}properties/{})".format(thing_filter.property_value, sta_path, thing_filter.property_key)
        )
    return filters


def _get_filter_for_observed_properties(layer, sta_path: str):
    filters = []
    for op_filter in StaObservedPropertyFilter.objects.filter(layer=layer):
        filters.append(
            "substringof('{}',{}{})".format(op_filter.property_value, sta_path, op_filter.property_key)
        )
    return filters


def _get_datastream_filter_params(request):

    filter_params = []

    obs_property_id = request.GET.get('property')
    if obs_property_id:
        param = "Datastreams/ObservedProperty/id eq {}".format(obs_property_id)
        filter_params.append(param)

    threshold = request.GET.get('threshold')
    if threshold:
        param = "Datastreams/Observations/result gt {}".format(threshold)
        filter_params.append(param)

    begin_date = request.GET.get('beginDate')
    if begin_date:
        param = "Datastreams/Observations/resultTime gt {}T00:00:00%2B01:00".format(begin_date)
        filter_params.append(param)

    end_date = request.GET.get('endDate')
    if end_date:
        param = "Datastreams/Observations/resultTime lt {}T00:00:00%2B01:00".format(end_date)
        filter_params.append(param)

    return filter_params


def _get_bbox_filter_params(request):

    filter_params = []

    # bottomLeftLon, bottomLeftLat, topRightLon, topRightLat
    bbox_params = request.GET.get('bbox')

    if bbox_params:
        bbox_params = bbox_params.split(',')

        bbox_top_lon = bbox_params[0]
        bbox_top_lat = bbox_params[3]
        bbox_bottom_lon = bbox_params[2]
        bbox_bottom_lat = bbox_params[1]

        if bbox_top_lon and bbox_top_lat and bbox_bottom_lon and bbox_bottom_lat:

            # define polygon in WKT-representation
            b_r_corner = "{} {}".format(bbox_bottom_lon, bbox_bottom_lat)
            t_r_corner = "{} {}".format(bbox_bottom_lon, bbox_top_lat)
            t_l_corner = "{} {}".format(bbox_top_lon, bbox_top_lat)
            b_l_corner = "{} {}".format(bbox_top_lon, bbox_bottom_lat)

            # define counter-clock-wise with coords as lon-lat
            polygon = "{}, {}, {}, {}, {}".format(b_r_corner, t_r_corner, t_l_corner, b_l_corner, b_r_corner)

            filter_params.append("st_within(Locations/location, geography'POLYGON (({}))')".format(polygon))

    return filter_params