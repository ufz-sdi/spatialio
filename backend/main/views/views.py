import os

from django.http import HttpResponse, HttpResponseNotFound, JsonResponse    # noqa
from django.shortcuts import get_object_or_404    # noqa
from django.forms.models import model_to_dict    # noqa
from django.db import reset_queries    # noqa

from main.models import *    # noqa
from main.lib.utils.backend import create_legend_map, get_geoserver_url_for_frontend # noqa


def set_text_by_locale(record, locale, key=None):
    if key:
        record[key] = record[key + '_en'] if locale == "en" else record[key + '_de']

        for prop in [key + '_de', key + '_en']:
            del record[prop]
    else:
        record['name'] = record['name_en'] if locale == "en" else record['name_de']

        for prop in ['name_de', 'name_en']:
            del record[prop]

    return record


def get_projects(request, locale):
    projects = Project.objects.filter(show_in_gallery=True).order_by('position_in_gallery')

    result = {'message': '', 'success': True, 'result': {}}
    project_overview = []

    for project in projects:
        project_overview.append(_get_project_information(project, locale))

    result['result']['projects'] = project_overview
    return JsonResponse(result)


def get_project_information(request, locale, project_name):
    project = get_object_or_404(Project, name_in_backend=project_name)

    result = {'message': '', 'success': True, 'result': {}}

    record = _get_project_information(project, locale)

    result['result']['project_info'] = record

    reset_queries()

    return JsonResponse(result)


def get_project_area(request, locale: str, area_id: int):

    result = {'message': '', 'success': True, 'result': {}}
    result['result']['area'] = {}

    area = get_object_or_404(Area, id=area_id)

    area_record = model_to_dict(area)
    area_record['layers'] = []

    area_record = set_text_by_locale(area_record, locale)
    area_record = set_text_by_locale(area_record, locale, 'info')

    for area_layer in (AreaLayer.objects
            .filter(area=area.id, is_active=True)
            .select_related('wms_layer__variable')
            .order_by('position', 'id')):

        layer_record = {}

        if area_layer.wms_layer:
            layer_record = get_wms_layer(area_layer, locale)

        if area_layer.sta_layer:
            layer_record = get_sta_layer(area_layer, locale)

        if area_layer.geojson_layer:
            layer_record = get_geojson_layer(area_layer, locale)

        if area_layer.wfs_layer:
            layer_record = get_wfs_layer(area_layer, locale)

        layer_record['is_default_on_start'] = area_layer.is_default_on_start
        area_record['layers'].append(layer_record)

    area_record['num_listFilters'] = ListFilter.objects.filter(area=area_id, is_active=True).count()
    area_record['listFilters'] = []

    result['result']['area'] = area_record

    reset_queries()

    return JsonResponse(result)


def get_project_area_names(request, locale, project_name):
    result = {'message': '', 'success': True, 'result': {}}
    result['result']['areas'] = []

    areas = Area.objects.filter(project__name_in_backend=project_name, is_active=True).order_by('position')

    if len(areas) == 0:
        return HttpResponseNotFound("Project not found.")

    for area in areas:
        area_record = {
            'id': area.id,
            'name': area.name_en if locale == "en" else area.name_de
        }
        result['result']['areas'].append(area_record)

    return JsonResponse(result)



def get_wms_layer(area_layer: AreaLayer, locale: str):
    wms_layer = area_layer.wms_layer

    layer_record = model_to_dict(wms_layer)
    for prop in ['variable', 'file_path', 'bucket']:
        del layer_record[prop]

    layer_record = set_text_by_locale(layer_record, locale)
    layer_record = set_text_by_locale(layer_record, locale, 'info')
    layer_record = set_text_by_locale(layer_record, locale, 'unit')

    for prop in ['styles', 'available_aggregations', 'available_methods']:
        layer_record[prop] = []

    layer_record['legends_to_map'] = {}

    for layer_legend in WmsLayerLegend.objects.filter(wms_layer=wms_layer.id).select_related('legend').order_by('position'):

        legend_record = model_to_dict(layer_legend.legend)
        legend_record = set_text_by_locale(legend_record, locale)

        legend_name = legend_record['name']

        layer_record['styles'].append(legend_name)
        if layer_legend.replace_values_in_viewer_by_label:
            layer_record['legends_to_map'][legend_name] = create_legend_map(layer_legend.legend, locale)

    for layer_process in WmsLayerProcess.objects.filter(wms_layer=wms_layer.id).select_related('process'):
        if layer_process.process.type == AGGREGATION:
            layer_record['available_aggregations'].append(layer_process.process.sub_type)
        if layer_process.process.type == ANOMALY or layer_process.process.type == MAGNITUDE:
            layer_record['available_methods'].append(layer_process.process.type)

    if len(layer_record['available_aggregations']) > 0:
        layer_record['available_methods'].append(AGGREGATION)

    layer_record['id'] = area_layer.id
    layer_record['own_id'] = wms_layer.id
    layer_record['type'] = 'wms'

    return layer_record


def get_sta_layer(area_layer: AreaLayer, locale: str):
    sta_layer = area_layer.sta_layer

    layer_record = model_to_dict(sta_layer)

    layer_record = set_text_by_locale(layer_record, locale)
    layer_record = set_text_by_locale(layer_record, locale, 'info')

    layer_record['id'] = area_layer.id
    layer_record['own_id'] = sta_layer.id
    layer_record['type'] = 'sta'

    layer_record['legend'] = []
    layer_legend = sta_layer.legend
    if layer_legend:
        for color in StaColor.objects.filter(legend=layer_legend).order_by('-position'):
            color_record = model_to_dict(color)
            del color_record['id']
            del color_record['legend']
            layer_record['legend'].append(color_record)

    layer_record['popupInfoConfig'] = {}

    for thing_property in StaThingProperty.objects.filter(endpoint=sta_layer.endpoint, show_prop_in_frontend=True):
        property_record = model_to_dict(thing_property)
        for prop in ['id', 'endpoint', 'property_key', 'apply_as_filter', 'show_prop_in_frontend']:
            del property_record[prop]
        layer_record['popupInfoConfig'][thing_property.property_key] = property_record

    return layer_record


def get_geojson_layer(area_layer: AreaLayer, locale: str):
    geojson_layer = area_layer.geojson_layer

    layer_record = model_to_dict(geojson_layer)

    layer_record = set_text_by_locale(layer_record, locale)
    layer_record = set_text_by_locale(layer_record, locale, 'info')

    # base_url = os.environ.get('MINIO_ENDPOINT')
    # if base_url.endswith('/'):
    #     base_url = base_url.rstrip('/')
    # if base_url == 'http://minio:9000':
    #     base_url = 'http://localhost:9000'
    #
    # file_path = geojson_layer.file_path
    # if file_path.startswith('/'):
    #     file_path = file_path.lstrip('/')
    #
    # layer_record['url'] = '{}/{}/{}'.format(base_url, geojson_layer.bucket.name, file_path)

    layer_record['id'] = area_layer.id
    layer_record['type'] = 'geojson'

    return layer_record


def get_wfs_layer(area_layer: AreaLayer, locale: str):
    wfs_layer = area_layer.wfs_layer

    layer_record = model_to_dict(wfs_layer)

    layer_record = set_text_by_locale(layer_record, locale)
    layer_record = set_text_by_locale(layer_record, locale, 'info')

    layer_record['id'] = area_layer.id
    layer_record['own_id'] = wfs_layer.id
    layer_record['type'] = 'wfs'
    layer_record['url'] = get_geoserver_url_for_frontend(wfs_layer.url)

    return layer_record


def get_filters(request, locale: str, area_id: int):

    result = {'message': '', 'success': True, 'list_filters': []}

    name_prop = 'name_de' if locale == 'de' else 'name_en'

    for list_filter in ListFilter.objects.filter(area=area_id, is_active=True).order_by('position'):
        filter = model_to_dict(list_filter)
        filter['items'] = []

        filter = set_text_by_locale(filter, locale)

        for list_filter_item in ListFilterItem.objects.filter(list_filter=list_filter).order_by(name_prop):
            item = model_to_dict(list_filter_item)
            item['layers'] = []

            item = set_text_by_locale(item, locale)

            for assigned_layer in LayerListFilterItem.objects.filter(filter_item=list_filter_item):
                item['layers'].append(assigned_layer.layer.id)

            filter['items'].append(item)

        result['list_filters'].append(filter)

    return JsonResponse(result)


def _get_project_information(project, locale):
    record = model_to_dict(project)
    record = set_text_by_locale(record, locale)
    record = set_text_by_locale(record, locale, 'teaser')
    record = set_text_by_locale(record, locale, 'about_text')
    record = set_text_by_locale(record, locale, 'imprint_text')
    record = set_text_by_locale(record, locale, 'faq_text')
    record = set_text_by_locale(record, locale, 'maintenance_notification')
    record = set_text_by_locale(record, locale, 'print_annotation')
    return record
