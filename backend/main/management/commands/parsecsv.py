import os
import time

from django.utils import timezone   # noqa
from django.core.management.base import BaseCommand # noqa
from django.db.models import Q  # noqa

from data_import.models import CsvImportJob, PointData, CsvIncludeCriteria # noqa
from data_import.models import WFS, STA # noqa

from main.lib.utils.s3_utils import download_minio_file # noqa

from data_import.lib.StaImporter import StaImporter    # noqa
from data_import.lib.PostgisImporter import PostgisImporter # noqa


SIZE_LIMIT = 30000000   # 30 MB
ROW_LIMIT = 50000

class Command(BaseCommand):

    def handle(self, *args, **options):

        job = CsvImportJob.objects.filter(Q(is_processed=False) & Q(is_running=False) & (Q(bucket__connect_to_sta=True) | Q(bucket__connect_to_wfs=True))).select_related('bucket__sta_endpoint', 'bucket__csv_parser').order_by('created_at').first()

        if job:
            start_time = time.time()

            job.started_at = timezone.now()
            job.is_running = True
            job.save()

            file_name = job.s3_file.split('/')[-1]

            download_minio_file(job.bucket.name, job.s3_file, file_name)

            set_file_stats_and_validate(job, file_name)
            if job.validation_error:
                job.is_processed = True
                job.save()
                return

            if job.target == STA:
                i = StaImporter(job)
                i.import_csv_in_sta()

                if not PointData.objects.filter(import_job=job).exists():
                    job.is_success = True

            if job.target == WFS:
                i = PostgisImporter(job)
                i.import_csv_to_wfs()
                i.write_data()
                job.is_success = not i.error
                if i.error is True:
                    job.validation_error = "\n".join(i.logs)

            job.is_processed = True
            job.is_running = False
            job.execution_time = str(round((time.time() - start_time), 2)) + "s"
            job.finished_at = timezone.now()
            #job.data_points_created = rows_succeeded
            #job.data_points_failed = rows_failed
            job.save()

            os.remove(file_name)


def set_file_stats_and_validate(job: CsvImportJob, file_name: str):
    job.file_size = os.path.getsize(file_name)
    rows = os.popen('wc -l < {}'.format(file_name)).read()
    job.num_rows = int(rows[:-1])

    if job.file_size > SIZE_LIMIT:
        job.validation_error = 'file exceeds maximum file-size: {}B'.format(SIZE_LIMIT)

    if job.num_rows > ROW_LIMIT:
        job.validation_error = 'file exceeds maximum number of rows: {}'.format(ROW_LIMIT)
