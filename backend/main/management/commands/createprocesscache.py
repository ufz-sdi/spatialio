import datetime
import os
from django.core.management.base import BaseCommand
from main.models import WmsLayer, WmsLayerProcess, ProcessCache
from main.lib.api.AggregationApi import AggregationApi

bl_ids = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16']


def _check_date():
    current_time = datetime.datetime.now().time()
    start_time = datetime.time(int(os.environ.get("CACHE_START_DATE")))
    end_time = datetime.time(int(os.environ.get("CACHE_END_DATE")))
    return start_time < current_time or current_time < end_time


def _update_cache(process, layer):
    try:
        for entry in ProcessCache.objects.filter(wms_layer=layer, process=process):
            if entry.results is None or entry.time_steps != layer.time_steps:
                print('Found outdated cache entry [layer: {}, process: {}, polygon id: {}] '
                      .format(layer.id, process.id, entry.polygon_id))
                print('Update entry...')
                _update_cache_entry(entry, layer, process, entry.polygon_id)
    except ProcessCache.DoesNotExist:
        print('No cache entry found for [layer: {}, process: {}].'
              .format(layer.id, process.id))
        print('Create new entries...')
    for bl_id in bl_ids:
        try:
            ProcessCache.objects.get(wms_layer=layer, process=process, polygon_id=bl_id)
        except ProcessCache.DoesNotExist:
            print('Create new entry for [layer: {}, process: {}, polygon id: {}]. '
                  .format(layer.id, process.id, bl_id))
            _add_cache_entry(layer, process, bl_id)


def _update_cache_entry(cache_entry, layer, process, polygon_id):
    try:
        results = _call_process(layer, process, polygon_id)
        cache_entry.time_steps = layer.time_steps
        cache_entry.results = results
        cache_entry.save()
        print('Updated cache entry for layer {} | Bundesland {} '.format(layer.id, polygon_id))
    except Exception as e:
        print('Could not update cache entry for layer {} | Bundesland {} due to error: {}'.format(layer.id, polygon_id, str(e)))


def _add_cache_entry(layer, process, polygon_id):
    try:
        results = _call_process(layer, process, polygon_id)
        new_cache_entry = ProcessCache(wms_layer=layer,
                                       process=process,
                                       polygon_id=polygon_id,
                                       time_steps=layer.time_steps,
                                       results=results)
        new_cache_entry.save()
        print('Created new cache entry for layer {} | Bundesland {}'.format(layer.id, polygon_id))
    except Exception as e:
        print('Could not create new cache entry for layer {} | Bundesland {} due to error: {}'.format(layer.id, polygon_id, str(e)))


def _call_process(layer, process, polygon_id):
    process_api = AggregationApi(os.environ.get("AGGREGATION_API_URL"))
    results = process_api.call_process(layer, process, ['bl_id', polygon_id], layer.time_steps)
    return results


class Command(BaseCommand):

    def handle(self, *args, **options):
        while True:
            while _check_date():
                for layer_process in WmsLayerProcess.objects.filter(in_cache=True):
                    _update_cache(layer_process.process, layer_process.layer)
