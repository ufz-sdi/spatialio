import os
from time import sleep

import requests

from django.core.management.base import BaseCommand
from django.core import management

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, Permission, User
from django.db.models import Q

from main.models import Bucket, S3User, WfsLayer, WmsLayer, WmsLayerLegend, WmsLegend

from main.lib.utils.s3_utils import create_bucket, create_public_folder_in_bucket, get_minio_client, upload_file, give_write_access_to_bucket
from main.lib.legends.legend import create_sld_file
from main.lib.api.GeoserverApi import GeoServerApi
from metadata.api import GeonetworkApi

BUCKET_NAME = "test-bucket"
BUCKET_NAME2 = "alternate-bucket"

mc = get_minio_client(True)
user = get_user_model()


class Command(BaseCommand):

    def handle(self, *args, **options):

        message = """
--------------------------------
  Starting spatial.IO platform
--------------------------------
        """
        print(message)

        print("\nStep 1: Setup Minio-Buckets: " + BUCKET_NAME)

        if not mc.bucket_exists(BUCKET_NAME):
            create_bucket(BUCKET_NAME)
            create_public_folder_in_bucket(BUCKET_NAME)

        if not mc.bucket_exists(BUCKET_NAME2):
            create_bucket(BUCKET_NAME2)

        print("\nStep 2: Create Projects")

        test_group = create_group('test-group')

        create_admin_user('admin')
        user1 = create_normal_user('user1', test_group)
        user2 = create_normal_user('user2', test_group)

        management.call_command("loaddata", "test_db_content")

        connect_user_to_s3(user1, BUCKET_NAME, 'miniouser1')
        connect_user_to_s3(user2, BUCKET_NAME, 'miniouser2')

        print("\nStep 3: Create GeoNetwork-group")
        geonetwork_api = GeonetworkApi()

        bucket1 = Bucket.objects.get(name=BUCKET_NAME)
        bucket1.geonetwork_group = geonetwork_api.create_group("test-group")
        bucket1.save()

        bucket2 = Bucket.objects.get(name=BUCKET_NAME2)
        bucket2.geonetwork_group = geonetwork_api.create_group("test-group2")
        bucket2.save()


        print("\nStep 4: Configure GeoServer")

        geoserver_api = GeoServerApi()
        geoserver_api.add_allowed_url("Minio", "http://minio:9000")

        print("\nStep 5: Import Netcdf-files")

        import_file_to_minio(BUCKET_NAME, "https://minio.ufz.de/rdm/sdi-test-data/drought.nc", "drought.nc", "drought")
        import_file_to_minio(BUCKET_NAME, "https://minio.ufz.de/rdm/sdi-test-data/precipitation.nc", "precipitation.nc", "pre")

        # load reference-file for pygeoapi process "precipitation-anomaly"
        import_file_to_minio(BUCKET_NAME, "https://minio.ufz.de/rdm/sdi-test-data/pre_1950_2021_monsum.nc", "pre_1950_2021_monsum.nc", "pre_1950_2021_monsum")


        print("\nStep 6: Configure Layer in GeoServer")

        for legend in WmsLegend.objects.all():
            sld = create_sld_file(legend, "de")
            geoserver_api.create_style(legend.name_de, sld)

            sld = create_sld_file(legend, "en")
            geoserver_api.create_style(legend.name_en, sld)

        for layer in WmsLayer.objects.filter(bucket__connect_to_geoserver=True):

            layer_legends = WmsLayerLegend.objects.filter(wms_layer=layer).order_by("position")

            for index, layer_legend in enumerate(layer_legends):
                style_name_de = layer_legend.legend.name_de
                style_name_en = layer_legend.legend.name_en
                if index == 0:
                    geoserver_api.add_style(style_name_de, layer.layer_name, True)
                    geoserver_api.add_style(style_name_en, layer.layer_name, True)
                else:
                    geoserver_api.add_style(style_name_de, layer.layer_name)
                    geoserver_api.add_style(style_name_en, layer.layer_name)


        print("\nStep 7: Upload GeoJson-File")
        upload_file(BUCKET_NAME, "public/bw.json", "main/geojson/districts/08_admin.geojson")

        print("\nStep 8: Upload Metadata-Json-File")
        upload_file(BUCKET_NAME, "drought/metadata.jsonld", "main/fixtures/metadata.jsonld")

        print("\nStep 9: Import STA-Data")
        import_file_to_minio(BUCKET_NAME, "https://minio.ufz.de/rdm/sdi-test-data/wis-d_input_last_14_D.csv", "wis-d_input_last_14_D.csv", "sta")

        print("\nStep 10: Import Shapefile Data")
        import_file_to_minio(BUCKET_NAME, "https://minio.ufz.de/rdm/sdi-test-data/shp/cologne_polygon_shp.zip", "cologne_polygon_shp.zip", "shp")

        print("\nStep 11: Import CSV-Data to WFS")
        is_data_imported = False
        import_file_to_minio(BUCKET_NAME2, "https://minio.ufz.de/rdm/sdi-test-data/data.csv", "data.csv", "chemicals")

        print("\nCreate WFS-Datastore...")
        while not is_data_imported:
            created_wfs_layers = WfsLayer.objects.filter(is_created=True)
            if len(created_wfs_layers) > 0:
                is_data_imported = True
                print("\nWFS-Datastore created.")
                print("\nTest-Data imported.")
            sleep(3)


def import_file_to_minio(bucket, url, file_name, sub_path):
    with open(file_name, "wb") as file:
        response = requests.get(url)
        file.write(response.content)

    upload_file(bucket, sub_path + "/" + file_name, file_name)
    os.remove(file_name)
    print("File imported: {}" .format(file_name))

def create_admin_user(name: str):
    if not user.objects.filter(username=name).exists():
        user.objects.create_superuser(name, '', name)
        print(name + ' created')
    else:
        print(name + 'already exists')

def create_normal_user(name: str, group: Group):
    if not user.objects.filter(username=name).exists():
        normal_user = user.objects.create_user(name, '', name, is_staff=True)
        group.user_set.add(normal_user)
        print(name +' created')
        return normal_user
    else:
        print(name + ' already exists')
        return user.objects.get(username=name)


def connect_user_to_s3(django_user: User, bucket_name: str, s3_username: str):
    mc.user_add(s3_username, s3_username)
    s3user = S3User.objects.get(name=s3_username)
    s3user.user = django_user
    s3user.save()
    give_write_access_to_bucket(bucket_name, s3user)

def create_group(name: str):
    try:
        test_group = Group.objects.get(name=name)
    except Group.DoesNotExist:
        test_group = Group.objects.create(name=name)

    models = [
        'area',
        'arealayer',
        'author',
        'listfilter',
        'listfilteritem',
        'layerlistfilteritem',
        'wmslayer',
        'connectedlayer',
        'wmslayerprocess',
        'wmslayerlegend',
        'wmscolor',
        'wmslegend',
        'wfslayer',
        'stalayer',
        'stalegend',
        'geojsonlayer',
        'stathingproperty',
        'staobservedpropertyfilter',
        'bucketuser',
        'metadatarecord',
        'metadatarecordauthor',
        'organization',
        'csvparser',
        'csvparserextracolumn',
        'csvincludecriteria',
        'csvimportjob',
        'pointdata',
        'extendedpointdata',
    ]

    restricted_permissions = [
        'change_bucket',
        'view_bucket',
        'view_bucketevent',
        'change_project',
        'view_project'
    ]

    permissions = Permission.objects.filter(Q(content_type__model__in=models) | Q(codename__in=restricted_permissions))
    for permission in permissions:
        test_group.permissions.add(permission)

    return test_group