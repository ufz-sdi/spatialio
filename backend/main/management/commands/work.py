import os
import time

from django.core.management.base import BaseCommand # noqa
from django.core import management # noqa
from django.db import DataError

from minio import Minio # noqa

from metadata.lib import MetadataService, JsonLDParser # noqa
from main.lib.utils import NetcdfParser # noqa
from main.lib.utils.file import File, is_wms_file, is_zip_file_for_import, create_file_identifier, get_or_create_metadata_record, get_metadata_record, search_metadata_record # noqa
from main.lib.utils.backend import NETCDF_HELPER_VARIABLES, TIME_BNDS, get_geoserver_url_for_frontend # noqa


from main.models import Bucket, BucketEvent, WmsLayer, GeojsonLayer, WfsLayer, DataVariable, WmsLayerLegend # noqa
from metadata.models import MetadataRecord # noqa
from data_import.models import CsvImportJob, WFS, STA # noqa

from main.lib.utils.s3_utils import download_minio_file, get_s3_filesystem, get_minio_client # noqa
from main.lib.api.GeoserverApi import GeoServerApi # noqa
from metadata.api import GeonetworkApi # noqa

import rasterio # noqa

fs = get_s3_filesystem()
minio_client: Minio = get_minio_client()

S3_DATA_DIR = "minio-files"


class Command(BaseCommand):

    def __init__(self):
        self.geoserver_api = GeoServerApi()
        self.has_error = False
        self.logs = []

    def handle(self, *args, **options): # noqa

        while True:
            events = BucketEvent.objects.filter(is_new__isnull=True).order_by('event_time')

            for event in events:

                bucket = self.get_bucket_if_exists(event)
                event.bucket = bucket

                if bucket:

                    file = File(bucket.name, event.get_relative_path())

                    if event.is_create_event():

                        if file.relative_path.endswith('.jsonld'):
                            self.create_metadata(bucket, file)
                        else:
                            self.update_metadata_if_exists(bucket, file)  #

                        if bucket.connect_to_geoserver:
                            if is_wms_file(bucket, file):
                                if file.is_netcdf():
                                    self.create_netcdf_datastore(bucket, file)
                                if file.is_geotiff():
                                    self.create_geotiff_datastore(bucket, file)
                            if is_zip_file_for_import(bucket, file):
                                if file.is_shapefile():
                                    self.create_shapefile_datastore(bucket, file)
                        if bucket.connect_to_thredds:
                            if file.is_netcdf():
                                self.download_to_thredds(file)
                        if file.is_csv():
                            if bucket.connect_to_sta:
                                previous_jobs = get_finished_import_jobs(bucket, file)
                                delete_sta_data(previous_jobs)
                                create_import_job(bucket, file, STA)
                            if bucket.connect_to_wfs:
                                create_import_job(bucket, file, WFS)

                    if event.is_delete_event():

                        if file.relative_path.endswith('.jsonld'):
                            self.delete_metadata(bucket, file)
                        else:
                            self.update_metadata_if_exists(bucket, file)  #

                        if bucket.connect_to_geoserver:
                            if is_wms_file(bucket, file):
                                self.delete_wms_datastore(file)
                            if is_zip_file_for_import(bucket, file):
                                self.delete_shp_wfs_datastore(bucket, file)
                        if bucket.connect_to_thredds:
                            if file.is_netcdf():
                                self.remove_from_thredds(file.absolute_path)
                        if bucket.connect_to_sta:
                            if file.is_csv():
                                previous_jobs = get_finished_import_jobs(bucket, file)
                                delete_sta_data(previous_jobs)

                self.save_event(event)

                self.geoserver_api.reset()
                self.has_error = False
                self.logs = []

            management.call_command("parsecsv")

            if is_csv_in_queue():
                # immediately start next run to parse open csv-files quickly
                continue
            else:
                if CsvImportJob.objects.filter(is_processed=True, target='WFS').count() > 0:
                    for wfs_layer in WfsLayer.objects.filter(is_created=False):
                        self.geoserver_api.create_postgis_datastore(wfs_layer)
                        wfs_layer.logs = self.geoserver_api.logs
                        if not self.geoserver_api.has_error:
                            wfs_layer.is_created = True
                        wfs_layer.save()

                # nothing else to do, wait until next run
                time.sleep(int(os.environ.get("INTERVAL_SECONDS")))

    def get_bucket_if_exists(self, event: BucketEvent):
        bucket_name = event.get_bucket_name()
        try:
            return Bucket.objects.get(name=bucket_name)
        except Bucket.DoesNotExist:
            self.logs.append('bucket is not configured in backend: ' + bucket_name)
            self.has_error = True
            return None

    def create_metadata(self, bucket: Bucket, file: File):
        record = get_or_create_metadata_record(bucket, file.relative_path)
        if record:
            JsonLDParser(fs).update_metadata_record(record, file.absolute_path)
            service = MetadataService(GeonetworkApi())
            service.submit_to_geonetwork(record)

            self.logs.extend(service.api.logs)
            self.has_error = service.has_error

    def update_metadata_if_exists(self, bucket: Bucket, file: File):
        pieces = file.relative_path.split('/')
        record = search_metadata_record(bucket, pieces)
        if record:
            service = MetadataService(GeonetworkApi())
            service.submit_to_geonetwork(record)

            self.logs.extend(service.api.logs)
            self.has_error = service.has_error

    def delete_metadata(self, bucket: Bucket, file: File):
        metadata_record = get_metadata_record(bucket, file.absolute_path)
        if metadata_record:
            service = MetadataService(GeonetworkApi())
            service.delete_record(metadata_record)
            self.logs.extend(service.api.logs)
            self.has_error = service.has_error
            metadata_record.delete()
        else:
            self.logs.extend('Could not find metadata_record to delete: ' + file.absolute_path)
            self.has_error = True

    def create_netcdf_datastore(self, bucket: Bucket, file: File):

        object_url = minio_client.presigned_get_object(bucket.name, file.relative_path)
        workspace = bucket.name
        coverage_store = create_data_store_name(file)

        self.geoserver_api.create_datastore(workspace, coverage_store, object_url, 'url.netcdf')

        for variable in get_netcdf_variables(file, bucket):

            datastore_name = coverage_store + '_' + variable.name
            layer_name = workspace + ':' + datastore_name

            layers = list(WmsLayer.objects.filter(layer_name=layer_name, variable=variable))

            if len(layers) == 0:
                if variable.name not in NETCDF_HELPER_VARIABLES:
                    new_layer = WmsLayer(
                        wms_url=get_geoserver_url_for_frontend() + '/' + workspace,
                        epsg_code=get_crs(file),
                        axis_label_x='Long',
                        axis_label_y='Lat',
                        scale_factor=1,
                        layer_name=layer_name,
                        variable=variable,
                        no_data_value=-9999,
                        file_path=file.absolute_path,
                        bucket=bucket
                    )
                    layers.append(new_layer)

            if variable.name != TIME_BNDS:
                native_name = variable.name
                self.geoserver_api.configure_layer(workspace, coverage_store, native_name)

            for layer in layers:
                layer.time_steps = self.geoserver_api.get_wms_timesteps(workspace, layer_name)
                layer.save()

                if layer.store_in_geowebcache:
                    layer_legends = WmsLayerLegend.objects.filter(wms_layer=layer)
                    for layer_legend in layer_legends:
                        self.geoserver_api.modify_gwc(layer.layer_name, layer_legend.legend.name_en, "seed")
                        self.geoserver_api.modify_gwc(layer.layer_name, layer_legend.legend.name_en, "seed")

        self.logs.extend(self.geoserver_api.logs)
        self.has_error = self.geoserver_api.has_error

    def create_geotiff_datastore(self, bucket: Bucket, file: File):

        variable = get_or_create_variable("band_data", bucket)

        object_url = minio_client.presigned_get_object(bucket.name, file.relative_path)
        workspace = bucket.name
        coverage_store = create_data_store_name(file)

        self.geoserver_api.create_datastore(workspace, coverage_store, object_url, 'url.geotiff')

        wms_name = workspace + ':' + coverage_store

        layers = list(WmsLayer.objects.filter(layer_name=wms_name, variable=variable))

        if len(layers) == 0:
            new_layer = WmsLayer(
                wms_url=get_geoserver_url_for_frontend() + '/' + workspace,
                epsg_code=get_crs(file),
                axis_label_x='E',
                axis_label_y='N',
                scale_factor=1,
                layer_name=wms_name,
                variable=variable,
                no_data_value=get_no_data_value(file.absolute_path),
                file_path=file.absolute_path,
                bucket=bucket
            )
            layers.append(new_layer)

        native_name = variable.name
        self.geoserver_api.configure_layer(workspace, coverage_store, native_name)

        for layer in layers:
            layer.save()

        self.logs.extend(self.geoserver_api.logs)
        self.has_error = self.geoserver_api.has_error

    def create_shapefile_datastore(self, bucket: Bucket, file: File):

        object_url = minio_client.presigned_get_object(bucket.name, file.relative_path)
        workspace = bucket.name
        data_store = create_wfs_data_store_name(bucket, file)
        native_name = file.get_file_name()

        if bucket.wfs_file_suffix:
            native_name = native_name.removesuffix(bucket.wfs_file_suffix)
            native_name = native_name.rstrip('_.- ')

        self.geoserver_api.create_datastore(workspace, data_store, object_url, 'url.shp', 'datastores')

        self.geoserver_api.configure_wfs_layer_name(workspace, data_store, native_name)

        layer_name = workspace + ':' + data_store

        url = '{}/{}/ows?service=WFS&version=2.0.0&request=GetFeature&typename={}&outputFormat=application/json&srsname=EPSG:3857'.format(
            get_geoserver_url_for_frontend(),
            workspace,
            layer_name
        )

        try:
            geojson_layer = GeojsonLayer.objects.get(file_path=file.relative_path)
        except GeojsonLayer.DoesNotExist:
            geojson_layer = GeojsonLayer(
                bucket=bucket,
                url=url,
                file_path=file.relative_path,
            )

        geojson_layer.save()

        self.logs.extend(self.geoserver_api.logs)
        self.has_error = self.geoserver_api.has_error


    def delete_wms_datastore(self, file: File):

        layers = WmsLayer.objects.filter(file_path=file.absolute_path).select_related('variable')

        for layer in layers:

            pieces = layer.layer_name.split(':')
            workspace = pieces[0]
            layer_name = pieces[1]
            if file.is_netcdf():
                coverage_store = layer_name.rstrip('_' + layer.variable.name)
                layer_legends = WmsLayerLegend.objects.filter(wms_layer=layer)
                for layer_legend in layer_legends:
                    self.geoserver_api.modify_gwc(layer.layer_name, layer_legend.legend.name_de, "truncate")
                    self.geoserver_api.modify_gwc(layer.layer_name, layer_legend.legend.name_en, "truncate")
            else:
                coverage_store = layer_name

            self.has_error = self.geoserver_api.has_error
            self.geoserver_api.delete_datastore(workspace, coverage_store)
            self.logs.extend(self.geoserver_api.logs)

            if not self.has_error:
                layer.file_path = None
                layer.save()


    def delete_shp_wfs_datastore(self, bucket: Bucket, file: File):

        try:
            layer = GeojsonLayer.objects.get(bucket=bucket, file_path=file.relative_path)

            data_store = create_wfs_data_store_name(bucket, file)

            self.has_error = self.geoserver_api.has_error
            self.geoserver_api.delete_datastore(bucket.name, data_store, 'datastores')
            self.logs.extend(self.geoserver_api.logs)

            if not self.has_error:
                layer.url = '-'
                layer.file_path = '-'
                layer.save()

        except GeojsonLayer.DoesNotExist:
            self.logs.extend('Could not delete WFS-datastore')


    def download_to_thredds(self, file: File):

        download_path = '/{}/thredds/{}'.format(S3_DATA_DIR, file.absolute_path)

        is_file_copied = download_minio_file(file.bucket_name, file.relative_path, download_path)

        if is_file_copied:
            self.logs.append('Downloaded to Thredds successful: ' + download_path)
        else:
            self.logs.append('Could not download file to Thredds: ' + download_path)
            self.has_error = True

    def remove_from_thredds(self, absolute_path: str):
        download_path = '/{}/thredds/{}'.format(S3_DATA_DIR, absolute_path)
        try:
            os.remove(download_path)
        except OSError:
            self.has_error = True

    def save_event(self, event: BucketEvent):

        if len(self.logs) > 0:
            final_logs = "\n".join(self.logs)
        else:
            final_logs = '-'
            print('no logs.\n')

        event.logs = final_logs
        event.is_success = not self.has_error

        if not self.geoserver_api.has_timeout:
            event.is_new = False
        try:
            event.save()
        except DataError:
            print(final_logs)
            event.logs = "Error: Logs too long!"
            event.save()


def get_netcdf_variables(file: File, bucket: Bucket):
    variables = []
    if file.is_netcdf():
        nc_variables = NetcdfParser(fs).get_nc_variables(file.absolute_path)
        if nc_variables:
            for var in nc_variables:
                data_variable = get_or_create_variable(var, bucket)
                variables.append(data_variable)
    return variables


def create_data_store_name(file: File) -> str:
    name_parts = file.relative_path.split('/')
    result = "_".join(name_parts)
    return result.replace(".", "_")

def create_wfs_data_store_name(bucket: Bucket, file: File) -> str:
    name = create_data_store_name(file)
    name = name.removesuffix('_zip')
    if bucket.wfs_file_suffix:
        name = name.removesuffix(bucket.wfs_file_suffix)
        name = name.rstrip('_.- ')
    return name

def get_crs(file: File):

    if file.is_netcdf():
        return 'EPSG:4326'
    if file.is_geotiff():
        with fs.open(file.absolute_path, 'rb') as s3_handle:
            src = rasterio.open(s3_handle)
            return src.crs
    return None


def get_no_data_value(absolute_path: str):
    no_data_value = -1
    # TODO: use file_path to geoserver-geodata-dir
    with fs.open(absolute_path, 'rb') as file:
        src = rasterio.open(file)

        if len(src.nodatavals) == 1:
            no_data_value = int(src.nodatavals[0])
        else:
            print("Error: file has invalid nodatavals: " + absolute_path)

    return no_data_value


def get_or_create_variable(variable_name: str, bucket: Bucket):
    try:
        data_variable = DataVariable.objects.get(name=variable_name, group=bucket.group)
    except DataVariable.DoesNotExist:
        print("Create new Variable")
        data_variable = DataVariable(
            name=variable_name,
            group=bucket.group
        )
        data_variable.save()

    return data_variable


def create_import_job(bucket: Bucket, file: File, target: str):
    job = CsvImportJob(
        bucket=bucket,
        s3_file=file.relative_path,
        target=target
    )
    job.save()

def get_finished_import_jobs(bucket: Bucket, file: File):
    return CsvImportJob.objects.filter(
        bucket=bucket,
        s3_file=file.relative_path,
        is_processed=True,
        is_running=False
    )

def delete_sta_data(jobs: list[CsvImportJob]):

    # TODO: cancel running data-extraction and import jobs?

    for job in jobs:
        # api.delete_data(job)
        job.is_data_deleted = True
        print('delete')

def is_csv_in_queue():
    jobs = CsvImportJob.objects.filter(is_processed=False, is_running=False)
    return len(jobs) > 0
