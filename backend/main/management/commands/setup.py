from django.core.management.base import BaseCommand # noqa
from django.core import management  # noqa

class Command(BaseCommand):

    def handle(self, *args, **options):

        management.call_command("migrate", interactive=False)

        management.call_command("loaddata", "ufz_theme")
