from django.contrib import admin
from django.template.response import TemplateResponse


class BasicAdminSite(admin.AdminSite):
    def about(self, request):
        return TemplateResponse(request, "admin/about.html", {})


basic_site = BasicAdminSite()
basic_site.enable_nav_sidebar = False