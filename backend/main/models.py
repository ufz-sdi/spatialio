from django.db import models    # noqa
from django.contrib.postgres.fields import ArrayField    # noqa
from django.contrib.auth.models import Group, User    # noqa
from colorfield.fields import ColorField    # noqa
import re
from urllib.parse import unquote

from data_import.models import CsvParser    # noqa

from django.core.validators import MinValueValidator, MaxValueValidator    # noqa
from django.core.exceptions import ValidationError    # noqa

TIME_FORMATS = (
    ('month', 'month'),
    ('period-19', 'period-19-years (from start-year)'),
    ('period-29', 'period-29-years (from center-year)'),
    ('period-49', 'period-49-years (from center-year)'),
    ('season', 'season'),
    ('year', 'year'),
)

SIZE_UNITS = (
    ('ki', 'KB'),
    ('mi', 'MB'),
    ('gi', 'GB'),
    ('ti', 'TB'),
)

BASE_MAPS = (
    ('osm', 'OpenStreetMap'),
    ('baseMap', 'BaseMap'),
)

LEGEND_TYPES = (
    ('discrete', 'discrete'),
    ('gradient', 'gradient'),
)

AGGREGATION = 'aggregation'
PROCESS = 'process'
MAGNITUDE = 'magnitude'
ANOMALY = 'anomaly'

COMPUTATION_TYPES = (
    (AGGREGATION, AGGREGATION),
    (PROCESS, PROCESS),
    (MAGNITUDE, MAGNITUDE),
    (ANOMALY, ANOMALY)
)

REQUEST_TYPE_1 = 'timeseries'
REQUEST_TYPE_2 = 'single value'

REQUEST_TYPES = (
    (REQUEST_TYPE_1, REQUEST_TYPE_1),
    (REQUEST_TYPE_2, REQUEST_TYPE_2),
)


def validate_bucket_name(value):
    if len(value) < 3 or len(value) > 63:
        raise ValidationError("Length must be between 3 and 63 characters.")
    if not re.match(r'^[a-z-]*$', value):
        raise ValidationError('Only lowercase-letters and hyphens are allowed.')
    if "--" in value:
        raise ValidationError("Invalid combination of characters: '--'")
    if value.startswith('xn--'):
        raise ValidationError("Invalid prefix: 'xn--'")
    if value.endswith('-s3alias'):
        raise ValidationError("Invalid suffix: '-s3alias'")
    if value.startswith('-'):
        raise ValidationError("Invalid first character: '-'")
    if value.endswith('-'):
        raise ValidationError("Invalid last character: '-'")


def validate_wfs_file_path(value):
    if len(value) < 5 or len(value) > 55:
        raise ValidationError("Length must be between 5 and 50 characters.")
    if not re.match(r'^[a-z.-/*]*$', value):
        raise ValidationError('Only lowercase-letters, slashes, asterisks and hyphens are allowed.')
    for comb in ["//", "..", "**", ".*", "/.", "./", "-/", "/-", "--", "-.", ".-"]:
        if comb in value:
            raise ValidationError("Invalid file_path: {}".format(comb))
    if not value.endswith('*.csv'):
        raise ValidationError("Filepath has to end with: '*.csv'")
    if value.startswith('-') or value.startswith('.'):
        raise ValidationError("Invalid first character.")


# abstract class giving german or english version of text
class CommonInfo(models.Model):
    name_de = models.CharField('Name (de)', max_length=1000, null=True)
    name_en = models.CharField('Name (en)', max_length=1000, null=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name_de


class StaEndpoint(models.Model):
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    base_url = models.CharField(max_length=4000)
    username = models.CharField(max_length=4000, blank=True, null=True)
    password = models.CharField(max_length=4000, blank=True, null=True)

    def __str__(self):
        result = self.base_url
        for thing_filter in StaThingProperty.objects.filter(endpoint=self, apply_as_filter=True):
            result += ' - ' + thing_filter.property_value
        return result


class Bucket(models.Model):
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, unique=True, validators=[validate_bucket_name])
    geonetwork_group = models.IntegerField(blank=True, null=True)
    connect_to_geoserver = models.BooleanField(default=False)
    connect_to_thredds = models.BooleanField(default=False)
    connect_to_sta = models.BooleanField(default=False)
    connect_to_wfs = models.BooleanField(default=False)
    sta_endpoint = models.ForeignKey(StaEndpoint, on_delete=models.CASCADE, blank=True, null=True)
    csv_parser = models.ForeignKey(CsvParser, on_delete=models.CASCADE, blank=True, null=True)
    public_folder = models.BooleanField(default=False)
    quota = models.IntegerField('Quota', default=10, validators=[MinValueValidator(1)])
    size_unit = models.CharField('Unit', max_length=20, choices=SIZE_UNITS, default='g')
    wms_file_suffix = models.CharField(max_length=100, blank=True, null=True)
    wfs_file_suffix = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.name


class S3User(models.Model):
    name = models.CharField(max_length=100, unique=True, validators=[validate_bucket_name])
    user = models.OneToOneField(User, on_delete=models.CASCADE, blank=True, null=True)
    ldap_dn = models.CharField(max_length=100, blank=True, null=True, unique=True)

    def __str__(self):
        return self.name


class BucketUser(models.Model):
    s3user = models.ForeignKey(S3User, on_delete=models.CASCADE)
    bucket = models.ForeignKey(Bucket, on_delete=models.CASCADE)
    has_write_permission = models.BooleanField(default=True)
    is_bucket_admin = models.BooleanField(default=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['s3user', 'bucket'], name='unique_bucket_user')
        ]

    def __str__(self):
        return str(self.bucket) + "-" + str(self.s3user)


class Project(CommonInfo):
    bucket = models.ForeignKey(Bucket, on_delete=models.CASCADE)
    name_in_url = models.CharField(max_length=100, unique=True)
    name_in_backend = models.CharField(max_length=100, unique=True)
    teaser_de = models.TextField(blank=True, null=True)
    teaser_en = models.TextField(blank=True, null=True)
    about_text_de = models.TextField(blank=True, null=True)
    about_text_en = models.TextField(blank=True, null=True)
    imprint_text_de = models.TextField(blank=True, null=True)
    imprint_text_en = models.TextField(blank=True, null=True)
    faq_text_de = models.TextField(blank=True, null=True)
    faq_text_en = models.TextField(blank=True, null=True)
    theme = models.CharField(max_length=100, blank=True, null=True)
    default_lon = models.DecimalField('Longitude', max_digits=12, decimal_places=10, blank=True, null=True)
    default_lat = models.DecimalField('Latitude', max_digits=12, decimal_places=10, blank=True, null=True)
    default_zoom = models.DecimalField(max_digits=10, decimal_places=8, blank=True, null=True)
    maintenance_notification_de = models.CharField(max_length=4000, blank=True, null=True)  # hidden for user
    maintenance_notification_en = models.CharField(max_length=4000, blank=True, null=True)  # hidden for user
    is_english_version_active = models.BooleanField(default=False)
    show_in_gallery = models.BooleanField(default=False)
    position_in_gallery = models.IntegerField(blank=True, null=True)
    gallery_image = models.CharField(max_length=4000, blank=True, null=True)
    enable_print_function = models.BooleanField(default=False)
    viewer = models.CharField(max_length=4000, blank=True, null=True)
    print_annotation_de = models.CharField(max_length=120, blank=True, null=True)
    print_annotation_en = models.CharField(max_length=120, blank=True, null=True)
    base_map = models.CharField(max_length=20, choices=BASE_MAPS, default='osm')


class Area(CommonInfo):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    position = models.IntegerField(default=1)
    is_active = models.BooleanField(default=True)
    info_de = models.TextField(blank=True, null=True)
    info_en = models.TextField(blank=True, null=True)
    is_default_on_start = models.BooleanField(default=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['project', 'position'], name='unique_area_pos')
        ]
        verbose_name = "Layer-Group"
        verbose_name_plural = "Layer-Groups"

    def __str__(self):
        return self.name_en


class WmsLegend(models.Model):
    name_de = models.CharField('Name (de)', max_length=50, unique=True)   # has to be unique for request to GeoServer
    name_en = models.CharField('Name (en)', max_length=50, unique=True)   # has to be unique for request to GeoServer
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    type = models.CharField(max_length=20, choices=LEGEND_TYPES, default='discrete')

    def __str__(self):
        return self.name_de

    class Meta:
        verbose_name = "WMS-Map-Legend"
        verbose_name_plural = "WMS-Map-Legends"


class WmsColor(CommonInfo):
    legend = models.ForeignKey(WmsLegend, on_delete=models.CASCADE)
    color = ColorField(format="hex")
    position = models.IntegerField(default=1)
    upper_limit = models.DecimalField(max_digits=12, decimal_places=6)
    is_invisible = models.BooleanField(default=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['legend', 'position'], name='unique_color_pos'),
            models.UniqueConstraint(fields=['legend', 'upper_limit'], name='unique_color_limit')
        ]


class StaLegend(models.Model):
    name = models.CharField(max_length=1000)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "STA-Map-Legend"
        verbose_name_plural = "STA-Map-Legends"


class StaColor(models.Model):
    legend = models.ForeignKey(StaLegend, on_delete=models.CASCADE)
    color = ColorField(format="hex")
    position = models.IntegerField(default=1)
    upper_limit = models.DecimalField(max_digits=12, decimal_places=6)
    radius = models.IntegerField(default=7)

    def __str__(self):
        return "Color {} ({} {})".format(self.position, self.color, self.upper_limit)


class DataVariable(models.Model):
    name = models.CharField(max_length=1000)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['name', 'group'], name='unique_group_variable'),
        ]

    def __str__(self):
        return self.name


class Layer(CommonInfo):
    info_de = models.TextField(blank=True, null=True)
    info_en = models.TextField(blank=True, null=True)
    time_format = models.CharField(max_length=20, choices=TIME_FORMATS, blank=True, null=True)
    y_axis_min = models.DecimalField(max_digits=12, decimal_places=5, blank=True, null=True)
    y_axis_max = models.DecimalField(max_digits=12, decimal_places=5, blank=True, null=True)

    class Meta:
        abstract = True


class VectorLayer(Layer):
    cluster_minimum_size = models.IntegerField(default=30)
    cluster_scale_factor = models.DecimalField(max_digits=12, decimal_places=6, default=0.02)

    class Meta:
        abstract = True


class StaThingProperty(models.Model):
    endpoint = models.ForeignKey(StaEndpoint, on_delete=models.CASCADE)
    property_key = models.CharField(max_length=1000)
    property_value = models.CharField(max_length=1000)
    apply_as_filter = models.BooleanField(default=False)
    show_prop_in_frontend = models.BooleanField(default=False)
    name_in_frontend_de = models.CharField(max_length=100, blank=True, null=True)
    name_in_frontend_en = models.CharField(max_length=100, blank=True, null=True)


class StaLayer(VectorLayer):
    endpoint = models.ForeignKey(StaEndpoint, on_delete=models.CASCADE)
    legend = models.ForeignKey(StaLegend, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "STA-Data-Layer"
        verbose_name_plural = "STA-Data-Layers"


class StaObservedPropertyFilter(models.Model):
    layer = models.ForeignKey(StaLayer, on_delete=models.CASCADE)
    property_key = models.CharField(max_length=1000)
    property_value = models.CharField(max_length=1000)


class WmsLayer(Layer):
    bucket = models.ForeignKey(Bucket, on_delete=models.CASCADE, null=True)
    unit_de = models.CharField(max_length=4000, blank=True, null=True)
    unit_en = models.CharField(max_length=4000, blank=True, null=True)
    epsg_code = models.CharField(max_length=4000, blank=True, null=True)
    no_data_value = models.IntegerField(default=-9999)
    axis_label_x = models.CharField(max_length=4000, blank=True, null=True)  # is readonly
    axis_label_y = models.CharField(max_length=4000, blank=True, null=True)  # is readonly
    variable = models.ForeignKey(DataVariable, on_delete=models.CASCADE, null=True)
    scale_factor = models.IntegerField(default=1)
    wms_url = models.URLField(max_length=4000, null=True)  # is readonly
    layer_name = models.CharField(max_length=4000)  # is readonly
    time_steps = ArrayField(models.CharField(max_length=100, blank=True, null=True), blank=True, null=True, default=list)  # is readonly
    uncertainty_path = models.CharField(max_length=4000, blank=True, null=True)
    opacity = models.DecimalField(max_digits=3, decimal_places=2, default=0.5)
    is_point_request_enabled = models.BooleanField(default=True)
    num_decimals = models.PositiveIntegerField(blank=True, null=True, validators=[MaxValueValidator(20)])
    file_path = models.CharField(max_length=4000, null=True)
    store_in_geowebcache = models.BooleanField(default=False)

    def __str__(self):
        if self.name_en:
            return self.name_de
        else:
            return 'TBD (' + str(self.id) + ')'

    class Meta:
        verbose_name = "WMS-Data-Layer"
        verbose_name_plural = "WMS-Data-Layers"


class WfsLayer(VectorLayer):
    bucket = models.ForeignKey(Bucket, on_delete=models.CASCADE)  # is readonly
    url = models.CharField(max_length=4000)
    workspace = models.CharField(max_length=50, blank=True, null=True)
    data_store_name = models.CharField(max_length=50, blank=True, null=True)
    file_path = models.CharField(max_length=50, validators=[validate_wfs_file_path])
    publish = models.BooleanField(default=False)
    is_created = models.BooleanField(default=False)
    logs = models.CharField(max_length=4000, blank=True, null=True)

    def __str__(self):
        if self.name_en:
            return self.name_de
        else:
            return 'TBD (' + str(self.id) + ')'

    class Meta:
        verbose_name = "WFS-Data-Layer"
        verbose_name_plural = "WFS-Data-Layers"


class GeojsonLayer(Layer):
    bucket = models.ForeignKey(Bucket, on_delete=models.CASCADE, null=True)
    file_path = models.CharField(max_length=4000, null=True)
    url = models.CharField(max_length=4000)

    class Meta:
        verbose_name = "GeoJSON-Data-Layer"
        verbose_name_plural = "GeoJSON-Data-Layers"


class AreaLayer(models.Model):
    area = models.ForeignKey(Area, on_delete=models.CASCADE, blank=True, null=True)
    position = models.IntegerField(default=1, blank=True, null=True)
    is_active = models.BooleanField(default=False)
    is_default_on_start = models.BooleanField(default=False)
    wms_layer = models.OneToOneField(WmsLayer, on_delete=models.CASCADE, blank=True, null=True)
    sta_layer = models.OneToOneField(StaLayer, on_delete=models.CASCADE, blank=True, null=True)
    geojson_layer = models.OneToOneField(GeojsonLayer, on_delete=models.CASCADE, blank=True, null=True)
    wfs_layer = models.OneToOneField(WfsLayer, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        if self.wms_layer:
            return str(self.wms_layer)
        elif self.sta_layer:
            return str(self.sta_layer)
        elif self.geojson_layer:
            return str(self.geojson_layer)
        elif self.wfs_layer:
            return str(self.wfs_layer)
        else:
            return 'TBD (' + str(self.id) + ')'


class WmsLayerLegend(models.Model):
    wms_layer = models.ForeignKey(WmsLayer, on_delete=models.CASCADE)
    legend = models.ForeignKey(WmsLegend, on_delete=models.CASCADE)
    replace_values_in_viewer_by_label = models.BooleanField(default=False)
    position = models.IntegerField(default=1)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['wms_layer', 'legend'], name='unique_wms_layer_legend'),
            models.UniqueConstraint(fields=['wms_layer', 'position'], name='unique_wms_legend_pos')
        ]


class Process(CommonInfo):
    type = models.CharField(max_length=20, choices=COMPUTATION_TYPES)
    sub_type = models.CharField(max_length=100, unique=True)
    spatial_aggregation_type = models.CharField(max_length=32, blank=True, null=True)
    temporal_aggregation_type = models.CharField(max_length=32, blank=True, null=True)

    def __str__(self):
        if self.name_de:
            return self.name_de
        else:
            return self.type + ':' + self.sub_type

    class Meta:
        verbose_name = 'Process'
        verbose_name_plural = 'Processes'


class WmsLayerProcess(models.Model):
    wms_layer = models.ForeignKey(WmsLayer, on_delete=models.CASCADE)
    process = models.ForeignKey(Process, on_delete=models.CASCADE)
    in_cache = models.BooleanField(default=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['wms_layer', 'process'], name='unique_layer_comp')
        ]
        verbose_name = 'WmsLayer-Process-Configuration'
        verbose_name_plural = 'WmsLayer-Process-Configurations'


class ProcessCache(models.Model):
    wms_layer = models.ForeignKey(WmsLayer, on_delete=models.CASCADE)
    process = models.ForeignKey(Process, on_delete=models.CASCADE)
    polygon_id = models.CharField(max_length=100)
    time_steps = ArrayField(models.CharField(max_length=100))
    results = models.JSONField()

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['wms_layer', 'process', 'polygon_id'], name='unique_layer_process_polygon')
        ]


class ConnectedLayer(models.Model):
    wms_layer = models.ForeignKey(WmsLayer, on_delete=models.CASCADE, related_name='ref_layer')
    referenced_wms_layer = models.ForeignKey(WmsLayer, on_delete=models.CASCADE, related_name='ref_target_layer')
    type = models.CharField(max_length=20, choices=REQUEST_TYPES, default='')

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['wms_layer', 'referenced_wms_layer'], name='unique_connected_wms_layer')
        ]


class BucketEvent(models.Model):
    event_time = models.DateTimeField()
    event_data = models.JSONField()
    logs = models.CharField(max_length=4000, blank=True, null=True)
    is_new = models.BooleanField(default=False, null=True)
    is_success = models.BooleanField(default=False, null=True)
    bucket = models.ForeignKey(Bucket, on_delete=models.CASCADE, blank=True, null=True)

    def get_bucket_name(self) -> str:
        return self.event_data["Records"][0]["s3"]["bucket"]["name"]

    def get_relative_path(self) -> str:
        path = self.get_object_key()
        return path.replace('%2F', '/')

    def get_object_key(self) -> str:
        object_key = self.event_data["Records"][0]["s3"]["object"]["key"]
        return unquote(object_key)

    def get_user(self) -> str:
        return self.event_data["Records"][0]["userIdentity"]["principalId"]

    def get_event_name(self) -> str:
        return self.event_data["Records"][0]["eventName"]

    def is_create_event(self) -> bool:
        return self.get_event_name() in ("s3:ObjectCreated:CompleteMultipartUpload", "s3:ObjectCreated:Put")

    def is_delete_event(self) -> bool:
        return self.get_event_name() == "s3:ObjectRemoved:Delete"

    def __str__(self):
        return self.get_relative_path()


class ListFilter(CommonInfo):
    area = models.ForeignKey(Area, on_delete=models.CASCADE)
    position = models.IntegerField(default=1)
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'List-Filter'
        verbose_name_plural = 'List-Filter'
        constraints = [
            models.UniqueConstraint(fields=['area', 'position'], name='unique_filter_pos')
        ]

    def __str__(self):
        return self.area.name_de + ' - ' + self.name_de


class ListFilterItem(CommonInfo):
    list_filter = models.ForeignKey(ListFilter, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Filter-Option'
        verbose_name_plural = 'Filter-Options'

    def __str__(self):
        return self.list_filter.name_de + ' - ' + self.name_de


class LayerListFilterItem(models.Model):
    layer = models.ForeignKey(AreaLayer, on_delete=models.CASCADE)
    filter_item = models.ForeignKey(ListFilterItem, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Connected Layer'
        verbose_name_plural = 'Connected Layers'
        constraints = [
            models.UniqueConstraint(fields=['layer', 'filter_item'], name='unique_layer_list_filter')
        ]

    def __str__(self):
        return 'Layer ' + str(self.layer.id)
