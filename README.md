<img src="img/spatial-io-logo.jpg" alt="Spatial.IO-Logo" height="55px" style="margin-top: 15px;" />

<a href="https://www.ufz.de/index.php?en=45348" target="_blank">
<img src="backend/main/static/RDM_logo.png" alt="Spatial.IO-Logo" align="right" height="60px" style="margin-top: 15px; margin-right: 10px" />
</a>
<a href="https://www.ufz.de/" target="_blank">
<img src="backend/main/static/UFZ_Logo_en.png" alt="Spatial.IO-Logo" align="right" height="60px" style="margin-top: 15px; margin-right: 30px" />
</a>


# spatial.IO - An integrated cloud-ready geospatial data management system of the Helmholtz Center for Environmental Research (UFZ)

A Spatial Data Infrastructure (SDI) is a combination of policies, standards and software to manage and deliver geospatial data ([Simmons, 2018](https://doi.org/10.1016/B978-0-12-409548-9.09611-1)). 
A good SDI follows policies and standards that are (widely) accepted in the communities (e.g.[FAIR](https://www.go-fair.org/fair-principles/), [OGC](https://www.ogc.org/)). 
Although often providing new functionality, the main advantage of an SDI is the connection of different tools and software products to build (mostly) automated workflows. 
This allows for less manual processing (and therefore fewer errors) as well as standardised data products due to fixed workflows. For this to work flawlessly, extensive documentation and user instructions are key. 
An SDI can contain (but is not limited to) data storage, metadata catalogue, tools for data processing, WebGIS and a form of data access (e.g. download, web service).

Climate modelling and research increasingly produce and share the data standard of [netCDF](https://www.unidata.ucar.edu/software/netcdf/) files, leading to an increasing demand in automated management of netCDF data. 
This application aims to provide automated workflows to manage standardized netCDF data and display them in an interactive WebGIS. The standard specifications follow the [Binding Regulations for Storing Data as netCDF Files](https://hereon-netcdf.readthedocs.io/en/latest/). 
Other vector and raster data formats ((Cloud optimized) GeoTIFF, sensor data) can be included in the workflows with manual work-steps and will be automated in the next versions. 
The application will be expanded continuously into a self-service platform to create custom WebGIS, automated workflows and various (meta-)data provision interfaces for a wide range of spatial data formats.

## Requirements

- Simply [FAIR](https://www.go-fair.org/fair-principles/)
- Science- and Management friendly: Provide interoperable and reliable netCDF data enriched by metadata and with provenance information.
- User friendly: Easy to use user interface for people that manage netCDF data or create WebGIS for netCDF data, without requiring knowledge about underlying technologies like databases.
- Admin friendly: A scalable and transferable container based solution that will smoothly integrate into typical scientific IT landscapes.
- Developer friendly: Common open source solutions structured by microservice architecture to keep it open and simple to extend for developers.

## Features

- S3 cloud-storage with [MinIO](https://min.io/)
- Creation of custom interactive WebGIS components for netCDF, STA and GeoTIFF data
- Extendable processes to get spatially aggregated values for netCDF and GeoTIFF data
- Use of django framework to make configuration of data and WebGIS user-friendly
- Workflow for automated creation of OGC web services with [GeoServer](https://geoserver.org/) of new netCDF data
- Workflow for automated creation of metadata entries in [GeoNetwork](https://geonetwork-opensource.org/)
- [FROST®-Server](https://www.iosb.fraunhofer.de/de/projekte-produkte/frostserver.html) to store and access point data via OGC STA
- [THREDDS Data Server (TDS)](https://www.unidata.ucar.edu/software/tds/) to provide netCDF data with [OPeNDAP](https://www.opendap.org/)

## Getting Started

Start all containers by running:


    docker compose up -d


If you want to load test-data, run:


    ./bin/load-testdata.sh


To try out the viewer, go to: [http://localhost:3000/gdi/](http://localhost:3000/gdi/)
<br>
For modifying the content, go to [http://localhost:5001/gdi-backend/admin/](http://localhost:5001/gdi-backend/admin/) and log in with: _admin/admin_.
<br>


## Tech Stack, dependencies and third party open source products

- [MinIO](https://min.io/)
- [FROST®-Server](https://www.iosb.fraunhofer.de/de/projekte-produkte/frostserver.html)
- [GeoServer](https://geoserver.org/)
- [GeoNetwork](https://geonetwork-opensource.org/)
- [THREDDS Data Server (TDS)](https://www.unidata.ucar.edu/software/tds/)
- [django](https://www.djangoproject.com/)
- [PostgreSQL](https://www.postgresql.org/)
- [Vue](https://vuejs.org/)
- [Bulma](https://bulma.io/)
- [pygeoapi](https://pygeoapi.io/)

## License

All software and components written within the spatial.IO project is currently licensed under the [EUPL](https://joinup.ec.europa.eu/sites/default/files/custom-page/attachment/eupl_v1.2_en.pdf).

## How to cite spatial.IO

If spatial.IO is advancing your research, please cite as:

> Schulz, Christian, Lange, Rebekka, & Bumberger, Jan (2023). spatial.IO - An integrated cloud-ready geospatial data management system. Zenodo. [https://zenodo.org/doi/10.5281/zenodo.10391523](https://zenodo.org/doi/10.5281/zenodo.10391523)

## Technical implementation

### Project Members

spatial.IO is realized by:
- Jan Bumberger (RDM at UFZ) (ORCID: [0000-0003-3780-8663](https://orcid.org/0000-0003-3780-8663))
- Christian Schulz (RDM at UFZ) (ORCID: [0009-0003-7941-6059](https://orcid.org/0009-0003-7941-6059))
- Rebekka Lange (RDM at UFZ) (ORCID: [0009-0000-1301-1372](https://orcid.org/0009-0000-1301-1372))

### Software Developers

spatial.IO is developed by:
- Christian Schulz (RDM at UFZ) (ORCID: [0009-0003-7941-6059](https://orcid.org/0009-0003-7941-6059))
- Rebekka Lange (RDM at UFZ) (ORCID: [0009-0000-1301-1372](https://orcid.org/0009-0000-1301-1372))

### RDM

The technical implementation of this project was realised by the [Research Data Management Team (RDM)](https://www.ufz.de/index.php?en=45348) at the Helmholtz Center for Environmental Research. RDM at the UFZ is part of of the [Helmholtz Earth and Environment DataHub](https://datahub.erde-und-umwelt.de/en/) initiative.

## Acknowledgements

We thank the [Helmholtz Association](https://www.helmholtz.de/en/) and the [Federal Ministry of Education and Research (BMBF)](https://www.bmbf.de/bmbf/en/home/home_node.html) 
for supporting the [DataHub Initiative of the Research Field Earth and Environment](https://datahub.erde-und-umwelt.de/en/). 
The DataHub enables an overarching and comprehensive research data management, following [FAIR](https://www.nature.com/articles/sdata201618) principles, 
for all Topics in the Program [Changing Earth – Sustaining our Future](https://www.helmholtz.de/en/research/research-fields/earth-and-environment/).

## Contact

Feel free to contact [Christian Schulz](mailto:christian.schulz@ufz.de) and [Rebekka Lange](mailto:rebekka.lange@ufz.de) or the [UFZ RDM team](https://www.ufz.de/index.php?en=45348) in general with every question you may have.

[Imprint](https://www.ufz.de/index.php?en=36683)