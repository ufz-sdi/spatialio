## Frontend

#### install a new package

- run:


    docker compose exec frontend bash

- install your package with npm


#### Production-Build

- run:


    ./bin/build_frontend_image.sh


## Backend

- to create a new image, you could also use a useful script:


    ./bin/build_backend_image.sh


- to update the test-data (fixtures), run:


    python3 manage.py dumpdata --exclude=admin --exclude=auth --exclude=contenttypes --exclude=sessions --exclude=main.bucketevent -o main/fixtures/test_db_content.json




- to reset the repository (remove all containers and data), run:


    sudo ./bin/cleanup-test-project.sh



## FROST-Server

#### Getting Started

- go to: [http://localhost:8082/FROST-Server/v1.1](http://localhost:8082/FROST-Server/v1.1)

- login with: _'read' / 'read'_
- more information for handling point data: [container/frost/README.md](container/frost/README.md)


## Misc

#### save current configuration of production database
- start backend with production credentials
- run:


    python manage.py dumpdata --exclude=auth --exclude=admin --exclude=contenttypes --exclude=sessions --exclude=main.processcache --exclude=main.bucketevent -o main/fixtures/prod_db_content.json

- import the data:


    python manage.py loaddata prod_db_content



## Resources

### Frontend 

#### VueJS 3
- https://v3.vuejs.org/guide/introduction.html

#### Pinia-Store
- https://pinia.vuejs.org/getting-started.html

#### Typescript
- https://www.typescriptlang.org/
- https://v3.vuejs.org/guide/typescript-support.html

#### OpenLayers
- https://vue3openlayers.netlify.app/
- https://openlayers.org/en/latest/apidoc/

#### proj4
- http://proj4js.org/
- https://epsg.io/

#### Plotly-JS
- https://plotly.com/javascript/
- Timeseries examples: https://plotly.com/javascript/configuration-options/
- Config: https://github.com/plotly/plotly.js/blob/master/src/plot_api/plot_config.js
- Translation: https://github.com/plotly/react-plotly.js/issues/99
- two y-axis: https://plotly.com/javascript/multiple-axes/

#### shapefiles
- https://github.com/calvinmetcalf/shapefile-js

#### NetCDF
- Unidata Fact Sheet: https://www.unidata.ucar.edu/publications/factsheets/current/factsheet_netcdf.pdf
- GeoServer requirements: https://docs.geoserver.org/main/en/user/extensions/netcdf/netcdf.html
- Scale-Factor: https://gis.stackexchange.com/questions/299311/geoserver-netcdf-loader-ignores-the-scaling-factor-how-to-fix

#### Bulma CSS (responsive Design)
- https://bulma.io/


### GeoServer

#### Setup
- https://docs.geoserver.org/main/en/user/production/index.html
- https://geoserver.geosolutionsgroup.com/edu/en/adv_gsconfig/gsproduction.html

#### WFS
- https://docs.geoserver.org/main/en/user/services/wfs/basics.html
- https://docs.geoserver.org/main/en/user/services/wfs/reference.html
- https://docs.geoserver.org/2.22.x/en/user/extensions/querylayer/index.html
- https://docs.ogc.org/is/09-025r2/09-025r2.html#50
- https://docs.geoserver.org/main/en/user/tutorials/cql/cql_tutorial.html#cql-tutorial
- https://gis.stackexchange.com/questions/132229/cql-filter-that-joins-two-feature-types-in-wfs
- https://docs.geoserver.org/latest/en/user/filter/function_reference.html#filter-function-reference

- Python library for using the api: https://pypi.org/project/geoserver-restconfig/

### Django / Backend
- documentation: https://docs.djangoproject.com/en/4.0/
- alternate theme: https://github.com/fabiocaccamo/django-admin-interface
- custom commands: https://docs.djangoproject.com/en/4.0/howto/custom-management-commands/
- ColorField: https://pypi.org/project/django-colorfield/
- autocomplete: https://docs.djangoproject.com/en/5.1/ref/contrib/admin/#django.contrib.admin.ModelAdmin.autocomplete_fields
- tabs in admin form: https://pypi.org/project/django-tabbed-changeform-admin/
- Templates 
  - locating dir: `python -c "import django; print(django.__path__)"`
  - copying to local dir, e.g.: `docker compose cp backend:/usr/local/lib/python3.9/site-packages/django/contrib/admin/templates/admin/base.html .`

### PyGeoApi
- https://docs.pygeoapi.io/en/stable/introduction.html
- https://opengeospatial.github.io/e-learning/ogcapi-edr/text/basic-main.html
- https://wiki.gdi-de.org/display/akgeod/OGC+API

### S3 mount tutorial
- https://blog.meain.io/2020/mounting-s3-bucket-kube/
- python s3fs: https://s3fs.readthedocs.io/en/latest/api.html#s3fs.core.S3FileSystem.read_block

### Minio Notifications
- https://docs.min.io/minio/baremetal/monitoring/bucket-notifications/publish-events-to-postgresql.html
- [example-notification](app/main/lib/utils/README.md)

### GeoNetwork
- use API with Python: https://docs.geonetwork-opensource.org/4.4/api/the-geonetwork-api/#connecting-to-the-api-with-python
- docker-compose-setup with Postgres and ElasticSearch: https://hub.docker.com/_/geonetwork/

