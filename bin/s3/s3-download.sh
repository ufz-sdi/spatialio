#!/bin/bash

set -eu

# READ $HOST, $BUCKET, S3_ACCESS_KEY, $S3_SECRET_KEY
. "$( dirname -- "$0"; )/.s3-credentials"


download_file ()
{
  RESOURCE="/${BUCKET}/${1}"
  CONTENT_TYPE="application/octet-stream"
  DATE=`date -R`
  REQUEST="GET\n\n${CONTENT_TYPE}\n${DATE}\n${RESOURCE}"
  SIGNATURE=`echo -en ${REQUEST} | openssl sha1 -hmac ${S3_SECRET_KEY} -binary | base64`

  mkdir -p $(dirname $1)

  curl -v -o ${1} \
    -H "HOST: $HOST" \
    -H "DATE: ${DATE}" \
    -H "Content-Type: ${CONTENT_TYPE}" \
    -H "Authorization: AWS ${S3_ACCESS_KEY}:${SIGNATURE}" \
    https://$HOST${RESOURCE}
}

read_dom ()
{
  local IFS=\>
  read -d \< ENTITY CONTENT
}

list_bucket_content ()
{
  RESOURCE="/$BUCKET"
  CONTENT_TYPE="application/octet-stream"
  DATE=`date -R`
  REQUEST="GET\n\n${CONTENT_TYPE}\n${DATE}\n${RESOURCE}"
  SIGNATURE=`echo -en ${REQUEST} | openssl sha1 -hmac ${S3_SECRET_KEY} -binary | base64`

  while read_dom; do
      if [[ $ENTITY = "Key" ]] ; then
          echo $CONTENT
      fi
  done < <(
    curl \
      -H "HOST: $HOST" \
      -H "DATE: ${DATE}" \
      -H "Content-Type: ${CONTENT_TYPE}" \
      -H "Authorization: AWS ${S3_ACCESS_KEY}:${SIGNATURE}" \
      https://$HOST${RESOURCE}
  )
}

case "$1" in
-a)
  list_bucket_content ;;
-d)
  SUB_PATH="$2"

  while read -r FILE ; do

    # download if filename starts with relative path matching the specified directory
    if  [[ "$FILE" == "$SUB_PATH"* ]] ; then
      download_file "$FILE"
    fi

  done < <(list_bucket_content)
  ;;
-f)
  download_file "$2" ;;
*)
  echo "$1 is not an option";;
esac
