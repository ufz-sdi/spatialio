
docker compose down

docker volume rm spatialio_postgres_data

sudo chown -R 1000:1000 minio-sys-dir
sudo rm -rf minio-sys-dir/.minio.sys
sudo rm -rf minio-sys-dir/test-bucket
sudo rm -rf minio-sys-dir/alternate-bucket
sudo rm -rf minio-files/test-bucket
sudo rm -rf minio-files/thredds/test-bucket

sudo rm -r geoserver-data/*