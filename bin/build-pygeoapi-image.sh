#!/usr/bin/env bash
set -e
DIR_SCRIPT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

COMPOSE_FILE_PATH="${DIR_SCRIPT}/../docker-compose-prod.yml"

#
# Build
#
docker compose -f $COMPOSE_FILE_PATH --env-file .env-prod build --no-cache pygeoapi


#
# Push
#
echo
echo "Image in die Registry pushen? [y|N]"
read -n 1 YESNO
if [ "${YESNO}" == "y" ]; then
    docker compose -f $COMPOSE_FILE_PATH push pygeoapi
fi
