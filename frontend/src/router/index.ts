import {createRouter, createWebHistory, RouteRecordRaw, RouterOptions, RouterView} from "vue-router";
import MapView from "../components/MapView.vue";
import ContentView from "../components/ContentView.vue";
import StartPageView from "../components/StartPageView.vue";

export const GDI_ROUTE_PREFIX = 'gdi-'

const routeName = import.meta.env.VITE_APP_ROUTE;
const routeNameAlias = import.meta.env.VITE_APP_ROUTE_ALIAS;

function getChildRoutes(prefix: string) {
  return [
  {
    path: "",
    name: prefix + "home",
    component: MapView,
  },
  {
    path: "about",
    name: prefix + "about",
    component: ContentView,
    props: {
      contentVar: 'about_text'
    }
},
{
    path: "imprint",
    name: prefix + "imprint",
    component: ContentView,
    props: {
      contentVar: 'imprint_text'
    }
  },
  {
    path: "faq",
    name: prefix + "faq",
    component: ContentView,
    props: {
      contentVar: 'faq_text'
    }
  }
]}

const routes: Array<RouteRecordRaw> = [
  { path: "/",
    redirect: { name: "home" }
  },
  {
    path: "/gdi/",
    component: StartPageView
  },
  {
    path: "/gdi/:project/:locale([a-z]{2})?",
    component: RouterView,
    children: getChildRoutes(GDI_ROUTE_PREFIX)
  },
  {
    path: "/" + routeName + "/:locale([a-z]{2})?",
    component: RouterView,
    children: getChildRoutes('')
  },
  {
    path: "/" + routeNameAlias,
    redirect: { name: 'home', params: { locale: 'en' }}
  }
] as RouteRecordRaw[];

const router = createRouter({
  history: createWebHistory(),
  routes,
} as RouterOptions);

export default router;
