import {marked} from "marked";

export function parseMarkdown(text: string|null) {
  if (text) {
    const html = marked.parse(text) as string

    // add target-blank in html because it is not possible with markdown syntax
    // (jump-tags with href="#... should be ignored)
    return html.replaceAll('href="http', 'target="_blank" href="http')
  } else {
    return ''
  }
}
