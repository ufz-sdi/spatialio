import {PlotlyTimeSeries} from "@/types";


const seasonalDates = [
    { 'month': 3, 'de': 'Frühling', 'en': 'Spring' },
    { 'month': 6, 'de': 'Sommer', 'en': 'Summer' },
    { 'month': 9, 'de': 'Herbst', 'en': 'Autumn' }
]


export function convertTime(value: string, locale: string, timeFormat: string|null = null) {

    if (locale !== 'de' && locale !== 'en') {
        return ''
    }

    if (value === '') {
        return ''
    }

    const date = new Date(value)

    if (timeFormat === 'period-49') {
        return formatYearPeriodFromCenter(value, 50)
    }
    if (timeFormat === 'period-29') {
        return formatYearPeriodFromCenter(value, 30)
    }
    if (timeFormat === 'period-19') {
        return format19YearPeriod(value)
    }
    if (timeFormat === 'year') {
        return date.getFullYear() + ''
    }
    if (timeFormat === 'month') {
        return date.toLocaleDateString('de-DE', { year: 'numeric', month: '2-digit' })
    }
    if (timeFormat === 'season') {
        return getSeasonalDate(value, locale)
    }
    return date.toLocaleDateString('de-DE', { year: 'numeric', month: '2-digit', day: '2-digit' })
}


export function getSeasonalDate(timeStep: string, locale: 'de'|'en') {
    let result = timeStep

    const date = new Date(timeStep)

    for (const season of seasonalDates) {
        if (date.getMonth() === season.month) {
            result = season[locale] + ' ' + date.getFullYear()
        }
    }
    return result
}

export function formatYearPeriodFromCenter(timeStep: string, years: number) {

    const date = new Date(timeStep)
    const year = date.getFullYear()
    const diff = (years / 2 ) - 1

    return (year - diff) + '-' + (year + (years / 2 ))
}

export function format19YearPeriod(timeStep: string) {
    const date = new Date(timeStep)
    const year = date.getFullYear()
    return (year) + '-' + (year + 19)
}

export function getSeasonalDates(timeseries: PlotlyTimeSeries, locale: 'de'|'en') {
    const tickvals = []
    const ticktexts = []

    for (const timeStep of timeseries.x) {
        tickvals.push(timeStep)
        ticktexts.push(getSeasonalDate(timeStep, locale))
    }

    return {
      tickmode: "array",
      tickvals: tickvals,
      ticktext: ticktexts,
      tickangle: 45
    }
}

export function getYearPeriodsFromCenter(timeseries: PlotlyTimeSeries, years: number) {
    const tickvals = []
    const ticktexts = []

    for (const timeStep of timeseries.x) {
        tickvals.push(timeStep)
        ticktexts.push(formatYearPeriodFromCenter(timeStep, years))
    }

    return {
      tickmode: "array",
      tickvals: tickvals,
      ticktext: ticktexts
    }
}

export function get19YearPeriods(timeseries: PlotlyTimeSeries) {
    const tickvals = []
    const ticktexts = []

    for (const timeStep of timeseries.x) {
        tickvals.push(timeStep)
        ticktexts.push(format19YearPeriod(timeStep))
    }

    return {
      tickmode: "array",
      tickvals: tickvals,
      ticktext: ticktexts
    }
}


export function isValidDateFormat(dateInput: string) {
    if (dateInput == '' || dateInput == undefined) {
        return true
    }
    return dateInput.match(/^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/);
}