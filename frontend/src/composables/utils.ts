import {CsvContent, LegendRecord, WMSLayer} from "@/types";


export function getLastTimeStepIfExists(layer: WMSLayer) {
    if (layer.time_steps.length > 0) {
        return layer.time_steps.slice(-1)[0]
    }
    console.log('not found for: ' + layer.name)
    return ''
}

export function setPageHeight() {
    const style = getComputedStyle(document.body);
    const headerHeight = style.getPropertyValue('--header-height') as string

    const contentHeight = `calc(${window.innerHeight}px - ${headerHeight})`
    document.body.style.height = contentHeight;

    ["map", "sidenav", "sidenavModal"].forEach(function (selector) {
        const element = document.getElementById(selector)
        if (element) {
            element.style.height = contentHeight
        }
    })
}

export function round4(num: number) {
    return parseFloat(num.toFixed(4))
}

export function getTextValue(layer: WMSLayer, value: number)  {

    let result = ''

    const style = layer.styles[0]

    if (layer.legends_to_map) {

        const legend = layer.legends_to_map[style]
        if (legend) {
            legend.forEach(function (record: LegendRecord) {
                if (record.quantity == value) {
                    result = record.label
                }
            })
        }
    }
    return result
}

export function downloadDataAsCsv(csvContent: CsvContent, fileName: string) {

      let csvFormatted = 'data:text/csv;charset=utf-8,'
      csvContent.forEach(function (rows: Array<any>) {
        let row = rows.join(',')
        csvFormatted += row + '\r\n'
      })

      let encodedUri = encodeURI(csvFormatted)
      let link = document.createElement('a')
      link.setAttribute('href', encodedUri)
      link.setAttribute('download', fileName + '.csv')
      document.body.appendChild(link)
      link.click()
      document.body.removeChild(link)
}