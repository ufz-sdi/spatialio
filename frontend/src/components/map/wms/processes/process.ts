

export const process_url = import.meta.env.VITE_APP_BACKEND_URL + "process"

export const AGGREGATION = 'aggregation'
export const ANOMALY = 'anomaly'
export const MAGNITUDE = 'magnitude'