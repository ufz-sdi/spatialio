import { createApp } from "vue"

// @ts-ignore
import App from "./App.vue"
import router from "./router"
import { createI18n } from 'vue-i18n'
import { createPinia } from 'pinia'
import 'bulma/css/bulma.css'

/* import the fontawesome core */
import { library } from '@fortawesome/fontawesome-svg-core'

/* import font awesome icon component */
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

/* import specific icons */
import {faAngleDown, faFileCsv} from '@fortawesome/free-solid-svg-icons'
import { faAngleUp } from '@fortawesome/free-solid-svg-icons'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { faChartLine } from '@fortawesome/free-solid-svg-icons'
import { faCircle } from '@fortawesome/free-solid-svg-icons'
import { faCircleHalfStroke } from '@fortawesome/free-solid-svg-icons'
import { faCircleInfo } from '@fortawesome/free-solid-svg-icons'
import { faFilter } from '@fortawesome/free-solid-svg-icons'
import { faLanguage } from '@fortawesome/free-solid-svg-icons'
import { faMaximize } from '@fortawesome/free-solid-svg-icons'
import { faRectangleXmark } from '@fortawesome/free-solid-svg-icons'
import { faRightLeft } from '@fortawesome/free-solid-svg-icons'
import { faThumbtack } from '@fortawesome/free-solid-svg-icons'
import { faUpload } from '@fortawesome/free-solid-svg-icons'
import { faSave } from '@fortawesome/free-solid-svg-icons'

/* add icons to the library */
library.add(faAngleDown)
library.add(faAngleUp)
library.add(faBars)
library.add(faChartLine)
library.add(faCircle)
library.add(faCircleHalfStroke)
library.add(faCircleInfo)
library.add(faFilter)
library.add(faLanguage)
library.add(faMaximize)
library.add(faRectangleXmark)
library.add(faRightLeft)
library.add(faThumbtack)
library.add(faUpload)
library.add(faSave)
library.add(faFileCsv)

import 'bulma/css/bulma.css'

import OpenLayersMap from "vue3-openlayers"
import "vue3-openlayers/dist/vue3-openlayers.css"
import drag from "v-drag"


import { messages } from "./translation/messages"

const i18n = createI18n({
  locale: 'de',
  fallbackLocale: 'de',
  warnHtmlInMessage: 'off',
  messages
})

const app = createApp(App)
const pinia = createPinia()

app.use(router)
app.use(OpenLayersMap)
app.use(drag)
app.use(i18n)
app.use(pinia)

app.component('font-awesome-icon', FontAwesomeIcon)

app.mount("#app")