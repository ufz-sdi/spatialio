import {Coordinate, Layer} from "@/types/index";


export interface VECTORLayer extends Layer {
    cluster_scale_factor: number
    cluster_minimum_size: number
}


export interface STALayer extends VECTORLayer {
    endpoint: number
    own_id: number
    observed_property_use_group: string
    legend: StaColor[]
    popupInfoConfig: {
        [key: string]: {
            "thing_property_value": string
            "name_in_frontend_de": string | undefined
            "name_in_frontend_en": string | undefined
        }
    }
}


export interface WFSLayer extends VECTORLayer {
    own_id: number
    url: string
    workspace: string
    data_store_name: string
}


export interface STAObservedProperty {
    "@iot.id": number,
    "definition": string,
    "description": string,
    "name": string,
    "properties": {
        [key: string]: string|number|string[]
    }
}


export interface WfsTimeseriesFeature {
    "type": "Feature",
    "id": string,
    "geometry": {
        "type": "Point",
        "coordinates": {
            0: number,
            1: number
        }
    },
    "geometry_name": "geom",
    "properties": {
        feature_id: number,
        property_id: number,
        unit: string,
        lat: number,
        lon: number,
        gtype: "Point",
        srid: number,
        min_value: number,
        max_value: number,
        min_date: string,
        max_date: string
    }
}


export interface VectorProperty {
    id: number,
    fullName: string,
    wfsTimeseriesId: number,
    type: 'sta'|'wfs'
}


export interface StaThing {
    "@iot.selfLink": string
    "@iot.id": number
    name: string
    description: string
    "properties": {
        [key: string]: string|number|string[]
    }
}

export interface StaColor {
    color: string,
    position: number,
    upper_limit: number,
    radius: number
}

export interface StaPopupInfo {
    label: string,
    text: string,
}

export type MatrixCell = [[number, number], [number, number], number]

export interface Cluster {
  coords: Coordinate,
  coordsWgs84: Coordinate,
  count: number,
  requestId: number,
  cellNumber: number
}