import { SwipeControl, TileLayer } from "vue3-openlayers";


export type DateFormat = '' | 'period-49' |  'period-29' | 'period-19' | 'year' | 'month' | 'season'

export type Coordinate = [number, number]

export type BoundingBox = [Coordinate, Coordinate]


export interface AreaName {
    id: number
    name: string
}

export interface Area {
    id: number
    name: string
    activeBaseMap: string
    baseMaps: []
    layers: Layer[]
    info: string
    is_default_on_start: boolean
    num_listFilters: number
    listFilters: []
}

export interface AreaCache {
    [key: number]: Area
}

export interface ProjectInfo {
    id: number,
    name_in_url: string,
    name_in_backend: string,
    name: string,
    theme: string,
    teaser: string,
    about_text: string,
    imprint_text: string,
    faq_text: string,
    maintenance_notification: string,
    is_english_version_active: boolean,
    enable_print_function: boolean,
    print_annotation: string,
    default_lon: number,
    default_lat: number,
    default_zoom: number,
    baseMap: string,
    gallery_image: string
}

export type LayerType = 'wms' | 'sta' | 'wfs' | 'geojson'

export interface Layer {
    id: number
    name: string
    info: string
    time_format: DateFormat
    y_axis_min: number
    y_axis_max: number
    type: LayerType
    is_default_on_start: boolean
}


export interface WMSLayer extends Layer {
    wms_url: string
    layer_name: string
    styles: string[]
    time_steps: string[]
    epsg_code: string
    axis_label_x: string
    axis_label_y: string
    unit: string
    desc: string
    no_data_value: number
    position: number
    available_aggregations: string[]
    available_methods: string[]
    scale_factor: number
    legends_to_map: {
        [key: string]: LegendRecord[]
    }
    uncertainty_path: string | undefined
    opacity: number
    is_point_request_enabled: boolean
    own_id: number
}


export interface GEOJSONLayer extends Layer {
    url: string
}

export interface MapClickEvent {
    coordinate: Coordinate
    pixel: [number, number]
    ranges: {
        [key: string]: {
            'values': number[]
        }
    }
    frameState: {
        size: [number, number]
    }
}

export interface LegendRecord {
    color: string,
    label: string,
    quantity: number
}

export interface StringMap {
    [key: string]: string
  }

export interface Aggregation {
    [key: string]: number
}


export type Geometry = [{
    type: 'Polygon',
    crs: 'EPSG:4326',
    coordinates: [Coordinate[]]
}]

export interface ProcessParams {
    layer_id: number
    process_type: string
    process_sub_type: string
    time_steps: string[]
    polygon: ['bl_id'|'lk_id'|'geometry', number|Geometry]
}

export interface CountryCoordinateMap {
    [key: string]: Coordinate
}

export interface PlotlyTimeSeries {
  x: string[],
  y: number[],
  type: 'scatter',
  mode: 'lines+markers',
  name: string,
  position: number,
  timeFormat: DateFormat,
  unit: string|undefined,
  yMin: number|undefined,
  yMax: number|undefined,
  visible: boolean|string
}

export interface CovJsonSingleResult {
  label: string,
  unit: string,
  value: number,
  time: string,
  timeFormat: string,
  textValue: string,
  error: boolean
}

export interface LayerFilter {
    [key: string]: number[]
}


export interface ColorLegend {
    label: string
    quantity: string
    color: string
}

export type Vue3SwipeControl = InstanceType<typeof SwipeControl>
export type Vue3TileLayer = InstanceType<typeof TileLayer>


export interface LinkConfig {
    name: string
    params: {
        locale: string
        [key: string]: string
    }
}

export interface AggregationEvent {
    layer: WMSLayer,
    feature: string
}

export interface DataCollection {
    chartName: string,
    mapPopupResult: CovJsonSingleResult,
    timeSeriesCollection: PlotlyTimeSeries[]
    singleResultCollection: CovJsonSingleResult[]
}

export type CsvContent = (string|number)[][]