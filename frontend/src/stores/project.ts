import {defineStore} from "pinia";
import {ProjectInfo} from "@/types";
import proj4 from "proj4";

export const useProjectStore = defineStore('project', {

    state: (): any => ({
        projectInfo: null as ProjectInfo|null,
        error: false as boolean,
        isPageLoading: false as boolean,
        projection: 'EPSG:3857' as string
    }),
    getters: {
        zoom() {
            return (this.projectInfo) ? this.projectInfo.default_zoom : 7
        },
        center() {
            if (this.projectInfo) {
              const mapCenter = [Number(this.projectInfo.default_lon), Number(this.projectInfo.default_lat)]
              return proj4('WGS84', 'EPSG:3857', mapCenter)
            } else {
                return null
            }
        }
    },
    actions: {
        getUrl(route: string, locale: string, subPath: string) {
            return import.meta.env.VITE_APP_BACKEND_URL + subPath + '/' + locale + '/' + this.getProjectName(route)
        },
        getProjectName(route: string) {
            if (import.meta.env.VITE_APP_PROJECT === 'gdi') {
                return route
            }
            return import.meta.env.VITE_APP_PROJECT
        }
    }
})