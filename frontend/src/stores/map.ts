import {defineStore} from "pinia";

export const useMapStore = defineStore('map', {

    state: (): any => ({
        showSidenav: false,
        showPopup: false,
        showChart: false,
        isAggregationWidgetEnabled: false,
        showMainLayerTimeRange: true as boolean,
        showVectorLayer: false as boolean,
        showVectorControl: true as boolean,
        showMainFilter: false as boolean,
        showComparisonFilter: false as boolean,

        showStabilityUncertainty: true as boolean,
        showSignificanceUncertainty: true as boolean,

        areCountriesVisible: false as boolean,
        areDistrictsVisible: false as boolean,
        isShapefileVisible: false as boolean,
        projWarning: '' as string,

        showPrintDialog: false as boolean,
        isMapVisible: true as boolean
    }),
    getters: {
        isAggregationMode() {
            if (this.areCountriesVisible || this.areDistrictsVisible || this.isShapefileVisible) {
                return true
            }
            return false
        }
    },
    actions: {
        toggleSidenav() {
            this.showSidenav = !this.showSidenav
        },
        setWmsControls() {
            this.showVectorControl = false
            this.showMainLayerTimeRange = true
        },
        setVectorControls() {
            this.showVectorLayer = true
            this.showVectorControl = true
            this.showMainLayerTimeRange = false
        },
        toggleTimeRange() {
            this.showMainLayerTimeRange = !this.showMainLayerTimeRange
            this.showVectorControl = !this.showMainLayerTimeRange;
        },
        closeChartAndPopup() {
            this.showPopup = false
            this.showChart = false
            this.isAggregationWidgetEnabled = false // TODO: is this really necessary?
        },
        closeElements() {
            this.showPopup = false
            this.showChart = false
            this.showVectorLayer = false
            this.showVectorControl = false
            this.showMainLayerTimeRange = false

            this.showPrintDialog = false
        },
        closeFilter() {
            this.showMainFilter = false
            this.showComparisonFilter = false
        },
        toggleCountries() {
            this.areCountriesVisible = !this.areCountriesVisible
            this.areDistrictsVisible = false
            this.isShapefileVisible = false
            this.closeChartAndPopup()
            //this.aggregationEvent = null
        },
        showDistricts() {
            this.areCountriesVisible = false
            this.areDistrictsVisible = true
            this.isShapefileVisible = false
            this.closeChartAndPopup()
        },
        toggleMainFilter() {
            this.showMainFilter = !this.showMainFilter
            this.showComparisonFilter = false
        },
        toggleComparisonFilter() {
            this.showMainFilter = false
            this.showComparisonFilter = !this.showComparisonFilter
        },
        toggleTrendStability(): void {
            this.showStabilityUncertainty = !this.showStabilityUncertainty
        },
        toggleSignificance(): void {
            this.showSignificanceUncertainty = !this.showSignificanceUncertainty
        },
        toggleMap() {
            this.isMapVisible = !this.isMapVisible
        },
        togglePrintDialog() {
            this.showPopup = false
            this.showChart = false
            this.isAggregationWidgetEnabled = false

            this.showPrintDialog = !this.showPrintDialog

            if (!this.showPrintDialog) {
                if (!this.isMapVisible) {
                    this.toggleMap()
                }
            }
        }
    }
})