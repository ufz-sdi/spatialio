import {defineStore} from 'pinia'
import axios from "axios";
import {Coordinate, CovJsonSingleResult, DataCollection, WMSLayer} from "@/types";
import {convertTime} from "@/composables/time_utils.ts";
import {STALayer, StaPopupInfo, StaThing, VectorProperty, WFSLayer, WfsTimeseriesFeature} from "@/types/vector";

export interface TimeseriesStore {
    wfsLayer: WFSLayer|null,
    staLayer: STALayer|null,
    feature: {},
    featureId: number|undefined,
    selectedTimeseriesId: number|undefined,
    locationProperties: VectorProperty[],
    layerProperties: VectorProperty[],
    selectedProperty: VectorProperty|undefined,
    dataCollection: DataCollection,
    inputPlaceholder: string,
    isChartLoading: boolean,
    clickedLayer: WMSLayer|undefined,
    clickedLayerTime: string|undefined,
    clickedCoord: Coordinate|undefined,
    staThing: StaThing|undefined,
    isClickOnFeature: boolean,
    popupInfos: StaPopupInfo[]
}

export const useTimeseriesStore = defineStore('timeseries', {

    state: (): TimeseriesStore => ({
        wfsLayer: null,
        staLayer: null,
        feature: {},
        featureId: undefined,
        selectedTimeseriesId: undefined,
        locationProperties: [] as VectorProperty[],
        layerProperties: [] as VectorProperty[],
        selectedProperty: {} as VectorProperty,
        dataCollection: {} as DataCollection,
        inputPlaceholder: '-',
        isChartLoading: false,

        clickedLayer: undefined,
        clickedLayerTime: undefined,
        clickedCoord: undefined,

        staThing: undefined,
        isClickOnFeature: false,
        popupInfos: [] as StaPopupInfo[]
    }),
    getters: {
        baseUrl() {
            return this.wfsLayer.url + '/' + this.wfsLayer.workspace + '/ows?'
        },
        typeName() {
            return this.wfsLayer.workspace + '%3A' + this.wfsLayer.data_store_name
        },
    },
    actions: {
        setWfsFeature(feature: any) {
            this.featureId = feature.values_.fid

            if (this.featureId !== undefined) {

                this.isClickOnFeature = true

                const params = [
                    'service=WFS',
                    'version=2.0.0',
                    'request=GetFeature',
                    'typeName=' + this.typeName + '_feature',
                    'outputFormat=application%2Fjson',
                    'featureID=' + this.featureId
                ]
                const url = this.baseUrl + params.join('&')

                axios.get(url)
                    .then(response => {
                        this.feature = response.data.features[0]

                        this.locationProperties = []
                    })
                    .catch((error: any) => {
                        console.log(error)
                    })
                    .finally(() => {
                        this.requestWfsPropertiesAtFeature()
                    })
            }
        },
        setStaFeature(feature: any) {
            let thing = feature.values_

            if (thing.id !== undefined) {
                this.isClickOnFeature = true

                const url = import.meta.env.VITE_APP_BACKEND_URL + 'sta-thing/' + this.staLayer.endpoint + '/' + thing.id

                axios.get(url)
                    .then(response => {
                        this.staThing = response.data.value[0]
                    })
                    .catch((error: any) => {
                        console.log(error)
                    })
                    .finally(() => {
                        this.requestStaPropertiesAtThing(this.staThing)
                    })
            }
        },
        requestWfsPropertiesAtFeature() {

            const params = [
                'service=WFS',
                'version=2.0.0',
                'request=GetFeature',
                'typeName=' + this.typeName + '_timeseries',
                'outputFormat=application%2Fjson',
                'CQL_FILTER=feature_id = ' + this.featureId
            ]
            const url = this.baseUrl + params.join('&')

            axios.get(url)
                .then(response => {

                    if (this.locationProperties.length === 0) {  // following loop gets somehow executed twice
                        for (const timeseriesFeature of response.data['features']) {
                            this.locationProperties.push({
                                id: timeseriesFeature.properties.property_id,
                                wfsTimeseriesId: timeseriesFeature.id.replace(this.wfsLayer.data_store_name + '_timeseries.', ''),
                                fullName: this.getNameOfProperty(timeseriesFeature) + ' (' + timeseriesFeature.properties.unit + ')'
                            })
                        }
                    }
                    this.locationProperties.sort((a: VectorProperty, b: VectorProperty) => a.fullName.localeCompare(b.fullName))
                })
                .catch((error: any) => {
                    console.log(error)
                })
                .finally(() => {
                    if (this.selectedProperty) {
                        for (const property of this.locationProperties) {
                            if (this.selectedProperty.id == property.id) {
                                this.selectedTimeseriesId = property.wfsTimeseriesId
                                this.requestPointData()
                            }
                        }
                    }
                })
        },
        getNameOfProperty(timeseriesFeature: WfsTimeseriesFeature) {
            for (const prop of this.layerProperties) {
                if (prop.id == timeseriesFeature.properties.property_id) {
                    return prop.fullName
                }
            }
            return timeseriesFeature.id
        },
        setSelectedProperty(property: VectorProperty|undefined) {
            this.selectedProperty = property

            if (property) {
                this.inputPlaceholder = property.fullName
                this.selectedTimeseriesId = property.wfsTimeseriesId

            } else {
                this.inputPlaceholder = '-'
                this.selectedTimeseriesId = undefined
            }

            if (this.staThing) {
                this.isClickOnFeature = true
            }
        },
        requestWfsProperties() {

            this.layerProperties = []

            const params = [
                'service=WFS',
                'version=2.0.0',
                'request=GetFeature',
                'typeName=' + this.typeName + '_property',
                'outputFormat=application%2Fjson'
            ]
            const url = this.baseUrl + params.join('&')

            axios.get(url)
                .then(response => {
                    for (const property of response.data['features']) {
                        this.layerProperties.push({
                            id: property['id'].replace(this.wfsLayer.data_store_name + '_property.', ''),
                            fullName: property['properties']['name']
                        })
                    }
                    this.layerProperties.sort((a: VectorProperty, b: VectorProperty) => a.fullName.localeCompare(b.fullName))
                })
                .catch((error: any) => {
                    console.log(error)
                })
        },
        requestStaProperties(locale: string) {
            this.layerProperties = []

            let url = import.meta.env.VITE_APP_BACKEND_URL + 'sta-obs-properties/' + this.staLayer.own_id

            axios.get(url)
                .then(response => {

                    const name_prop = (locale == 'de') ? 'name_de' : 'name_en'
                    for (const property of response.data.value) {

                        let fullName = property.name
                        if (property.properties) {
                            if (property.properties[name_prop]) {
                                fullName = property.properties[name_prop]
                            }
                        }
                        this.layerProperties.push({
                            id: property['@iot.id'],
                            fullName: fullName
                        })
                    }
                    this.layerProperties.sort((a: VectorProperty, b: VectorProperty) => a.fullName.localeCompare(b.fullName))
                })
                .catch((error: any) => {
                    console.log(error)
                })
        },
        requestStaPropertiesAtThing(thing: StaThing) {

            let url = import.meta.env.VITE_APP_BACKEND_URL + 'sta-obs-properties-by-thing/' + this.staLayer.own_id + '/' + thing['@iot.id']

            this.locationProperties = []

            axios.get(url)
                .then(response => {
                    this.setPopupInfos(thing)

                    for (const obsProperty of response.data.value) {
                        this.locationProperties.push({
                            id: obsProperty['@iot.id'],
                            fullName: obsProperty['name']
                        })
                    }
                    this.locationProperties.sort((a: VectorProperty, b: VectorProperty) => a.fullName.localeCompare(b.fullName))
                })
                .then(() => {
                    if (this.locationProperties.length == 1) {
                        this.selectedProperty = this.locationProperties[0] // if only one property, select it directly
                    }
                })
                .catch((error: any) => {
                    console.log(error)
                })
                .finally(() => {
                    this.requestPointData()
                })
        },
        setPopupInfos(thing: StaThing) {
            this.popupInfos = []
            for (const key of Object.keys(thing.properties)) {

                let infoText = thing.properties[key]
                if (infoText instanceof Array) {
                    infoText = infoText.join(', ')
                }
                this.popupInfos.push({
                    label: key,
                    text: infoText
                })
            }
        },
        requestPointData() {

            const params: string[] = []

            if (this.clickedLayer && this.clickedLayerTime && this.clickedCoord) {
                params.push('wmsLayer=' + this.clickedLayer.own_id)
                params.push('time=' + this.clickedLayerTime)
                params.push('x=' + this.clickedCoord[0])
                params.push('y=' + this.clickedCoord[1])
            }

            if (this.isClickOnFeature && this.staLayer && this.staThing && this.selectedProperty?.id) {
                params.push('staLayer=' + this.staLayer.own_id)
                params.push('staThing=' + this.staThing['@iot.id'])
                params.push('staObsProperty=' + this.selectedProperty.id)
            }

            if (this.isClickOnFeature && this.wfsLayer && this.selectedProperty && this.selectedTimeseriesId) {
                params.push('wfsLayer=' + this.wfsLayer.own_id)
                params.push('wfsTimeSeries=' + this.selectedTimeseriesId)
            }

            if (params.length > 0) {

                this.isChartLoading = true

                // const url = import.meta.env.VITE_APP_BACKEND_URL + 'timeseries/' + this.$i18n.locale + '?' + params.join('&')
                const url = import.meta.env.VITE_APP_BACKEND_URL + 'timeseries/de?' + params.join('&')

                axios.get(url)
                    .then(response => {
                        this.dataCollection = response.data
                        if (this.dataCollection.mapPopupResult) {
                            this.setTimeFormatInPopup(this.dataCollection.mapPopupResult)
                        }
                        if (this.dataCollection.timeSeriesCollection[0]) {
                            if (this.dataCollection.timeSeriesCollection[0].name === 'wfs') {
                                this.dataCollection.timeSeriesCollection[0].name = this.selectedProperty.fullName
                                // this.dataCollection.timeSeriesCollection[0].unit = this.selectedTimeseries.unit
                            }
                        }
                    })
                    .catch((error) => {
                        console.log(error)
                    })
                    .finally(() => {
                        this.isChartLoading = false
                        this.isClickOnFeature = false
                        this.clickedLayer = null
                    })
            }
        },
        setTimeFormatInPopup(result: CovJsonSingleResult) {
            this.dataCollection.mapPopupResult.time = convertTime(
                result.time,
                'de',
                result.timeFormat
            )
        },
        reset() {
            this.staLayer = null
            this.staThing = undefined
            this.wfsLayer = null
            this.setSelectedProperty(undefined)
            this.unsetTimeseriesData()
        },
        unsetTimeseriesData() {
            this.dataCollection = {
                chartName: '',
                mapPopupResult: {},
                timeSeriesCollection: [],
                singleResultCollection: []
            }
        }
    }
})