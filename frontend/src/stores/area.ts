import {defineStore} from 'pinia'
import {AggregationEvent, Area, AreaCache, AreaName, GEOJSONLayer, WMSLayer} from "@/types";
import {parseMarkdown} from "@/composables/parseMarkdown.ts";
import axios from "axios";
import {FeatureCollectionWithFilename} from "shpjs";
import {VECTORLayer} from "@/types/vector";
import {getLastTimeStepIfExists} from "@/composables/utils.ts";

export const useAreaStore = defineStore('area', {

    state: (): any => ({
        areaNames: [] as AreaName[],
        cachedAreas: {} as AreaCache,
        activeArea: null as Area|null,

        hasWmsLayers: false as boolean,
        hasStaLayers: false as boolean,
        hasGeojsonLayers: false as boolean,
        hasWfsLayers: false as boolean,

        mainLayer: null as WMSLayer | null,
        comparisonLayer: null as WMSLayer | null,
        mainLayerTime: '' as string,
        comparisonLayerTime: '' as string,
        mainLayerStyle: '' as string,
        comparisonLayerStyle: '' as string,
        mainLayerOpacity: 0.5 as number,
        comparisonLayerOpacity: 0.5 as number,

        stabilityUncertainty: undefined as string | undefined,
        significanceUncertainty: undefined as string | undefined,

        geojsonLayer: null as GEOJSONLayer | null,
        vectorLayer: {} as VECTORLayer|null,

        shapefileGeoJson: {} as FeatureCollectionWithFilename | FeatureCollectionWithFilename[],

        aggregationEvent: null as AggregationEvent|null
    }),
    getters: {
        getInfoBoxText() {
            if (this.activeArea) {
                return parseMarkdown(this.activeArea.info)
            } else {
                return ''
            }
        },
        hasLayerUncertainties() {
            return this.stabilityUncertainty || this.significanceUncertainty
        }
    },
    actions: {
        requestArea(areaId: number, locale: string) {
            if (this.cachedAreas[areaId]) {
                this.setArea(this.cachedAreas[areaId])
                return new Promise((resolve) => {
                    resolve(this.cachedAreas[areaId])
                })
            } else {
                return axios.get(import.meta.env.VITE_APP_BACKEND_URL + 'area/' + locale + '/' + areaId)
                .then(response => {

                    const area = response.data.result.area

                    this.cachedAreas[areaId] = area
                    this.setArea(area)

                    if (area.num_listFilters > 0 && area.listFilters.length == 0) {
                        this.requestFilters(area, locale)
                    }
                    return area
                })
            }
        },
        setArea(area: Area) {
            this.activeArea = area

            this.hasWmsLayers = false
            this.hasStaLayers = false
            this.hasGeojsonLayers = false
            this.hasWfsLayers = false

            for (const layer of area.layers) {
                if (layer.type === 'wms') {
                    this.hasWmsLayers = true
                }
                if (layer.type === 'sta') {
                    this.hasStaLayers = true
                }
                if (layer.type === 'geojson') {
                    this.hasGeojsonLayers = true
                }
                if (layer.type === 'wfs') {
                    this.hasWfsLayers = true
                }
            }
        },
        requestFilters(area: Area, locale: string) {

            const url = import.meta.env.VITE_APP_BACKEND_URL + 'filter/' + locale + '/' + area.id

            axios.get(url)
                .then(response => {
                    this.activeArea.listFilters = response.data.list_filters
                })
                .catch((error) => {
                    console.log(error)
                })
        },
        setGeojsonLayer(layer: GEOJSONLayer) {
            this.unsetGeojsonLayer()
            setTimeout(() => this.geojsonLayer = layer, 100)
        },
        unsetGeojsonLayer() {
            this.geojsonLayer = null
        },
        setVectorLayer(layer: VECTORLayer) {
            this.vectorLayer = layer
        },
        unsetVectorLayer() {
            this.vectorLayer = null
        },
        setMainLayer(layer: WMSLayer) {
            this.mainLayer = layer

            if (layer.time_steps.indexOf(this.mainLayerTime) === -1) {
                // change only if keeping the same timestep not possible
                this.setMainLayerTime(getLastTimeStepIfExists(this.mainLayer))
            }
            this.changeMainLayerStyle(this.mainLayer.styles[0])
            this.setLayerTransparency()
        },
        unsetMainLayer() {
            this.mainLayer = null
        },
        changeMainLayerStyle(style: string) {
            this.mainLayerStyle = style
        },
        setComparisonLayer(layer: WMSLayer) {
            this.comparisonLayer = layer

            if (layer.time_steps.indexOf(this.comparisonLayerTime) === -1) {
                // change only if keeping the same timestep not possible
                this.setComparisonLayerTime(getLastTimeStepIfExists(this.comparisonLayer as WMSLayer))
            }
            this.changeComparisonLayerStyle(this.comparisonLayer.styles[0])
            this.setLayerTransparency()
        },
        unsetComparisonLayer() {
            this.comparisonLayer = null
        },
        changeComparisonLayerStyle(style: string) {
            this.comparisonLayerStyle = style
        },
        setMainLayerTime(value: string) {
            this.mainLayerTime = value

            this.setMainLayerUncertainties()
        },
        setMainLayerUncertainties() {
            this.stabilityUncertainty = undefined
            this.significanceUncertainty = undefined
            if (this.mainLayer.uncertainty_path) {
                this.stabilityUncertainty = this.mainLayer.uncertainty_path + "_direction0p66_timestamp" + this.mainLayer.time_steps.indexOf(this.mainLayerTime) + ".geoJSON"
                this.significanceUncertainty = this.mainLayer.uncertainty_path + "_significance0p5_timestamp" + this.mainLayer.time_steps.indexOf(this.mainLayerTime) + ".geoJSON"
            }
        },
        setComparisonLayerTime(value: string) {
            this.comparisonLayerTime = value
        },
        setLayerTransparency() {
            if(this.mainLayer) {
                this.mainLayerOpacity = Number(this.mainLayer.opacity)
            }
            if(this.comparisonLayer) {
                this.comparisonLayerOpacity = Number(this.comparisonLayer.opacity)
            }
        },
        setLayerOpacity() {
            this.mainLayerOpacity = 1
            this.comparisonLayerOpacity = 1
        }
    }
})