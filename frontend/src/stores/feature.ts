import {defineStore} from 'pinia'
import axios from "axios";
import proj4 from "proj4";
import {Cluster, MatrixCell, STALayer, VectorProperty, WFSLayer} from "@/types/vector";
import {BoundingBox} from "@/types";


const MAX_LOCATIONS_ON_DESKTOP = 300
const MAX_LOCATIONS_ON_MOBILE = 200

const MATRIX: MatrixCell[] = [
    [[0, 1], [1, 2], 1],
    [[0, 0], [1, 1], 2],

    [[1, 1], [2, 2], 3],
    [[1, 0], [2, 1], 4],

    [[2, 1], [3, 2], 5],
    [[2, 0], [3, 1], 6],
]
const MATRIX_WIDTH = 3
const MATRIX_HEIGHT = 2


const SUB_MATRIX_WIDTH = 3
const SUB_MATRIX_HEIGHT = 3
const SUB_MATRIX: MatrixCell[] = [
    [[0, 2], [1, 3], 1],
    [[0, 1], [1, 2], 2],
    [[0, 0], [1, 1], 3],

    [[1, 2], [2, 3], 4],
    [[1, 1], [2, 2], 5],
    [[1, 0], [2, 1], 6],

    [[2, 2], [3, 3], 7],
    [[2, 1], [3, 2], 8],
    [[2, 0], [3, 1], 9],
]

export interface FeatureStore {
    wfsLayer: WFSLayer|null,
    staLayer: STALayer|null,
    clusters: Cluster[],
    features: any[],
    featuresCount: number,
    selectedPropertyId: number|undefined,
    beginDate: string,
    endDate: string,
    thresholdValue: string,
    currentBoundingBox: BoundingBox,
    currentRequestId: number,
    requestsNeeded: number,
    requestsFinished: number,
    isLoading: boolean
}

export const useFeatureStore = defineStore('feature', {

    state: (): FeatureStore => ({
        wfsLayer: null,
        staLayer: null,
        clusters: [],
        features: [],
        featuresCount: 0,
        selectedPropertyId: undefined,
        beginDate: '',
        endDate: '',
        thresholdValue: '',
        currentBoundingBox: [[0, 0], [90, 90]] as BoundingBox,
        currentRequestId: 0 as number,
        requestsNeeded: 0 as number,
        requestsFinished: 0 as number,
        isLoading: false
    }),
    getters: {
        baseUrl() {
            return import.meta.env.VITE_APP_BACKEND_URL + 'wfs-features/'+ this.wfsLayer.own_id + '?'
        },
        bboxBottomLeftLon() {
            return this.currentBoundingBox[0][0]
        },
        bboxBottomLeftLat() {
            return this.currentBoundingBox[0][1]
        },
        bboxTopRightLon() {
            return this.currentBoundingBox[1][0]
        },
        bboxTopRightLat() {
            return this.currentBoundingBox[1][1]
        },
        maxLocations() {
            return (window.innerWidth > 1408) ? MAX_LOCATIONS_ON_DESKTOP : MAX_LOCATIONS_ON_MOBILE
        }
    },
    actions: {
        setSelectedProperty(property: VectorProperty|undefined) {
            this.featuresCount = 0

            if (property) {
                this.selectedPropertyId = property.id
            } else {
                this.selectedPropertyId = undefined
            }

            if (this.wfsLayer) {
                this.requestWfsFeatures()
            }
            if (this.staLayer) {
                this.requestStaFeatures()
            }
        },
        requestWfsFeatures() {
            const wfsParams = [
                'service=WFS',
                'outputFormat=application%2Fjson',
                'srs=EPSG:4326'
            ]
            const url = this.baseUrl + wfsParams.join('&') + '&'

            this.requestFeatures(url, 'totalFeatures')
        },
        requestStaFeatures() {
            const url = import.meta.env.VITE_APP_BACKEND_URL + 'sta-locations/' + this.staLayer.own_id + '?'

            this.requestFeatures(url, '@iot.count')
        },
        requestFeatures(url: string, countField: string) {

            const countUrl = url + this.getFilterParams() + '&count=1'

            axios.get(countUrl).then(response => {

                this.featuresCount = response.data[countField]

                this.isLoading = true

                this.clusters = []
                this.features = []

                if (this.featuresCount > this.maxLocations) {
                    this.requestCluster(url, countField)
                } else {
                    this.requestFullLocations(url)
                }
            })
            .catch((error: any) => {
                console.log(error)
            })
        },
        getCellBoundingBox(cell: MatrixCell): BoundingBox {
            const cellWidth = Math.abs((this.bboxBottomLeftLon - this.bboxTopRightLon) / MATRIX_WIDTH)
            const cellHeight = Math.abs((this.bboxBottomLeftLat - this.bboxTopRightLat) / MATRIX_HEIGHT)

            const cellBottomLon = this.bboxBottomLeftLon + (cellWidth * cell[0][0])
            const cellBottomLat = this.bboxBottomLeftLat + (cellHeight * cell[0][1])

            const cellTopLon = this.bboxBottomLeftLon + (cellWidth * cell[1][0])
            const cellTopLat = this.bboxBottomLeftLat + (cellHeight * cell[1][1])

            return [[cellBottomLon, cellBottomLat], [cellTopLon, cellTopLat]]
        },
        getSubCellBoundingBox(cell: MatrixCell, bbox: BoundingBox): BoundingBox {
            const cellWidth = Math.abs((bbox[0][0] - bbox[1][0]) / SUB_MATRIX_WIDTH)
            const cellHeight = Math.abs((bbox[0][1] - bbox[1][1]) / SUB_MATRIX_HEIGHT)

            const cellBottomLon = bbox[0][0] + (cellWidth * cell[0][0])
            const cellBottomLat = bbox[0][1] + (cellHeight * cell[0][1])

            const cellTopLon = bbox[0][0] + (cellWidth * cell[1][0])
            const cellTopLat = bbox[0][1] + (cellHeight * cell[1][1])

            return [[cellBottomLon, cellBottomLat], [cellTopLon, cellTopLat]]
        },
        getFilterParams(bbox: BoundingBox | undefined = undefined) {

            const params = []

            if (this.thresholdValue !== '') {
                params.push('threshold=' + this.thresholdValue)
            }
            if (this.beginDate !== '') {
                params.push('beginDate=' + this.beginDate)
            }
            if (this.endDate !== '') {
                params.push('endDate=' + this.endDate)
            }

            if (bbox) {
                params.push('bbox=' + bbox[0][0] + ',' + bbox[0][1] + ',' + bbox[1][0] + ',' + bbox[1][1] + ',EPSG:4326')
            } else {
                params.push('bbox=' + this.bboxBottomLeftLon + ',' + this.bboxBottomLeftLat + ',' + this.bboxTopRightLon + ',' + this.bboxTopRightLat + ',EPSG:4326')
            }

            if (this.selectedPropertyId) {
                params.push('property=' + this.selectedPropertyId)
            }

            return params.join('&')
        },
        requestCluster(url: string, countField: string) {

            const requestId = Math.floor(Math.random() * 1000000)
            this.currentRequestId = requestId
            this.requestsNeeded = 0

            for (const cell of MATRIX) {

                const bbox = this.getCellBoundingBox(cell)

                let clusterUrl = url + this.getFilterParams(bbox) + '&count=1'

                axios.get(clusterUrl).then(response => {

                        const countFeatures = response.data[countField]

                        if (countFeatures > 0 && requestId === this.currentRequestId) {
                            this.requestsNeeded += 9
                            this.requestSubCluster(url, countField, requestId, bbox)
                        }
                    })
                    .catch((error: any) => {
                        console.log(error)
                        this.isLoading = false
                    })
            }
        },
        requestSubCluster(url: string, countField: string, requestId: number, bbox: BoundingBox) {

            for (const cell of SUB_MATRIX) {

                const subBbox = this.getSubCellBoundingBox(cell, bbox)

                let clusterUrl = url + this.getFilterParams(subBbox) + '&count=1'

                axios.get(clusterUrl).then(response => {

                        const countFeatures = response.data[countField]

                        if (countFeatures > 0 && requestId === this.currentRequestId) {

                            if (response.data.features[0]) {

                                const coord = [
                                    (subBbox[0][0] + subBbox[1][0] + response.data.features[0].geometry.coordinates[0] * 4) / 6,
                                    (subBbox[0][1] + subBbox[1][1] + response.data.features[0].geometry.coordinates[1] * 4) / 6
                                ]

                                const newCluster = {
                                    coords: proj4('WGS84', 'EPSG:3857', coord),
                                    coordsWgs84: coord,
                                    count: countFeatures,
                                    requestId: requestId,
                                    cellNumber: cell[2]
                                }

                                if (requestId === this.currentRequestId) {
                                    this.clusters.push(newCluster)
                                }
                            }
                        }
                    })
                    .catch((error: any) => {
                        console.log(error)
                        this.isLoading = false
                    })
                    .finally(() => {
                        this.requestsFinished++
                        if (this.requestsFinished === this.requestsNeeded) {
                            this.requestsFinished = 0
                            this.requestsNeeded = 0
                            this.isLoading = false
                        }
                    })
            }
        },
        requestFullLocations(url: string) {

            const locationUrl = url + this.getFilterParams()

            axios.get(locationUrl)
                .then(response => {
                    if (response.data['features']) {
                        for (const point of response.data['features']) {
                            if (this.wfsLayer) {
                                point.properties['fid'] = point['id'].replace(this.wfsLayer.data_store_name + '_feature.', '')
                            }
                            this.features.push({
                                coordinates: proj4('WGS84', 'EPSG:3857', point.geometry.coordinates),
                                properties: point.properties
                            })
                        }
                    }
                })
                .catch((error: any) => {
                    console.log(error)
                    this.isLoading = false
                })
                .finally(() => {
                    this.isLoading = false
                })
        },
        reset() {
          this.staLayer = null
          this.wfsLayer = null
          this.setSelectedProperty(undefined)
        }
    }
})
